import React from "react";
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "./screens/home";
import Login from "./screens/login/login";
import SignUp from "./screens/login/signUp";
import Dashboard from "./screens/dashboard/dashboard";
import Orders from "./screens/orders/orderTab";
import Zones from "./screens/orders/zones";
import Chefs from "./screens/orders/chefs";
import Menus from "./screens/orders/orderView";
import Dishes from "./screens/orders/dishes";
import OrderSummary from "./screens/orders/OrderSummary";
import ChefsList from "./screens/chefs/chefsList";
import Chefszones from "./screens/chefs/chefszones";
import ChefsDetail from "./screens/chefs/chefsDetail";
import Reclaim from "./screens/reclaim/reclaim";


export default function App() {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="home">
        <Stack.Screen
          name="home"
          options={{ headerShown: false }}
          children={(props) => <Home {...props} />}
        />
        <Stack.Screen
          name="login"
          options={{ headerShown: false }}
          children={(props) => <Login {...props} />}
        />
         <Stack.Screen
          name="cdetail"
          options={{ headerShown: false }}
          children={(props) => <ChefsDetail {...props} />}
        />
        <Stack.Screen
          name="czones"
          options={{ headerShown: false }}
          children={(props) => <Chefszones {...props} />}
        />
        <Stack.Screen
          name="reclaim"
          options={{ headerShown: false }}
          children={(props) => <Reclaim {...props} />}
        />
        <Stack.Screen
          name="chefsList"
          options={{ headerShown: false }}
          children={(props) => <ChefsList {...props} />}
        />
        <Stack.Screen
          name="signUp"
          options={{ headerShown: false }}
          children={(props) => <SignUp {...props} />}
        />
        <Stack.Screen
          name="dashboard"
          options={{ headerShown: false }}
          children={(props) => <Dashboard {...props} />}
        />
         <Stack.Screen
          name="orders"
          options={{ headerShown: false }}
          children={(props) => <Orders {...props} />}
        />
         <Stack.Screen
          name="dishes"
          options={{ headerShown: false }}
          children={(props) => <Dishes {...props} />}
        />
        <Stack.Screen
          name="zones"
          options={{ headerShown: false }}
          children={(props) => <Zones {...props} />}
        />
        <Stack.Screen
          name="orderS"
          options={{ headerShown: false }}
          children={(props) => <OrderSummary {...props} />}
        />
        <Stack.Screen
          name="chefs"
          options={{ headerShown: false }}
          children={(props) => <Chefs {...props} />}
        />
        <Stack.Screen
          name="menu"
          options={{ headerShown: false }}
          children={(props) => <Menus {...props} />}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}