import React from 'react'
import {View,Text} from 'react-native'
   

const Hr = ({ title }) => {
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ flex: 1, height: 11, backgroundColor: '#ffcc00', marginRight: 4 }} />
            <View>
                <Text style={{ textAlign: 'center', color: '#c9c9c9' }}>{title}</Text>
            </View>
            <View style={{ flex: 1, height: 1, backgroundColor: '#c9c9c9', marginLeft: 4 }} />
        </View>
    )
}
export default Hr 

