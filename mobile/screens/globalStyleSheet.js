import { StyleSheet } from 'react-native'

const globalStyleSheet = StyleSheet.create({


classicFlatList :{

        width: '100%',
        marginBottom: 0,
        alignItems: 'center',
        height: 110, flexDirection: 'row',
        borderBottomWidth:1,
        borderBottomColor:'#e3e3e3'
   

},



    logoFlatList: {
        width: 50, height: 50, borderRadius: 50,borderWidth:2,
       
    },
    firtLine: {
        color: 'red',
        fontSize: 15,
        fontWeight: 'bold'
    },
    secondLine: {
        color: '#5b5b5b',
        fontSize: 12,
        fontWeight: 'bold'
    },
    line: {
        width: '100%',
        marginBottom: 5,
        alignItems: 'center',
        height: 140,
        flexDirection: 'row'
    },
    oddLine: {
        backgroundColor: '#fff'
    },
    notOddLine: {
        backgroundColor: '#f5f5f5'

    },
    iconAndTextWrapper: {
        flexDirection: "row",
        marginLeft: 0,
        marginTop: 0,
        textAlignVertical:'center',textAlign:'center',justifyContent:'center'
    },
    iconStyle: {
        marginTop: 4,
        color: '#fff',
        marginRight:4
    },
    
bigButtonText:
{ fontSize: 20,color:'#fff',fontWeight:'bold' },


}

)
export default globalStyleSheet