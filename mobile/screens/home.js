import { StatusBar } from 'expo-status-bar';
import React  from 'react';
import { StyleSheet, Text, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Location from 'expo-location';
import * as Notifications from 'expo-notifications';
import client from '../utils/client';
import * as Application from 'expo-application';

export default class Home extends React.Component {

    componentDidMount() {
       // this.props.navigation.navigate('login')
       this.getData();

    };

    getData = async () => {
        try {
            let { status } = await Location.requestPermissionsAsync();
            if (status !== 'granted') {
                return;
            }

            var loc = await Location.hasServicesEnabledAsync();

            if (loc === false) {

                let { status } = await Location.requestPermissionsAsync();
                if (status !== 'granted') {
                    console.log('notgranted')
                }
            }
            let location = await Location.getCurrentPositionAsync({});
            await AsyncStorage.setItem('location', JSON.stringify(location))
            await AsyncStorage.setItem('userID', Application.androidId)
            // await AsyncStorage.removeItem('token')
            // await AsyncStorage.removeItem('userId')
            // await AsyncStorage.removeItem('user')
            this.props.navigation.replace('dashboard');

        } catch (err) {
            console.log('d',err)
            this.props.navigation.replace('dashboard');
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Loading</Text>
                <StatusBar style="auto" />
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
