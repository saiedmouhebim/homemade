import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableHighlight,TouchableOpacity, Image, Alert,  } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { Col, Row, Grid } from "react-native-easy-grid";
import Ionicons from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AntDesign } from '@expo/vector-icons'; 

export default class Header extends React.Component {

    state = {
        visible: false,
        productMode: 'cascader',
        open: false,
        productInputValue: ''
    }

    toggleOpen = () => {
        this.setState({ open: !this.state.open });
    };

    deconnect = () => {
        Alert.alert('Quitter ?', 'Etes vous sûr de vouloir terminer votre session ?', [
            {
                text: 'Non, non !!, je continue', color: '#bb1125',
                onPress: () => null,
                style: 'cancel',
            },
            {
                text: 'Oui, j\'ai terminé'
                , onPress: () => this.logout()
    
            },
        ])
    }

    logout = async () => {
        await AsyncStorage.removeItem('location');
        await AsyncStorage.removeItem('token');
        await AsyncStorage.removeItem('userID');
        await AsyncStorage.removeItem('user');
        await AsyncStorage.removeItem('client');
        await AsyncStorage.removeItem('totalPrice');
        await AsyncStorage.removeItem('order');
        this.props.routeParams.navigation.replace('login')

    }

    componentDidMount() {
        if (this.props.textFilter) {
            this.setState({ productInputValue: this.props.textFilter })
        }

    }

    setVisible = (visible) => {
        this.setState({ visible });
    }

    toggleOverlay = () => {
        this.setVisible(!this.state.visible);
    };

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({ productInputValue: nextProps.textFilter });
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.props.routeParams.navigation.goBack()}
                    style={{ width: '10%', justifyContent: 'center' }}>
                    <FontAwesome name="chevron-left" size={24} color="#fff" />
                </TouchableOpacity>
                <View style={{ width: '75%', justifyContent: 'center' }}>
                    <Grid style={{ width: '100%', height: '100%' }}>
                        <Row>
                        
                            <Col size={70} style={{ justifyContent: 'center' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 20, color: '#fff' }}>{this.props.routeParams.routeName}</Text>
                            </Col>
                        </Row>
                    </Grid>
                </View>
                <View style={{width : '100%', height : '100%'}}>
                    <TouchableHighlight
                    
                        onPress={() => this.props.routeParams.navigation.navigate('orderS')}
                        style={StyleSheet.btnHeader,{width : '100%',height : '100%'}}>
                        <View style={{ flex: 1 }}>
                            <AntDesign name="shoppingcart" color={'#b9b9b9'} size={30} style={{ paddingRight: 15, paddingTop: 11 }} />
                            </View>
                    </TouchableHighlight>
                </View>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 65,
        width: '100%',
        borderBottomWidth: 2,
        borderBottomColor: 'black',
        flexDirection: 'row',
        backgroundColor: '#282828',
        paddingLeft: '3%'
    }
});
