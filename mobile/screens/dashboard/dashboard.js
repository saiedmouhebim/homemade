import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { menus } from '../../utils/const';
import { FontAwesome } from '@expo/vector-icons';
// import { AnimatedSVGPath } from "react-native-svg-animations";


//export default function Dashboard(props) {
const Dashboard = (props) => {


    return (
        <View style={{ flex: 10 }}>
                            <ImageBackground source={require('../../assets/bkg-home-top-opacity.jpg')}
style={{ flex: 1 }}>
            <View style={{ flex: 3 }}>
                    
                    <Text style={{ fontSize: 23, fontWeight: 'bold', textAlign: 'center', marginTop: 6, color: '#d44f8f' }}>Menu Général</Text>
                    <View style={styles.header}>

                        <Image style={styles.logo} source={require('../../assets/homemade.png')} />
                    </View>
            </View>

            <View style={styles.container}>
                <ScrollView
                // contentContainerStyle={{ backgroundColor:'red' }}
                >

                    {menus.map((elem, index) => {
                        return (
                            <TouchableOpacity key={index}
                                onPress={() => props.navigation.navigate(elem.route)}
                                style={{
                                    backgroundColor: elem.backgroundColor,
                                    height: 60,
                                    flexDirection: 'row',
                                    justifyContent: "center",
                                    marginBottom: 7,

                                    borderRadius: 30,
                                    marginHorizontal: 0
                                    // opacity: 1,
                                    // elevation: 4,
                                    // borderColor: '#d0d0d0',
                                    // borderStyle: 'solid',
                                    // borderWidth: 1,
                                }}>
                                <View style={{ marginLeft: '3%', borderColor: '#949494', borderWidth: 2, width: 40, height: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: '#2b2b2b', borderRadius: 50, alignSelf: 'center' }}>
                                    <FontAwesome name={elem.iconName} size={17} color='#cd5a92' />
                                </View>
                                <View style={{ width: '72%', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 18, fontWeight: '900', paddingLeft: 10, color: elem.forecolor }}>{elem.menuName}</Text>
                                </View>
                                <View style={{ width: '10%', justifyContent: 'center' }}>
                                    <FontAwesome name="angle-right" size={35} color="#fff" />
                                </View>
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
            </View>
            </ImageBackground >

        </View >
    );
}


export default Dashboard;

const styles = StyleSheet.create({
    container: {
        flex: 7,
        backgroundColor: '#fdfdfd',
        alignItems: 'center',
        marginTop: 100
    },

    header: {

        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25

        //  backgroundColor: 'grey'
    },
    logo: {
        width: 176, height: 134,
        alignItems: 'center',

        marginTop: 0
    },

    text: {
        fontSize: 20, fontWeight: '100', paddingLeft: 10, color: 'white'
    }
});

