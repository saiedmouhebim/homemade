import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList,TouchableHighlight, TouchableOpacity } from 'react-native';
import Header from '../header/header'
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import { getDistance, getPreciseDistance } from 'geolib';
import { Col, Row, Grid } from "react-native-easy-grid";
import Draggable from 'react-native-draggable';
const globalStyleSheet = require('../globalStyleSheet.js').default
export default class Home extends React.Component {

    state = {
        orders: [],
        page: 0,
        limit: 20,
        total: 0,
        loading: true
    }

    componentDidMount() {
        this.getData();
    };

    getData = async () => {
        const userId = await AsyncStorage.getItem('userID')
        const query = {
            query: {
                $limit: 10,
                c: userId,
                $skip: 0
            }
        }
        try {
            const orders = await client.service('reclaim').find(query);
            this.setState({
                loading: false, orders: orders.data,
                page: 1, total: orders.total
            })

        } catch (err) {
            console.log(err)
        }
    }

    renderMoreItems = async () => {
        if (this.state.total > this.state.orders.length) {
            const userId = await AsyncStorage.getItem('userID')
            const query = {
                query: {
                    $limit: 10,
                    userUniqueId: userId,
                    $skip: 10 * this.state.page
                }
            }
            client.service('reclaim').find(query).then((orders) => {

                this.setState({
                    loading: false, orders: [...this.state.orders, ...orders.data],
                    page: this.state.page
                })
            }).catch(err => console.log(err))

        }
    }

    renderItem({ item, index }) {

        return (
            <TouchableOpacity
            style={[globalStyleSheet.line, index % 2 === 0 ? globalStyleSheet.oddLine : globalStyleSheet.notOddLine]}
            >
                <View style={{ width: '75%', flexDirection: 'column' , marginLeft : 25}}>
                    <Text style={globalStyleSheet.firtLine}>Title : {item.title}</Text>
                 
                    <Text style={{ color: '#dcdcdc', fontWeight: 'bold' }}>Email : {item.email}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Header
                    routeParams={{ routeName: 'Reclamation', ...this.props }} />
                <View style={styles.itemsContainer}>
                    <FlatList
                        onEndReachedThreshold={0.5}
                        keyExtractor={item => item._id}
                        onEndReached={() => {
                            this.renderMoreItems()
                        }}
                        contentContainerStyle={styles.list}
                        data={this.state.orders}
                        renderItem={this.renderItem}
                    />
                     <Draggable>
                    <TouchableHighlight
                        // underlayColor={'rgba(205, 90, 146,0.5)'}
                        onPress={() => this.props.navigation.navigate('addReclaim')}
                        style={{
                            width: 60, height: 60, backgroundColor: 'rgb(205, 90, 146)',
                            justifyContent: 'center', alignItems: 'center', borderRadius: 30, elevation: 5
                        }}>
                        <Text style={{ fontSize: 35, fontWeight: 'bold', color: 'white' }}>+</Text>

                    </TouchableHighlight>
                </Draggable>
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    itemsContainer: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
    }
});
