
import React, { useState, useEffect } from 'react';
import {
    StyleSheet, ScrollView,
    ImageBackground,
    Text,
    View,
    TouchableOpacity,
    ActivityIndicator,
    TextInput,
    Alert
}
    from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import * as ImagePicker from 'expo-image-picker';
import { FontAwesome } from '@expo/vector-icons';
import { Col, Row, Grid } from "react-native-easy-grid";
import { Avatar } from 'react-native-elements';
import { GENDERS } from '../../utils/const'
import { Picker } from "@react-native-picker/picker";

export default function Login(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [photo, setPhoto] = useState('');
    const [gender, setGender] = useState(null);
    const [userName, setUserName] = useState('');
    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getPermissionsAsync();
    }, [])

    function loginWithCrediantials() {

    }

    const getPermissionsAsync = async () => {
        if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== 'granted') {
                console.log('Sorry, we need camera roll permissions to make this work!');
            }
        }
    }

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            base64: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            setPhoto('data:image/jpeg;base64,' + result.base64);
        }
    };

    const submit = () => {
        setLoading(true)
        if(userName.length === 0){
            setLoading(false)
            Alert.alert('veuillez remplir le nom d\'utilisateur')
            return;
        }
        if(password.length === 0){
            setLoading(false)
            Alert.alert('veuillez remplir le mots de passe')
            return;
        }
        if(email.length === 0){
            setLoading(false)
            Alert.alert('veuillez remplir le mail')
            return;
        }
        if(firstName.length === 0){
            setLoading(false)
            Alert.alert('veuillez remplir le prénom')
            return;
        }
        if(lastName.length === 0){
            setLoading(false)
            Alert.alert('veuillez remplir le nom ')
            return;
        }

        const query = {
            firstName,
            lastName,
            username:userName,
            photo,
            email,
            password,
            gender,
            zone,
            type: 'nu'
        }
        client.service('users').create(query).then(user =>{
            setLoading(false)
            props.navigation.navigate('login')
        }).catch(err =>{setLoading(false);console.log(JSON.stringify(err.message));Alert.alert(JSON.stringify(err.message))})
    }

    return (
        <ImageBackground style={{ resizeMode: "resize", justifyContent: 'flex-start', width: '100%' }} source={require("../../assets/bkg-home.jpg")}>
            <ScrollView >
                <Grid style={{ width: '100%', height: '100%', marginTop: 30 }}>

                    <Row size={25} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                        <Avatar
                            rounded
                            style={{ width: 100, height: 100, borderRadius: 100 }}
                            source={photo.length === 0 ? require('../../assets/pngegg.png') : { uri: photo }}
                        >
                            <Avatar.Accessory size={30} onPress={pickImage} />
                        </Avatar>
                    </Row>
                    <Row size={15} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <View style={styles.inputView} >
                            <TextInput
                                style={styles.inputText}
                                placeholder="Nom d'utilisateur..."
                                value={userName}
                                placeholderTextColor="#003f5c"
                                onChangeText={text => setUserName(text)} />
                        </View>
                    </Row>
                    <Row size={15} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                        <Col size={50} style={{ margin: 5 }}><View style={styles.inputView} >
                            <TextInput
                                style={styles.inputText}
                                placeholder="Prénom..."
                                value={firstName}
                                placeholderTextColor="#003f5c"
                                onChangeText={text => setfirstName(text)} />
                        </View>
                        </Col>
                        <Col size={50}><View style={styles.inputView} >
                            <TextInput
                                style={styles.inputText}
                                placeholder="Nom..."
                                value={lastName}
                                placeholderTextColor="#003f5c"
                                onChangeText={text => setlastName(text)} />
                        </View>
                        </Col>
                    </Row>
                    <Row size={15} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 10 }}>
                        <View style={styles.inputView} >
                            <TextInput
                                style={styles.inputText}
                                placeholder="Email..."
                                value={email}
                                placeholderTextColor="#003f5c"
                                onChangeText={text => setEmail(text)} />
                        </View>
                    </Row>
                    <Row size={15} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <View style={styles.inputView} >
                            <TextInput
                                secureTextEntry
                                value={password}
                                style={styles.inputText}
                                placeholder="Mot de passe..."
                                placeholderTextColor="#003f5c"
                                onChangeText={text => setPassword(text)} />
                        </View>
                    </Row>
                    <Row size={15}>
                    <View style={styles.inputView} >
                            <Picker
                                selectedValue={gender}
                                style={{ height: 50, width: "100%" }}
                                onValueChange={(itemValue, itemIndex) => {
                                    setGender(itemValue)
                                }}
                            >
                                <Picker.Item
                                    label={'Selectionner un element'}
                                    value={null}
                                />
                                {GENDERS.map((elem, index) => {
                                    return (
                                        <Picker.Item
                                            key={index}
                                            label={elem.label}
                                            value={elem.key}
                                        />
                                    );
                                })}
                            </Picker>
                        </View>
                     
                    </Row>
                    <Row size={10} style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                        <Grid>
                            <Row style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => submit()} style={styles.loginBtn}>
                                    {loading ? <ActivityIndicator size="small" style={styles.loading} /> : <Text style={styles.loginText}>Inscription</Text>}
                                </TouchableOpacity>
                            </Row>
                            <Row style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('login')} >
                                    <Text style={{ color: 'black' }}>CONNEXION</Text>
                                </TouchableOpacity>
                            </Row>

                        </Grid>
                    </Row>
                </Grid>
            </ScrollView>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#ffcc00',
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        width: '100%',
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor : '#ff0000'
    },
    logo: {
        width: 300,
        alignItems: 'center',
        height: 150,
        marginTop: -30,
        // backgroundColor:'green'

    },
    inputView: {
        width: "100%",
        backgroundColor: "white",
        borderRadius: 25,
        borderColor: 'darkgrey',
        borderWidth: 1,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "black"
    },
    forgot: {
        color: "black",
        fontSize: 11
    },
    loginBtn: {
        width: "80%",
        borderColor: 'darkgrey',
        borderWidth: 1,
        backgroundColor: "red",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    loading: {
        color: "black"
    },
    loginText: {
        color: "white"
    }
});
