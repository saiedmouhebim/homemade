import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, Dimensions, View, ImageBackground, TouchableOpacity, TextInput, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import feathersClient from '../../utils/client';
import Header from '../header/header';
import * as Location from 'expo-location';
import { Col, Row, Grid } from "react-native-easy-grid";
import { FontAwesome } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScrollView } from 'react-native-gesture-handler';
import globalStyleSheet from '../globalStyleSheet';
import client from '../../utils/client';
import { Overlay } from 'react-native-elements';
import { Picker } from "@react-native-picker/picker";

let windowHeight = Dimensions.get('window').height;
let windowWidth = Dimensions.get('window').width;

export default class Products extends React.Component {

    state = {
        order: [],
        clients: null,
        loading: true,
        adresse: '',
        payment: null,
        phone: '',
        location: null,
        fullName: '',
        visible: false,
        userId: ''
    }

    componentDidMount() {
        AsyncStorage.getItem('client').then(client => {
            AsyncStorage.getItem('orders').then(orders => {
                AsyncStorage.getItem('userID').then(userId => {
                    AsyncStorage.getItem('location').then(location => {
                        this.setState({
                            location: location ? JSON.parse(location) : null,
                            order: orders ? JSON.parse(orders) : [],
                            userId: userId ? userId : '',
                            clients: client ? JSON.parse(client) : null, loading: true
                        })
                    })
                })
            })
        })
    }

    handleRemoveItem = (index) => {
        let { order } = this.state;
        order.splice(index, 1)
        this.setState({ order })
    }

    getQuantity = (item) => {
        let { order } = this.state;
        let itemIndex = order.findIndex(key => key._id === item._id);
        return itemIndex !== -1 ? order[itemIndex].quantity : 0;
    }

    validateOrder = async () => {
        this.setState({ visible: true })
        let { order, clients, userId } = this.state;
        const items = [];
        if (clients === null) {
            Alert.alert('veuillez selectionner un Chef')
            this.setState({ visible: false })
            return;
        }
        let query = {
            user: userId,
            checkedDate: new Date(Date.now()),
            checked: false,
            deliveryAdress: this.state.adresse,
            deliveryPhone: this.state.phone,
            deliveryName: this.state.fullName,
            payments: this.state.payment,
            deliveryLat: this.state.location.coords.latitude,
            deliveryLog: this.state.location.coords.longitude,
            //    payment: { ref: 'payment', type: Schema.Types.ObjectId },
            synchronised: false,
            valid: false,
            chef: clients._id,
            totalPriceInclTax: this.getPrice(),
            delivered: false,
            items: order,
        }
        client.service('orders').create(query).then(async data => {
            await AsyncStorage.removeItem('orders')
            await AsyncStorage.removeItem('client')
            this.setState({ visible: false })
            this.props.navigation.navigate('orders')
        }).catch(err => console.log(err))
    }

    changeQuantity = (sign, item) => {
        let { order } = this.state;
        const itemIndex = order.findIndex(key => key.dish === item._id)

        if (sign === '+') {
            order[itemIndex].quantity++;
            order[itemIndex].totalPriceInclTax += item.price;
        } else {
            order[itemIndex].quantity--;
            order[itemIndex].totalPriceInclTax -= item.price;
            if (order[itemIndex].quantity === 0) {
                orders.splice(itemIndex, 1);
            }
        }
        this.setState({ orders })
    }

    cancelOrder = async () => {
        let price = 0;
        AsyncStorage.setItem('client', JSON.stringify({})).then(data => {
            AsyncStorage.setItem('orders', JSON.stringify([])).then(data => {
                this.props.navigation.replace('orders')
            })
        })
    }

    getPrice = () => {
        let total = 0;
        let { order } = this.state;
        order.map(elem => total += elem.totalPriceInclTax)
        return total
    }

    getClientName = () => {
        let { clients } = this.state;

        return clients !== null ? clients.username : ''
    }

    render() {

        return (
            <View style={styles.container}>

                <Header
                    routeParams={{ routeName: 'Panier', ...this.props }} />
                {(this.state.loading && this.state.order.length === 0) ? <Text>loading</Text> : <Grid
                    style={{ width: '100%', height: '100%', marginTop: 10 }}>
                    <Row size={6} style={{ borderBottomColor: '#e2e2e2', borderBottomWidth: 1, width: '90%', marginLeft: 10 }}>
                        <Text style={{ fontSize: 17 }}>Total Pannier : {this.getPrice().toFixed(3)} EURO HT</Text>
                    </Row>
                    <Row size={6} style={{ borderBottomColor: '#e2e2e2', borderBottomWidth: 1, width: '90%', marginLeft: 10 }}>
                        <Text style={{ fontSize: 17 }}>Chef : {this.getClientName()} </Text>
                    </Row>
                    <Row size={50} style={{ marginTop: 10 }}>
                        <ScrollView>

                            {this.state.order && this.state.order.length > 0 && <Grid style={{ width: '100%', height: '100%' }}>
                                <ScrollView>
                                    {this.state.order.map((elem, index) => {

                                        return (<View
                                            key={index}
                                            style={{
                                                width: '100%',
                                                marginBottom: 10,
                                                //alignItems: 'center',
                                                height: 160, backgroundColor: '#fff', flexDirection: 'row'
                                            }}
                                        // onpress={() => console.log(item)}
                                        >
                                            <Grid style={{ width: '100%', height: '100%' }}>
                                                <Row size={70}>
                                                    <Col size={10}>
                                                        {/* <View>
                                                        <Text>fds</Text>
                                                        <Image style={{ width: 100, height: 100 }} resizeMode="contain" source={{
                                                        uri: image ? image :
                                                            `http://41.231.122.145:3001/uploads/${item._id}/${uploadedImage.value}`
                                                    }} />
                                                    </View> */}
                                                        <ImageBackground source={require('../../assets/bkg-home-top-opacity.jpg')}
                                                            style={{ width: 120, height: 100 }}>

                                                        </ImageBackground>
                                                    </Col>
                                                    <Col size={20}>
                                                        <Text style={{ fontSize: 17, fontWeight: 'bold', color: 'red', marginTop: 10 }}>
                                                            {elem.menuName.length === 0 ? elem.dishName : elem.menuName}
                                                        </Text>

                                                        <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 20 }}>
                                                            {elem.totalPriceInclTax} EURO</Text>
                                                    </Col>
                                                </Row>

                                                <Row size={25}>
                                                    <Grid style={{ width: '100%', height: '100%' }}>
                                                        <Col size={50}>
                                                            <TouchableOpacity
                                                                style={{ width: '100%', height: '100%' }}
                                                                onPress={() => this.handleRemoveItem(index)}
                                                            >
                                                                <Grid style={{ width: '100%', height: '100%', marginLeft: 18, marginTop: 8 }}>
                                                                    <Col size={10}>
                                                                        <FontAwesome name={'trash'} size={24} color="#b3b3b3" />
                                                                    </Col>
                                                                    <Col size={30} style={{ marginTop: 2 }}>
                                                                        <Text style={{ color: '#b3b3b3' }}>RETIRER</Text>
                                                                    </Col>
                                                                    <Col size={30}>

                                                                    </Col>
                                                                </Grid>
                                                            </TouchableOpacity>
                                                        </Col>
                                                        <Col size={50}>
                                                            <Grid style={{ width: '60%', height: '100%', marginTop: 5 }}>
                                                                <Row>
                                                                    <Col size={10}>
                                                                        <TouchableOpacity
                                                                            onPress={() => this.changeQuantity('-', elem)}
                                                                            style={{
                                                                                width: 35,
                                                                                height: 35, backgroundColor: 'red',
                                                                                borderRadius: 30,
                                                                                justifyContent: 'center', alignItems: 'center'
                                                                            }}
                                                                        >
                                                                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#fff' }}>-</Text>
                                                                        </TouchableOpacity>
                                                                    </Col>
                                                                    <Col size={20} style={{ alignItems: 'center' }}>
                                                                        <Text style={{ marginTop: 5 }}>{this.getQuantity(elem)}</Text>
                                                                    </Col>
                                                                    <Col size={10}>
                                                                        <TouchableOpacity
                                                                            onPress={() => this.changeQuantity('+', elem)}
                                                                            style={{

                                                                                width: 35, height: 35,
                                                                                borderRadius: 30,
                                                                                justifyContent: 'center',
                                                                                backgroundColor: 'red', alignItems: 'center'
                                                                            }}
                                                                        >
                                                                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff' }}>+</Text>
                                                                        </TouchableOpacity>
                                                                    </Col>
                                                                </Row>
                                                            </Grid>
                                                        </Col>
                                                    </Grid>
                                                </Row>
                                                <Row size={2} style={{ alignItems: 'center', justifyContent: 'center', marginTop: 8 }}>
                                                    <View style={{ backgroundColor: '#d5d5d5', width: '90%', height: 1 }}></View>
                                                </Row>
                                            </Grid>
                                        </View>)
                                    })}

                                </ScrollView>
                            </Grid>}
                        </ScrollView>
                    </Row>
                    <Row size={8} style={{ alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 5 }}>

                        <TouchableOpacity
                            onPress={() => this.cancelOrder()}
                            style={{
                                justifyContent: 'center',
                                borderRadius: 3,
                                width: '38%',
                                alignItems: 'center',

                                height: '100%', backgroundColor: 'red'
                            }}>

                            <View style={globalStyleSheet.iconAndTextWrapper}>
                                <Icon name="times" size={20} style={globalStyleSheet.iletyle} />
                                <Text style={globalStyleSheet.bigButtonText}>Annuler</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.setState({ visible: true })}
                            style={{
                                justifyContent: 'center',
                                borderRadius: 3,
                                width: '58%',
                                alignItems: 'center',
                                height: '100%',
                                backgroundColor: 'green'
                            }}>


                            <View style={globalStyleSheet.iconAndTextWrapper}>
                                <Icon name="check" size={20} style={globalStyleSheet.iletyle} />


                                <Text style={globalStyleSheet.bigButtonText}>Valider</Text>
                            </View>
                        </TouchableOpacity>
                    </Row>
                </Grid>}
                <Overlay overlayStyle={{ width: 281, height: 300 }} isVisible={this.state.visible} >
                    <ScrollView style={{ width: '100%', height: '100%' }}>
                        <Grid style={{ width: '100%', height: '100%' }}>
                            <Row size={10}>
                                <Text>SVp! Veuillez remplir ces champs</Text>
                            </Row>
                            <Row size={30} style={{ marginTop: 10 }}>
                                <View style={styles.inputView} >
                                    <TextInput
                                        style={styles.inputText}
                                        placeholder="Veuillez remplire votre adresse"
                                        value={this.state.adresse}
                                        placeholderTextColor="#003f5c"
                                        onChangeText={text => this.setState({ adresse: text })} />
                                </View>
                            </Row>
                            <Row size={30} style={{ marginTop: 10 }}>
                                <View style={styles.inputView} >
                                    <TextInput
                                        style={styles.inputText}
                                        placeholder="Veuillez remplire votre nom et prénom"
                                        value={this.state.fullName}
                                        placeholderTextColor="#003f5c"
                                        onChangeText={text => this.setState({ fullName: text })} />
                                </View>
                            </Row>
                            <Row size={30} style={{ marginTop: 10 }}>
                                <View style={styles.inputView} >
                                    <TextInput
                                        style={styles.inputText}
                                        placeholder="Veuillez remplire votre téléphone"
                                        value={this.state.phone}
                                        placeholderTextColor="#003f5c"
                                        onChangeText={text => this.setState({ phone: text })} />
                                </View>
                            </Row>
                            <Row size={30} style={{ marginTop: 10 }}>
                                <Picker
                                    selectedValue={this.state.payment}
                                    style={{ height: 50, width: "100%" }}
                                    onValueChange={(itemValue, itemIndex) => {
                                        this.setState({ payment: itemValue })
                                    }}
                                >
                                    <Picker.Item
                                        label={'Selectionner un mode payment'}
                                        value={null}
                                    />
                                    <Picker.Item
                                        label={'cash'}
                                        value={'cash'}
                                    />
                                    <Picker.Item
                                        label={'paypal'}
                                        value={'paypal'}
                                    />
                                </Picker>
                            </Row>
                            <Row size={40} style={{ marginTop: 10 }}>
                                <TouchableOpacity
                                    onPress={() => this.validateOrder()}
                                    style={{
                                        justifyContent: 'center',
                                        borderRadius: 3,
                                        width: '100%',
                                        alignItems: 'center',
                                        height: '100%', backgroundColor: 'green'
                                    }}>

                                    <View style={globalStyleSheet.iconAndTextWrapper}>
                                        <Icon name="check" size={20} style={globalStyleSheet.iletyle} />
                                        <Text style={globalStyleSheet.bigButtonText}>Valider</Text>
                                    </View>
                                </TouchableOpacity>
                            </Row>
                        </Grid>
                    </ScrollView>
                </Overlay>
            </View>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    inputView: {
        width: "100%",
        backgroundColor: "white",
        borderRadius: 25,
        borderColor: 'darkgrey',
        borderWidth: 1,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "black"
    },
});
