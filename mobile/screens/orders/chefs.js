import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image, ScrollView, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import Header from '../header/header';
import { getSkipFromLimitAndPage } from '../../utils/const';
import globalStyleSheet from '../globalStyleSheet'
import { SearchBar } from "react-native-elements";
import { Col, Row, Grid } from "react-native-easy-grid";
import RegexEscape from "regex-escape";

export default function Clients(props) {

    const [clients, setClients] = useState([]);
    const [page, setPage] = useState(0);
    const [limit, setLimit] = useState(20);
    const [total, setTotal] = useState(0);
    const [loading, setLoading] = useState(true);
    const [search, setSearch] = useState('');

    useEffect(() => {
        getData('');
    }, [])

    const getData = async (searchquery) => {
        //console.log(searchquery)
        console.log(props)
        const query = {
            query: {
                $limit: 10,
                $populate: ['zone'],
                $skip: 0,
                type: 'cfo',
                zone: props.route.params.id
            }
        }
        const clientList = await client.service('users').find(query);
        
        setClients(clientList.data);
        setPage(1)
        setTotal(clientList.total);
        setLoading(false);

    }

    function renderMoreItems() {
        if (total > clients.length) {
            const query = {
                query: {
                    $limit: 10,
                    $populate: ['zone'],
                    $skip: 10 * page,
                    type: 'cfo',
                    zone: props.route.params.id
                }
            }
            client.service('client').find(query).then((clientData) => {
                setClients([...clients, ...clientData.data]);
                setPage(page + 1)
                setLoading(false);

            }).catch(err => console.log(err))
        }
    }

    const clientChecked = (client) => {
        delete client.photo
        AsyncStorage.setItem('orders', JSON.stringify([])).then(()=>{
            AsyncStorage.setItem('client',JSON.stringify(client)).then(data=>{
                props.navigation.navigate('menu',{id: client._id})
            })
        })
    }

    function renderItem({ item, index }) {
        return (
            <TouchableOpacity style={

                [globalStyleSheet.classicFlatList,
                { backgroundColor: index % 2 === 0 ? globalStyleSheet.oddLine.backgroundColor : globalStyleSheet.notOddLine.backgroundColor }]
            }
                onPress={() => clientChecked(item)}>

                <View style={{ width: '20%', marginHorizontal: 10 }}>
                    <Image style={globalStyleSheet.logoFlatList} source={{
                        uri: item.photo
                    }} />

                </View>
                <View style={{ width: '80%', flexDirection: 'column' }}>
                    <Text style={{ color: 'rgba(205, 90, 146, 0.8)', fontWeight: 'bold', fontSize: 20 }}>{item.username}</Text>
                    <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 15 }}>{item.gender}</Text>
                </View>
            </TouchableOpacity>
        );
    }
    //console.log(clients)
    return (
        <View style={styles.container}>
            <Header
                routeParams={{ routeName: 'Chef cuisiner', ...props }} />
            <View style={styles.itemsContainer}>
                <FlatList
                    onEndReachedThreshold={0.5}
                    keyExtractor={item => item._id}
                    onEndReached={() => {
                        renderMoreItems()
                    }}
                    contentContainerStyle={styles.list}
                    data={clients}
                    renderItem={renderItem}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    itemsContainer: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
    }
});
