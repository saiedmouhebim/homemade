import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, ScrollView, ImageBackground, ActivityIndicator } from 'react-native';
import Header from '../header/header'
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import { getDistance, getPreciseDistance } from 'geolib';
import { Col, Row, Grid } from "react-native-easy-grid";
import Draggable from 'react-native-draggable';
const globalStyleSheet = require('../globalStyleSheet.js').default
import BottomSheet, {
    BottomSheetScrollView,
    TouchableOpacity
} from '@gorhom/bottom-sheet';

export default class Home extends React.Component {

    state = {
        dishes: [],
        page: 0,
        orders: [],
        limit: 20,
        clients: null,
        total: 0,
        loading: true
    }
    bottomSheetRef = React.createRef();

    componentDidMount() {
        this.getData();
    };

    getData = async () => {
        const query = {
            query: {
                _id: this.props.route.params.menu,
                $select: ['dishes', 'name, price', 'quantity'],
                $populate: ['dishes']
            }
        }
        client.service('menus').find(query).then(dishes => {
            AsyncStorage.getItem('orders').then(async(currentOrders) => {
                const clients = await AsyncStorage.getItem('client');

                this.setState({ dishes: dishes.data[0].dishes, orders: currentOrders ? JSON.parse(currentOrders) : [],clients: clients ? JSON.parse(clients) : null  })
            })
        })
    }

    existQuantity = (item) => {
        const itemIndex = this.state.orders.findIndex(key => key.dish === item._id)
        return itemIndex !== -1
    }

    getQuantity = (item) => {
        const itemIndex = this.state.orders.findIndex(key => key.dish === item._id)
        return this.state.orders[itemIndex].quantity
    }

    addItem = async (item) => {
        const query = {
            dish: item._id,
            dishName: item.name,
            "menu": null,
            "menuName": "",
            quantity: 1,
            totalPriceInclTax: item.price
        }
        let { orders } = this.state;
        orders.push(query);
        AsyncStorage.setItem('orders', JSON.stringify(orders)).then(() => {
                            this.bottomSheetRef.current.snapTo(2)

            this.setState({ orders })
        })
    }

    changeQuantity = async (item, sign) => {
        let { orders } = this.state;
        const itemIndex = orders.findIndex(key => key.dish === item._id)

        if (sign === '+') {
            orders[itemIndex].quantity++;
            orders[itemIndex].totalPriceInclTax += item.price;
        } else {
            orders[itemIndex].quantity--;
            orders[itemIndex].totalPriceInclTax -= item.price;
            if (orders[itemIndex].quantity === 0) {
                orders.splice(itemIndex, 1);
            }
        }
        AsyncStorage.setItem('orders', JSON.stringify(orders)).then(() => {
            this.setState({ orders })
        })
    }

    handleNavigator = (item) => {
        this.props.navigation.navigate('dishes', { menu: item })
    }

    getClientName = () => {
        let {clients} = this.state;

        return clients !== null ? clients.username : ''
    }

    getPrice = () => {
        let total = 0;
        let {orders} = this.state;
        orders.map(elem =>total += elem.totalPriceInclTax)
        return total
    }

    renderItem = ({ item, index }) => {

        return (
            <View style={[globalStyleSheet.line, index % 2 === 0 ? globalStyleSheet.oddLine : globalStyleSheet.notOddLine]
            }
               // onPress={() => this.handleNavigator(item)}
            >
                <ImageBackground source={{
                    uri: item.photo
                }}
                    resizeMode={'stretch'}
                    style={{ flex: 1 }}>
                    <Grid style={{ width: '100%', height: '100%', paddingLeft: 20 }}>

                        <Grid style={{ width: '100%', height: '100%', marginTop: 8 }}>
                            <Row size={10}>
                                <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 20 }}>Plat: {item.name}</Text>
                            </Row>
                            <Row size={10}>
                                <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 15 }}>Prix: {item.price} EUR</Text>
                            </Row>
                            {this.existQuantity(item) ? <Row size={20}>
                                <Col size={10} style={{}}>
                                    <TouchableOpacity
                                        onPress={() => this.changeQuantity(item, '-')}
                                        style={{
                                            backgroundColor: "red",
                                            justifyContent: "center",
                                            borderRadius: 3,
                                            height: 35,
                                            alignItems: "center",
                                        }}>
                                        <Text>-</Text>
                                    </TouchableOpacity>
                                </Col>
                                <Col size={10} style={{ alignItems: 'center', }}><Text style={{ textAlign: 'center', marginTop: 8, color: 'red' }}>{this.getQuantity(item)}</Text></Col>
                                <Col size={10}><TouchableOpacity
                                    onPress={() => this.changeQuantity(item, '+')}
                                    style={{
                                        backgroundColor: "red",
                                        justifyContent: "center",
                                        borderRadius: 3,
                                        height: 35,
                                        alignItems: "center",
                                    }}>
                                    <Text>+</Text>
                                </TouchableOpacity></Col>
                                <Col size={50}></Col>
                            </Row> : <Row size={20}>
                                <TouchableOpacity
                                    onPress={() => this.addItem(item)}
                                    style={{
                                        backgroundColor: "red",
                                        justifyContent: "center",
                                        borderRadius: 3,
                                        height: 35,
                                        width: 120,
                                        alignItems: "center",
                                    }}>
                                    <Text>Ajouter</Text>
                                </TouchableOpacity>
                            </Row>}
                        </Grid>
                    </Grid>

                </ImageBackground>
            </View>
        );
    }

    reset = () => {
        AsyncStorage.removeItem('client').then(()=> {
            AsyncStorage.removeItem('orders').then(()=> {
                this.props.navigation.navigate('orders')
            }) 
        })
    }

    render() {
        const snapPoints = ['0.1%', '25%', '50%', '75%', '100%']

        return (
            <View style={styles.container}>
                <Header
                    routeParams={{ routeName: 'Plats ', ...this.props }} />
                <View style={styles.itemsContainer}>
                    <FlatList
                        onEndReachedThreshold={0.5}
                        keyExtractor={item => item._id}

                        contentContainerStyle={styles.list}
                        data={this.state.dishes}
                        renderItem={this.renderItem}
                    />
                </View>
                <BottomSheet
                    ref={this.bottomSheetRef}
                    index={0}
                    snapPoints={snapPoints}
                // onChange={(ev) => console.log(ev)}
                >
                    <BottomSheetScrollView>
                    <Grid style={{ width: '100%', height: '100%', paddingLeft: 10 }}>
                            <Row size={10}>

                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Total panier: {this.getPrice().toFixed(3)} EURO</Text>
                            </Row>

                            <Row size={10} style={{ marginTop: 10, marginBottom: 10 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Client : {this.getClientName()} </Text>
                            </Row>
                            <Row height={1} backgroundColor='#dcdcdc'></Row>
                            <Row size={5} style={{ marginTop: 20 }}>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>Article</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>Qte</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>Prix</Text>
                                </Col>
                            </Row>
                            {this.state.orders.map((elem,index) => {
                                return (<Row key={index} size={5} style={{ marginTop: 20 }}>
                                <Col size={25}>
                                <Text style={{ fontWeight: 'bold' }}>{elem.menuName.length === 0? elem.dishName: elem.menuName}</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>{elem.quantity}</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>{elem.totalPriceInclTax} EUR</Text>
                                </Col>
                            </Row>)
                            })}
                             <Row size={25} style={{ marginBottom: 10, marginTop: 30, justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('orderS')}
                                    style={{
                                        width: 150, margin: 3,
                                        alignItems: 'center',
                                        borderRadius: 50,
                                        height: '100%', backgroundColor: 'green'
                                    }}>
                                    <Text style={{ fontSize: 25,color:'#fff' }}>Valider</Text>
                                </TouchableOpacity>
                            </Row>
                            <Row size={25} style={{ marginBottom: 10, justifyContent: 'center' }}>

                                <TouchableOpacity
                                    onPress={() => this.reset()}
                                    style={{
                                        width: 150, margin: 3,
                                        alignItems: 'center',
                                        borderRadius: 50,
                                        height: '100%', backgroundColor: 'red'
                                    }}>
                                    <Text style={{ fontSize: 25,color:'#fff' }}>Annuler</Text>
                                </TouchableOpacity>
                            </Row>
                        </Grid>
                    </BottomSheetScrollView>
                </BottomSheet>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    itemsContainer: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
    }
});
