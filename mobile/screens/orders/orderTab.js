import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList,TouchableHighlight, Image } from 'react-native';
import Header from '../header/header'
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import { getDistance, getPreciseDistance } from 'geolib';
import { Col, Row, Grid } from "react-native-easy-grid";
import Draggable from 'react-native-draggable';
import { padStart } from "lodash"
import Moment from 'moment';
import { Icon } from "react-native-elements";
import 'moment/locale/fr'; Moment.locale('fr');
const globalStyleSheet = require('../globalStyleSheet.js').default
export default class Home extends React.Component {

    state = {
        orders: [],
        page: 0,
        limit: 20,
        total: 0,
        loading: true
    }

    componentDidMount() {
        this.getData();
    };

    getData = async () => {
        const userId = await AsyncStorage.getItem('userID')
        console.log(userId)
        const query = {
            query: {
                $limit: 10,
                user: userId,
                $populate: ['chef'],
                $skip: 0
            }
        }
        try {
            const orders = await client.service('orders').find(query);
            this.setState({
                loading: false, orders: orders.data,
                page: 1, total: orders.total
            })

        } catch (err) {
            console.log(err)
        }
    }

    renderMoreItems = async () => {
        if (this.state.total > this.state.orders.length) {
            const userId = await AsyncStorage.getItem('userID')
            const query = {
                query: {
                    $limit: 10,
                    user: userId,
                    $populate: ['chef'],
                    $skip: 10 * this.state.page
                }
            }
            client.service('orders').find(query).then((orders) => {

                this.setState({
                    loading: false, orders: [...this.state.orders, ...orders.data],
                    page: this.state.page
                })
            }).catch(err => console.log(err))

        }
    }

    renderItem({ item, index }) {

        return (
            <TouchableHighlight
            style={[item.checked ? styles.active : styles.inactive,
            styles.Animatable,
            ]}
            underlayColor={'lightgrey'}
        >
            <View>
                <View style={{ flex: 95, flexDirection: 'row' }}>
                    <View style={{
                        flex: 15,

                        // backgroundColor:'grey',
                        justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
                    }}
                    >
                        <Image style={styles.imageItem}
                            source={{ uri: item.chef.photo }} />
                    </View>
                    <View style={{
                        flex: 42,
                        // backgroundColor:'brown',
                        justifyContent: 'center', paddingRight: 7, alignSelf: 'center'
                    }}
                    >
                        <View style={{
                            flexDirection: 'row'
                            , justifyContent: 'space-between'
                        }} >
                            <View style={{ flexDirection: 'row' }}><Text style={{ color: '#c2c2c2', fontSize: 15, fontStyle: 'italic', fontWeight: 'bold' }} >CDE</Text>
                                <Text style={{ color: '#cd5a93', fontWeight: 'bold', fontSize: 16, fontWeight: 'bold' }}>#{padStart(item.ref, 4, 0)}</Text></View>
                            <View style={{ alignSelf: 'flex-end' }}><Text style={{ color: 'green', fontSize: 9, fontWeight: 'bold' }}>VALIDEE</Text></View>
                        </View>
                        <View><Text numberOfLines={1} style={{
                            letterSpacing: 0, alignItems: 'stretch', fontSize: 17, fontWeight: 'bold', color: '#474747',
                            // backgroundColor:'yellow',
                            textAlign: 'right',
                        }}>{item.chef.firstName + ' ' + item.chef.lastName}</Text></View>
                        <Text style={{ color: item.checked ? 'green' : 'red', fontSize: 9, fontWeight: 'bold', textAlign: 'right' }}>{item.checked ? 'payée' : 'non payée'}</Text>
                    </View>
                    <View style={{
                        flex: 23,
                        // backgroundColor:'white' ,
                        justifyContent: 'center', alignSelf: 'center'
                    }}
                    >
                        <View><Text style={{ color: '#c2c2c2', fontWeight: '900', textAlign: 'right', fontWeight: 'bold' }}>Total</Text></View>
                        <View><Text style={{ fontWeight: 'bold', fontSize: 15, color: '#ff0000', textAlign: 'right' }}>{parseFloat(item.totalPriceInclTax).toFixed(0)} EUR</Text></View>

                    </View>
                    <View style={{
                        flex: 15,
                        // backgroundColor:'green',
                        justifyContent: 'center'
                    }}
                    >
                        <View><Text style={{ color: '#474747', fontWeight: 'bold', textAlign: 'center' }}>{Moment(item.createdAt).format('YYYY')}</Text></View>
                        <View><Text style={{ color: '#cd5a93', fontWeight: 'bold', fontSize: 26, textAlign: 'center', fontStyle: 'italic' }}>{Moment(item.createdAt).format('DD')}</Text></View>
                        <View><Text style={{ color: '#4d4d4d', fontWeight: 'bold', textAlign: 'center', alignItems: 'baseline' }}>{Moment(item.createdAt).format('MMMM').substring(0, 4).toUpperCase()}</Text></View>
                    </View>
                </View>
                <View style={{ marginTop: 5, marginRight: 3, flex: 5, flexDirection: 'row', alignSelf: 'flex-end' }}>
                    <Icon name="history" size={18} color='#808080' />
                    <Text style={{ marginLeft: 3, fontSize: 12, color: '#b0b0b0', fontWeight: 'bold' }}>{'Ajoutée il y a environ '}
                        {Moment(item.createdAt).toNow(true)}
                    </Text>
                </View>
            </View>
        </TouchableHighlight>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Header
                    routeParams={{ routeName: 'Commandes', ...this.props }} />
                <View style={styles.itemsContainer}>
                    <FlatList
                        onEndReachedThreshold={0.5}
                        keyExtractor={item => item._id}
                        onEndReached={() => {
                            this.renderMoreItems()
                        }}
                        contentContainerStyle={styles.list}
                        data={this.state.orders}
                        renderItem={this.renderItem}
                    />
                     <Draggable>
                    <TouchableHighlight
                         underlayColor={'rgba(205, 90, 146,0.5)'}
                        onPress={() => this.props.navigation.navigate('zones')}
                        style={{
                            width: 60, height: 60, backgroundColor: 'rgb(205, 90, 146)',
                            justifyContent: 'center', alignItems: 'center', borderRadius: 30, elevation: 5
                        }}>
                        <Text style={{ fontSize: 35, fontWeight: 'bold', color: 'white' }}>+</Text>

                    </TouchableHighlight>
                </Draggable>
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    itemsContainer: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
    }
});
