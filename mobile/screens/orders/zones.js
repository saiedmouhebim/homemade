import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Location from 'expo-location';
import * as Notifications from 'expo-notifications';
import client from '../../utils/client';
import { SearchBar } from 'react-native-elements';
import RegexEscape from "regex-escape";
const globalStyleSheet = require('../globalStyleSheet.js').default
import { getDistance, getPreciseDistance } from "geolib";
import { FontAwesome } from '@expo/vector-icons';
import Header from '../header/header'

export default class Home extends React.Component {
    state = {
        search: '',
        zones: [],
        location: null,
    };


    componentDidMount() {
        const adresse = AsyncStorage.getItem('location').then(location => {
            this.setState({ location: JSON.parse(location) })
        })
    };

    updateSearch = (search) => {
        this.setState({ search });
        this.adresseSearch(search);
    };

    adresseSearch = (search) => {
        const fieldQueries = search.split(/ +/).reduce((queries, word) => {
            if (word) {
                queries.push({
                    city: { $regex: RegexEscape(word), $options: 'i' }
                });
            }
            return queries;
        }, []);
        const query = {
            query: {
                $limit: -1,
                $and: fieldQueries
            }
        }
        client.service('zones').find(query).then(zones => {
            this.setState({ zones });
        })
    }

    renderItem = ({ item, index }) => {
        const distance = getPreciseDistance(
            {
                latitude: item.lat,
                longitude: item.log,
            },
            {
                latitude: this.state.location.coords.latitude,
                longitude: this.state.location.coords.longitude,
            },
            1
        );
        return (
            <TouchableOpacity style={[globalStyleSheet.line, index % 2 === 0 ? globalStyleSheet.oddLine : globalStyleSheet.notOddLine]
            }
             onPress={() => this.props.navigation.navigate('chefs', { id: item._id })}
            >

                <View style={{ width: '10%', marginLeft: 20 }}>
                    <FontAwesome name="map-marker" size={30} color="red" />
                </View>
                <View style={{ width: '90%', flexDirection: 'column' }}>
                    <Text style={globalStyleSheet.firtLine}>state: {item.state}</Text>
                    <Text style={globalStyleSheet.secondLine}>city : {item.city}</Text>
                    <Text style={{ color: '#dcdcdc', fontWeight: 'bold' }}>distance: {distance.toString()} m</Text>
                </View>
            </TouchableOpacity>
        );
    }

    getDistance(item) {
        return '2';
    }

    render() {
        const { search } = this.state;
        return (
            <View style={styles.container}>
                <StatusBar hidden style="auto" />
                <Header
                    routeParams={{ routeName: 'Zonage', ...this.props }} />
                <View style={styles.itemsContainer}>

                    <View style={{ width: '100%', height: 80, backgroundColor: '#282828', justifyContent: 'center' }}>
                        <SearchBar
                            placeholder="Rechercher Une zone"
                            onChangeText={this.updateSearch}
                            value={search}
                        />
                    </View>
                    <FlatList
                        onEndReachedThreshold={0.5}
                        keyExtractor={item => item._id}

                        contentContainerStyle={styles.list}
                        data={this.state.zones}
                        renderItem={this.renderItem}
                    />
                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    itemsContainer: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
    }
});
