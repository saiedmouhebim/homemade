import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, ImageBackground, ActivityIndicator } from 'react-native';
import Header from '../header/header'
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import { getDistance, getPreciseDistance } from 'geolib';
import { Col, Row, Grid } from "react-native-easy-grid";
import Draggable from 'react-native-draggable';
import BottomSheet, {
    BottomSheetScrollView,
    TouchableOpacity
} from '@gorhom/bottom-sheet';
const globalStyleSheet = require('../globalStyleSheet.js').default


export default class Home extends React.Component {

    state = {
        menus: [],
        page: 0,
        limit: 20,
        clients: null,
        total: 0,
        orders : [],
        loading: true
    }
    bottomSheetRef = React.createRef();

    componentDidMount() {
        this.getData();
    };

    getData = async () => {
        const query = {
            query: {
                $limit: 10,
                $skip: 0,
                chef: this.props.route.params.id
            }
        }
        try {
            const orders = await client.service('menus').find(query);
            const currentOrders = await AsyncStorage.getItem('orders');
            const clients = await AsyncStorage.getItem('client');
            this.setState({
                loading: false, menus: orders.data,
                clients: clients ? JSON.parse(clients) : null,
                page: 1, total: orders.total, orders: currentOrders ? JSON.parse(currentOrders) : []
            })

        } catch (err) {
            console.log(err)
        }
    }

    renderMoreItems = () => {
        if (this.state.total > this.state.menus.length) {
            const query = {
                query: {
                    $limit: 10,
                    $skip: 10 * this.state.page,
                    chef: this.props.route.params.id
                }
            }
            client.service('menus').find(query).then((orders) => {
                this.setState({
                    loading: false, menus: [...this.state.menus, ...orders.data],
                    page: this.state.page
                })
            }).catch(err => console.log(err))

        }
    }

    existQuantity = (item) => {
        const itemIndex = this.state.orders.findIndex(key => key.menu === item._id)
        return itemIndex !== -1
    }

    getQuantity = (item) => {
        const itemIndex = this.state.orders.findIndex(key => key.menu === item._id)
        return this.state.orders[itemIndex].quantity
    }

    addItem = async (item) => {
        const query = {
            menu: item._id,
            menuName : item.name,
            dish: null,
            dishName: "",
            quantity: 1,
            totalPriceInclTax: item.price
        }
        let { orders } = this.state;
        orders.push(query);
        AsyncStorage.setItem('orders', JSON.stringify(orders)).then(() => {
            this.setState({ orders })
        })
    }

    changeQuantity = async (item, sign) => {
        let { orders } = this.state;
        const itemIndex = orders.findIndex(key => key.menu === item._id)

        if (sign === '+') {
            orders[itemIndex].quantity++;
            orders[itemIndex].totalPriceInclTax += item.price;
        } else {
            orders[itemIndex].quantity--;
            orders[itemIndex].totalPriceInclTax -= item.price;
            if (orders[itemIndex].quantity === 0) {
                orders.splice(itemIndex, 1);
            }
        }
        AsyncStorage.setItem('orders', JSON.stringify(orders)).then(() => {
            this.setState({ orders })
        })
    }

    handleNavigator = (item) => {
        this.props.navigation.navigate('dishes', { menu: item._id })
    }

    renderItem = ({ item, index }) => {

        return (
            <TouchableOpacity style={[globalStyleSheet.line, index % 2 === 0 ? globalStyleSheet.oddLine : globalStyleSheet.notOddLine]
            }
                onPress={() => this.handleNavigator(item)}
            >
                <ImageBackground source={{
                    uri: item.cover
                }}
                    resizeMode={'contain'}
                    style={{ flex: 1 }}>
                    <Grid style={{ width: '100%', height: '100%', paddingLeft: 20 }}>

                        <Grid style={{ width: '100%', height: '100%', marginTop: 8 }}>
                            <Row size={10}>
                                <Text style={{ color: 'rgba(205, 90, 146, 0.8)', fontWeight: 'bold', fontSize: 20 }}>Plat: {item.name}</Text>
                            </Row>
                            <Row size={10}>
                                <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 15 }}>Prix: {item.price} EUR</Text>
                            </Row>
                           
                        </Grid>
                    </Grid>

                </ImageBackground>
            </TouchableOpacity>
        );
    }

    getClientName = () => {
        let {clients} = this.state;

        return clients !== null ? clients.username : ''
    }

    getPrice = () => {
        let total = 0;
        let {orders} = this.state;
        orders.map(elem =>total += elem.totalPriceInclTax)
        return total
    }

    reset = () => {
        AsyncStorage.removeItem('client').then(()=> {
            AsyncStorage.removeItem('orders').then(()=> {
                this.props.navigation.navigate('orders')
            }) 
        })
    }

    render() {
        const snapPoints = ['0.1%', '25%', '50%', '75%', '100%']

        return (
            <View style={styles.container}>
                <Header
                    routeParams={{ routeName: 'Menu', ...this.props }} />
                <View style={styles.itemsContainer}>
                    <FlatList
                        onEndReachedThreshold={0.5}
                        keyExtractor={item => item._id}
                        onEndReached={() => {
                            this.renderMoreItems()
                        }}
                        contentContainerStyle={styles.list}
                        data={this.state.menus}
                        renderItem={this.renderItem}
                    />
                </View>
                <BottomSheet
                    ref={this.bottomSheetRef}
                    index={0}
                    snapPoints={snapPoints}
                // onChange={(ev) => console.log(ev)}
                >
                    <BottomSheetScrollView>
                        <Grid style={{ width: '100%', height: '100%', paddingLeft: 10 }}>
                            <Row size={10}>

                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Total panier: {this.getPrice().toFixed(3)} EURO</Text>
                            </Row>

                            <Row size={10} style={{ marginTop: 10, marginBottom: 10 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>Client : {this.getClientName()} </Text>
                            </Row>
                            <Row height={1} backgroundColor='#dcdcdc'></Row>
                            <Row size={5} style={{ marginTop: 20 }}>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>Article</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>Qte</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>Prix</Text>
                                </Col>
                            </Row>
                            {this.state.orders.map((elem,index) => {
                                return (<Row key={index} size={5} style={{ marginTop: 20 }}>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>{elem.menuName.length === 0? elem.dishName: elem.menuName}</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>{elem.quantity}</Text>
                                </Col>
                                <Col size={25}>
                                    <Text style={{ fontWeight: 'bold' }}>{elem.totalPriceInclTax} EUR</Text>
                                </Col>
                            </Row>)
                            })}
                            <Row size={25} style={{ marginBottom: 10, marginTop: 30, justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('orderS')}
                                    style={{
                                        width: 150, margin: 3,
                                        alignItems: 'center',
                                        borderRadius: 50,
                                        height: '100%', backgroundColor: 'green'
                                    }}>
                                    <Text style={{ fontSize: 25,color:'#fff' }}>Valider</Text>
                                </TouchableOpacity>
                            </Row>
                            <Row size={25} style={{ marginBottom: 10, justifyContent: 'center' }}>

                                <TouchableOpacity
                                    onPress={() => this.reset()}
                                    style={{
                                        width: 150, margin: 3,
                                        alignItems: 'center',
                                        borderRadius: 50,
                                        height: '100%', backgroundColor: 'red'
                                    }}>
                                    <Text style={{ fontSize: 25,color:'#fff' }}>Annuler</Text>
                                </TouchableOpacity>
                            </Row>
                        </Grid>
                    </BottomSheetScrollView>
                </BottomSheet>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    itemsContainer: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
    }
});
