import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity, ImageBackground, Image, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
const globalStyleSheet = require('../globalStyleSheet.js').default
import { FontAwesome } from '@expo/vector-icons';
import Header from '../header/header'
import { Col, Row, Grid } from "react-native-easy-grid";
import { Avatar, Card, Badge, Rating } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import * as Application from 'expo-application';

export default class Home extends React.Component {
    state = {
        chef: null,
        menus: [],
        dishes: [],
        sum: 0,
        reviews: [],
        location: null,
    };

    componentDidMount() {
        this.getChef();
        this.getMenusAndDishes();
        this.reviews();
    };

    getChef = async () => {
        const chef = await client.service('users').get(this.props.route.params.id)
        this.setState({ chef })
    }

    getMenusAndDishes = async () => {

        const query = {
            query: {
                $limit: -1,
                chef: this.props.route.params.id,
                $populate: ['dishes'],
                // day: {
                //     $gte: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
                //     $lt: new Date(date.getFullYear(), date.getMonth(), date.getDate() + 2)
                // }
            }
        }
        const menus = await client.service('menus').find(query)
        //console.log(menus)
        this.setState({ menus: menus, dishes: menus.length > 0 ? menus[0].dishes : [] })
    }

    reviews = () => {
        console.log(Application.androidId)
        const query = {
            query: {
                $limit: -1,
                target: this.props.route.params.id,
                user: Application.androidId
            }
        }
        client.service('reviews').find(query).then(reviews => {
            this.setState({ reviews, sum: reviews.length > 0 ? reviews[0].value : 0 })
        })
    }

    renderItem({ item, index }) {
        return <View
            key={index}
            style={{
                flex: 1,
                margin: 5,
                minWidth: '45%',
                maxWidth: '45%',
                height: 200,
                maxHeight: 200,
            }} >
            <Badge
                containerStyle={{ position: 'absolute', height: 20, top: 9, right: -4, elevation: 99, }}
                value={item.price + ' EUR'} status="error" />

            <Card>
                <Card.Title>{item.name}</Card.Title>
                <Card.Divider />

                <Card.Image resizeMode={'contain'} style={{ width: '100%', height: 100 }} height={100} source={{ uri: item.photo }}>
                    <Text style={{ marginBottom: 10, color: 'red', fontSize: 9 }}>
                        {item.description}
                    </Text>
                </Card.Image>

            </Card>

        </View>
    }

    ratingCompleted = (rate) => {
        this.setState({ sum: rate })
        let { reviews } = this.state;
        if (reviews.length === 0) {
            const query = {
                value: rate,
                user: Application.androidId,
                target: this.props.route.params.id,
            }
            client.service('reviews').create(query).then(data =>{
                Alert.alert('rate updated')
            })
        } else {
            reviews[0].value = rate
            client.service('reviews').update(reviews[0]._id, reviews[0]).then(data =>{
                Alert.alert('rate updated')
            })
        }
    }

    render() {
        console.log(this.state.sum)
        return (
            <View style={styles.container}>
                <StatusBar hidden style="auto" />
                <Header
                    routeParams={{ routeName: 'Détail', ...this.props }} />
                <ImageBackground source={require('../../assets/bkg-home-top-opacity.jpg')}
                    style={{ flex: 1 }}>
                    <View style={styles.itemsContainer}>
                        <ScrollView style={{ width: '100%', height: '100%' }}>
                            {this.state.chef && <Grid style={{ width: '100%', height: '100%' }}>
                                <Row size={20} style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                    <Avatar
                                        rounded
                                        size="large"

                                        source={{
                                            uri:
                                                this.state.chef.photo,
                                        }}
                                    />
                                </Row>
                                <Row size={10} style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                    <Text>{this.state.chef.gender.toUpperCase() + ' '}</Text><Text>{this.state.chef.firstName + '  ' + this.state.chef.lastName}</Text>
                                </Row>
                                <Row size={10} style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                    <Text>{this.state.chef.email}</Text>
                                </Row>
                                <Row size={20} style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                    <Rating
                                        //showRating
                                        defaultRating={this.state.sum}
                                        ratingColor='red'
                                        startingValue={this.state.sum}
                                        onFinishRating={(rate) => this.ratingCompleted(rate)}
                                        style={{ paddingVertical: 10 }}
                                    />
                                </Row>
                                <Row size={20} style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                                    <Text style={{color : 'red', fontSize : 16, fontWeight : 'bold'}}>Menu de jour</Text>
                                </Row>
                                <Row size={30} style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                    <ScrollView style={{ width: '100%', height: '100%' }}
                                        horizontal={true}
                                    >
                                        {this.state.menus.map((elem, index) => {
                                            return (
                                                <TouchableOpacity key={index}
                                                    onPress={() => this.setState({ dishes: elem.dishes })}
                                                    style={{ width: 150, height: 150, borderRadius: 30, marginLeft: 10 }}>
                                                    <ImageBackground source={{ uri: elem.cover }} style={{
                                                        flex: 1,
                                                        borderRadius: 30,
                                                        resizeMode: 'cover',
                                                        justifyContent: 'center',
                                                    }}>
                                                        <View style={{ paddingLeft: 10 }}>
                                                            <Text style={{ fontSize: 12, color: 'red', fontWeight: 'bold', textAlign: 'center' }}>{elem.name}</Text>
                                                        </View>
                                                    </ImageBackground>
                                                </TouchableOpacity>)
                                        })}
                                    </ScrollView>

                                </Row>
                                <View style={{ width: '100%', height: '60%', marginTop: 20 }}>

                                    <FlatList
                                        numColumns={2}
                                        keyExtractor={(item, index) => item._id}
                                        contentContainerStyle={styles.list}
                                        data={this.state.dishes}
                                        renderItem={this.renderItem}
                                    />
                                </View>
                            </Grid>}
                        </ScrollView>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    itemsContainer: {
        width: '100%',
        height: '90%',
    },
    list: {
        justifyContent: 'center',
        flexDirection: 'column',
    }
});
