
import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import {
    StyleSheet, Text, View, TextInput, TouchableOpacity, Image, ScrollView, ActivityIndicator,
    ImageBackground,Alert
}
    from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import jwt_decode from "jwt-decode";

export default function Login(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {

    }, [])

    function loginWithCrediantials() {
        setLoading(true)
        const crediantials = {
            username: email,
            password,
            strategy: 'local'
        }
        client.authenticate(crediantials).then(async (token) => {
            setLoading(false)
            //const user = client.passport.verifyJWT(token.accessToken)
            if (token.accessToken) {
                const decoded = jwt_decode(token.accessToken);
                await AsyncStorage.setItem('token', token.accessToken);
                await AsyncStorage.setItem('userID', decoded.userId);
                try {
                    const user = await client.service('users').get(decoded.userId);
                    await AsyncStorage.setItem('user', JSON.stringify(user));
                    props.navigation.replace('home');
                } catch (err) {
                    console.log(err)
                }
                // props.navigation.navigate('')
            }


        }).catch(err => { setLoading(false); Alert.alert(JSON.stringify(err.message)) })
    }

    return (
        <ImageBackground style={{ resizeMode: "resize", justifyContent: 'flex-start', width: '100%' }} source={require("../../assets/bkg-home.jpg")}>
            <ScrollView contentContainerStyle={{ width: '100%', height: '100%' }}>
                <View style={{ alignItems: 'center' }}>
                    <View style={styles.header}>
                        <Text style={{
                            fontWeight: "bold",
                            fontSize: 25,
                            color: "rgb(0, 0,0)",
                            marginBottom: 40
                        }}>Bienvenue !</Text>
                    </View>
                    <Image style={styles.logo} source={require('../../assets/homemade.png')} />
                </View>
                <View style={styles.container}>


                    <View style={styles.inputView} >
                        <TextInput
                            style={styles.inputText}
                            placeholder="Nom d'utilisateur..."
                            value={email}
                            placeholderTextColor="#003f5c"
                            onChangeText={text => setEmail(text)} />
                    </View>
                    <View style={styles.inputView} >
                        <TextInput
                            secureTextEntry
                            value={password}
                            style={styles.inputText}
                            placeholder="Mot de passe..."
                            placeholderTextColor="#003f5c"
                            onChangeText={text => setPassword(text)} />
                    </View>
                    <TouchableOpacity onPress={() => loginWithCrediantials()} style={styles.loginBtn}>
                        {loading ? <ActivityIndicator size="small" style={styles.loading} /> : <Text style={styles.loginText}>CONNEXION</Text>}
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#ffcc00',
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        width: '100%',
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor : '#ff0000'
    },
    logo: {
        width: 300,
        alignItems: 'center',
        height: 150,
        marginTop: -30,
        // backgroundColor:'green'

    },
    inputView: {
        width: "80%",
        backgroundColor: "white",
        borderRadius: 25,
        borderColor: 'darkgrey',
        borderWidth: 1,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "black"
    },
    forgot: {
        color: "black",
        fontSize: 11
    },
    loginBtn: {
        width: "80%",
        borderColor: 'darkgrey',
        borderWidth: 1,
        backgroundColor: "red",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    loading: {
        color: "black"
    },
    loginText: {
        color: "white"
    }
});
