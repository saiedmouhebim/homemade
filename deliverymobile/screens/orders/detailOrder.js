import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView, TouchableHighlight, TouchableOpacity, Alert } from 'react-native';
import Header from '../header/header'
import AsyncStorage from '@react-native-async-storage/async-storage';
import client from '../../utils/client';
import { getDistance, getPreciseDistance } from 'geolib';
import { Col, Row, Grid } from "react-native-easy-grid";
import Draggable from 'react-native-draggable';
import { padStart } from "lodash"
import Moment from 'moment';
import { Icon, Overlay } from "react-native-elements";
import * as Linking from "expo-linking"
import 'moment/locale/fr'; Moment.locale('fr');
const globalStyleSheet = require('../globalStyleSheet.js').default

export default class Home extends React.Component {

    state = {
        order: null,
        loading: true,
        visible : false
    }

    componentDidMount() {
        this.getData();
    };

    getData = async () => {
        const query = {
            query: {
                _id: this.props.route.params.id,
                $populate: ['chef', 'items.dish'],
                $limit: 1,
            }
        }
        const order = await client.service('orders').find(query)
        this.setState({ order: order.data[0], loading: false })
    }

    updateDelivred = async (key) => {
        let { order } = this.state;
        if (key === 'delivery') {
            order.delivered = true;
        } else {

        }
        await client.service('orders').update(order._id, order);
        Alert.alert('Commande a jour')
    }

    render() {
        const { order } = this.state;
        return (
            <View style={styles.container}>

                <Header
                    goBack
                    routeParams={{ routeName: 'Panier', ...this.props }} />
                {(this.state.loading || order === null) ? <Text>loading</Text> : <Grid
                    style={{ width: '100%', height: '100%', marginTop: 10 }}>
                    <Row size={6} style={{ borderBottomColor: '#e2e2e2', borderBottomWidth: 1, width: '90%', marginLeft: 10 }}>
                        <Text style={{ fontSize: 17 }}>Total Pannier : {order.totalPriceInclTax.toFixed(0)} EURO HT</Text>
                    </Row>
                    <Row size={6} style={{ borderBottomColor: '#e2e2e2', borderBottomWidth: 1, width: '90%', marginLeft: 10 }}>
                        <Text style={{ fontSize: 17 }}>Chef : {order.chef.firstName + ' ' + order.chef.lastName} </Text>
                    </Row>
                    <Row size={50} style={{ marginTop: 10 }}>
                        <ScrollView>
                            <Grid style={{ width: '100%', height: '100%' }}>
                                <ScrollView>
                                    {order.items.map((elem, index) => {

                                        return (<View
                                            key={index}
                                            style={{
                                                width: '100%',
                                                marginBottom: 10,
                                                //alignItems: 'center',
                                                height: 160, backgroundColor: '#fff', flexDirection: 'row'
                                            }}
                                        // onpress={() => console.log(item)}
                                        >
                                            <Grid style={{ width: '100%', height: '100%' }}>
                                                <Row size={70}>
                                                    <Col size={10}>
                                                        {/* <View>
                                                    <Text>fds</Text>
                                                    <Image style={{ width: 100, height: 100 }} resizeMode="contain" source={{
                                                    uri: image ? image :
                                                        `http://41.231.122.145:3001/uploads/${item._id}/${uploadedImage.value}`
                                                }} />
                                                </View> */}
                                                        <ImageBackground
                                                        resizeMode={'contain'}
                                                        source={{ uri: elem.dish.photo }}
                                                            style={{ width: 120, height: 100 }}>

                                                        </ImageBackground>
                                                    </Col>
                                                    <Col size={20} style={{marginLeft : 20}}>
                                                        <Text style={{ fontSize: 17, fontWeight: 'bold', color: 'red', marginTop: 10 }}>
                                                            {elem.dish.name}
                                                        </Text>
                                                        <Text style={{  marginTop: 10, fontSize: 20 }}>
                                                           Quantité: {elem.quantity}</Text>
                                                        <Text style={{ fontWeight: 'bold', marginTop: 10, fontSize: 20 }}>
                                                            {elem.totalPriceInclTax} EURO</Text>
                                                            
                                                    </Col>
                                                </Row>
                                                <Row size={2} style={{ alignItems: 'center', justifyContent: 'center', marginTop: 8 }}>
                                                    <View style={{ backgroundColor: '#d5d5d5', width: '90%', height: 1 }}></View>
                                                </Row>
                                            </Grid>
                                        </View>)
                                    })}

                                </ScrollView>
                            </Grid>
                        </ScrollView>
                    </Row>
                    <Row size={8} style={{ alignItems: 'center', justifyContent: 'space-between', marginHorizontal: 5 }}>
                        <TouchableOpacity
                            onPress={() => this.setState({ visible: true })}
                            style={{
                                justifyContent: 'center',
                                borderRadius: 15,
                                width: '30%',
                                alignItems: 'center',
                                height: '100%',
                                backgroundColor: 'green'
                            }}>


                            <View style={globalStyleSheet.iconAndTextWrapper}>
                                <Text style={globalStyleSheet.bigButtonText}>User Détails</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.updateDelivred('delivery')}
                            style={{
                                justifyContent: 'center',
                                borderRadius: 15,
                                width: '30%',
                                alignItems: 'center',
                                height: '100%',
                                backgroundColor: 'green'
                            }}>


                            <View style={globalStyleSheet.iconAndTextWrapper}>
                                <Text style={globalStyleSheet.bigButtonText}>Set delivred</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.updateDelivred('payment')}
                            style={{
                                justifyContent: 'center',
                                borderRadius: 15,
                                width: '30%',
                                alignItems: 'center',
                                height: '100%',
                                backgroundColor: 'green'
                            }}>


                            <View style={globalStyleSheet.iconAndTextWrapper}>
                                <Text style={globalStyleSheet.bigButtonText}>Set Payment</Text>
                            </View>
                        </TouchableOpacity>
                    </Row>
                </Grid>}
                {!this.state.loading && <Overlay overlayStyle={{ width: 281, height: 300 }} onBackdropPress={()=>this.setState({visible: false})} isVisible={this.state.visible} >
                    <ScrollView style={{ width: '100%', height: '100%' }}>
                        <Grid style={{ width: '100%', height: '100%' }}>
                            <Row size={40} style={{ width: '100%', height: 70 }}>
                                <Col size={30}>
                                    <Text style={{color: 'red'}}>Nom d'utilisateur:</Text>
                                </Col>
                                <Col size={40}>
                                    <Text>{order.deliveryName}</Text>
                                </Col>
                            </Row>
                            <Row size={20} style={{ width: '100%', height: 70 }}>
                                <Col size={40}>
                                    <Text style={{color: 'red'}}>Adresse:</Text>
                                </Col>
                                <Col size={40}>
                                    <Text>{order.deliveryAdress}</Text>
                                </Col>
                            </Row>
                            <Row size={20} style={{ width: '100%', height: 70 }}>
                                <Col size={40}>
                                    <Text style={{color: 'red'}}>Télephone:</Text>
                                </Col>
                                <Col size={40}>
                                    <Text>{order.deliveryPhone}</Text>
                                </Col>
                            </Row>
                            <Row size={20} style={{ width: '100%', height: 70 }}>
                                <Col size={40}>
                                    <Text style={{color: 'red'}}>Payment:</Text>
                                </Col>
                                <Col size={40}>
                                    <Text>{order.payments}</Text>
                                </Col>
                            </Row>
                        </Grid>
                    </ScrollView>
                </Overlay>}
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    itemsContainer: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
    },

});
