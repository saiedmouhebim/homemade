import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, ActivityIndicator, View , Image} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Location from 'expo-location';
import * as Notifications from 'expo-notifications';
import client from '../utils/client';
import { Button, Overlay } from 'react-native-elements';

export default class Home extends React.Component {

    componentDidMount() {
        // this.props.navigation.navigate('login')
        this.getData();

    };

    getData = async () => {
        try {
            let { status } = await Location.requestPermissionsAsync();
            if (status !== 'granted') {
                return;
            }

            var loc = await Location.hasServicesEnabledAsync();

            if (loc === false) {

                let { status } = await Location.requestPermissionsAsync();
                if (status !== 'granted') {
                    console.log('notgranted')
                }
            }
            let location = await Location.getCurrentPositionAsync({});
            await AsyncStorage.setItem('location', JSON.stringify(location))
            // await AsyncStorage.removeItem('token')
            // await AsyncStorage.removeItem('userId')
            // await AsyncStorage.removeItem('user')
            const storage = await AsyncStorage.getItem('token');
            if (storage) {

                const crediantials = {
                    accessToken: storage,
                    strategy: 'jwt'
                }
                const newToken = await client.authenticate(crediantials);

                await AsyncStorage.setItem('token', newToken.accessToken)
                this.props.navigation.replace('orders');

            } else {
              this.props.navigation.replace('login');
            }
        } catch (err) {
            console.log('d', err)
            this.props.navigation.replace('login');
        }
    }

    render() {
        return (
            <View
                style={styles.container}>
                    <Image
                        resizeMode="contain"
                        style={{ width: 300, height: 400 }}
                        source={require('../images/loading.gif')} />
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
