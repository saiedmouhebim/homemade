import React from "react";
import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "./screens/home";
import Login from "./screens/login/login";
import Dashboard from "./screens/dashboard/dashboard";
import Orders from "./screens/orders/orderTab";
import DetailOrder from "./screens/orders/detailOrder";

export default function App() {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="home">
        <Stack.Screen
          name="home"
          options={{ headerShown: false }}
          children={(props) => <Home {...props} />}
        />
        <Stack.Screen
          name="login"
          options={{ headerShown: false }}
          children={(props) => <Login {...props} />}
        />
        <Stack.Screen
          name="detail"
          options={{ headerShown: false }}
          children={(props) => <DetailOrder {...props} />}
        />
        <Stack.Screen
          name="dashboard"
          options={{ headerShown: false }}
          children={(props) => <Dashboard {...props} />}
        />
         <Stack.Screen
          name="orders"
          options={{ headerShown: false }}
          children={(props) => <Orders {...props} />}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}