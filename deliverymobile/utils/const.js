export const menus = [
    {menuName : 'Commandes',backgroundColor : '#414141', iconName : 'shopping-basket', forecolor:'#fff', iconName : 'shopping-basket',iconColor:'#cd5a92', route : 'orders'},
    {menuName : 'Chef Cuisiner',backgroundColor : '#414141', iconName : 'users', forecolor:'#fff', iconName : 'users',iconColor:'#cd5a92', route : 'czones'},

];
// export const menus = [
//     {menuName : 'Commandes clients',backgroundColor : '#e9e9e9', forecolor:'#cd5a92', iconName : 'shopping-basket', route : 'productVue'},
//     {menuName : 'Clients',backgroundColor : '#e9e9e9', forecolor:'#cd5a92', iconName : 'users', route : 'Client'},
//     {menuName : 'Catalogue',backgroundColor : '#e9e9e9', forecolor:'#cd5a92', iconName : 'product-hunt', route : 'Products'},
//     {menuName : 'Règlements',backgroundColor : '#e9e9e9', forecolor:'#cd5a92', iconName : 'newspaper-o', route : ''},
//     {menuName : 'Réclamations',backgroundColor : '#e9e9e9', forecolor:'#cd5a92', iconName : 'wechat', route : 'claims'},
//     {menuName : 'Livraisons',backgroundColor : '#e9e9e9', forecolor:'#cd5a92', iconName : 'truck', route : 'delivery'},
//     {menuName : 'Rapports de visite',backgroundColor : '#e9e9e9', forecolor:'#cd5a92', iconName : 'credit-card', route : 'visits'},
// ];
export const GENDERS = [
    { key: 'm', label: 'Monsieur' },
    { key: 'f', label: 'Madame' }
  ];

export const getSkipFromLimitAndPage = (limit, page) => page * limit - limit;

export const getValueFromFields = (item, key) => {
    if(item){const field = item.fields.find(x => x.key === key);
    return field ? field.value : '';}else {return ''}
}

export const postData = async (service, data) =>{
    
}

export const ENDPOINT = 'ws://41.231.122.145:3001'
