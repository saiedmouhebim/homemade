import feathers from "@feathersjs/client/dist/feathers";
import rest from "@feathersjs/client/dist/rest";
import auth from "@feathersjs/client/dist/authentication";
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Alert } from "react-native";

const host = "http://192.168.43.21:3001/api";
const restClient = rest(host);

const client = feathers()
  .configure(restClient.fetch(window.fetch))
  .configure(
    auth({
      storage: AsyncStorage // To store our accessToken
    })
  );
  
export default client;
