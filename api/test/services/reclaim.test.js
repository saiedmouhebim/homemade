const assert = require('assert');
const app = require('../../src/app');

describe('\'reclaim\' service', () => {
  it('registered the service', () => {
    const service = app.service('reclaim');

    assert.ok(service, 'Registered the service');
  });
});
