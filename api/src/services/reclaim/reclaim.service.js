// Initializes the `reclaim` service on path `/reclaim`
const { Reclaim } = require('./reclaim.class');
const createModel = require('../../models/reclaim.model');
const hooks = require('./reclaim.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.api.use('/reclaim', new Reclaim(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.api.service('reclaim');

  service.hooks(hooks);
};
