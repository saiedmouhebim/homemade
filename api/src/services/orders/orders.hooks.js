const { authenticate } = require('@feathersjs/authentication').hooks;
const fetch = require('node-fetch');

const commonHooks = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [],
    find: [
      commonHooks.disablePagination(),
    ],
    get: [],
    create: [setRef],
    update: [authenticate('jwt')],
    patch: [authenticate('jwt')],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async context => {
        // const order = context.result;
        // delete order._id;
        // delete order.createdAt;
        // delete order.updatedAt;
        // await context.app.api.service('orders-backup').create(order);

        // pusher.trigger('orders', 'addOrder', { order: context.result });

        // return context;
      }
    ],
    update: [
     
    ],
    patch: [],
    remove: [
      context => {
      }
    ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

async function setRef(context) {
  const { data } = context;
  if (data instanceof Array) {
    const counter = await context.app
      .get('counters')
      .getNext('orders', 'completed');

    data[0].ref = counter.sequence;
  } else {
    const counter = await context.app
      .get('counters')
      .getNext('orders', 'completed');

    data.ref = counter.sequence;
  }
  return context;
}
