const { authenticate } = require('@feathersjs/authentication').hooks;

const commonHooks = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [],
    find: [
      commonHooks.disablePagination(),
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

async function setRef(context) {
  const { data } = context;
  if (data instanceof Array) {
    const counter = await context.app
      .get('counters')
      .getNext('reviews', context.data.target);

    data[0].ref = counter.sequence;
  } else {
    const counter = await context.app
      .get('counters')
      .getNext('reviews', context.data.target);

    data.ref = counter.sequence;
  }
  return context;
}