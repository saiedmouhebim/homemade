const users = require('./users/users.service.js');

const zones = require('./zones/zones.service.js');

const dishes = require('./dishes/dishes.service.js');

const menus = require('./menus/menus.service.js');

const orders = require('./orders/orders.service.js');

const reviews = require('./reviews/reviews.service.js');

const reclaim = require('./reclaim/reclaim.service.js');

module.exports = function(app) {
  app.configure(users);

  app.configure(zones);
  app.configure(dishes);
  app.configure(menus);
  app.configure(orders);
  app.configure(reviews);
  app.configure(reclaim);
};
