const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('./logger');
const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');
const versionate = require('feathers-versionate');
const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');
const authentication = require('./authentication');
const mongoose = require('./mongoose');
const jobs = require('./jobs');
const mailer = require('./mailer');
const swagger = require('feathers-swagger');
const memory = require('feathers-memory');
const swaggerConfig = require('./swagger-config');
const blobService = require('feathers-blob');
const fs = require('fs-blob-store');
const multer = require('multer');
const fsystem = require('fs');

const app = express(feathers());
let uploadsPath = __dirname.substr(0, __dirname.length - 4) + '/public/uploads';
let blobStorage = fs(uploadsPath);

// const storage = multer.diskStorage({
//   destination: (req, file, cb) => cb(null, 'public/uploads/5fe48cd995802b23d05242b4'),
//   filename: (req, file, cb) => cb(null, `${Date.now()}-.jpg`)
// });

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const dir = `public/uploads/${req.params.id}`;
    fsystem.exists(dir, exist => {
      if (!exist) {
        return fsystem.mkdir(dir, error => cb(error, dir));
      }
      return cb(null, dir);
    });
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-.jpg`);
  }
});

const messageService = memory();

messageService.docs = {
  description: 'A service to send and receive messages',
  definitions: {
    messages: {
      type: 'object',
      required: ['text'],
      properties: {
        text: {
          type: 'string',
          description: 'The message text'
        },
        useId: {
          type: 'string',
          description: 'The id of the user that send the message'
        }
      }
    }
  }
};

// Load app configuration
app.configure(configuration());
// Configure versionate
app.configure(versionate());
// Register a base-path "/api", and provide access to it via `app.api`
app.versionate('api', '/api/');

// Enable security, CORS, compression, favicon and body parsing
app.use(
  helmet({
    frameguard: false
  })
);

app.use(cors());
app.use(compress());
app.use(express.json({ limit: '10000000mb' }));
app.use(express.urlencoded({ extended: true }));

app.use(favicon(path.join(app.get('public'), 'favicon.ico')));

app.configure(swagger(swaggerConfig(app)));

// Host the public folder
app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(express.rest());

app.configure(
  socketio(function(io) {
    io.on('connection', socket => {
      console.log('connection used');
      socket.emit('positionUpdate', { positionUpdated: 'hjoisfdj' });
      socket.on('position', async data => {
        console.log('updated');
        socket.emit('positionUpdate', { positionUpdated: 'gdfoghdfuiohgdfui' });
        const query = {
          query: {
            user: data.user
          }
        };
        try {
          const pos = await app.api.service('position').find(query);
          if (pos.total === 0) {
            const insertQuery = {
              user: data.user,
              position: {
                lat: data.location.coords.latitude,
                log: data.location.coords.longitude
              }
            };
            socket.emit('positionAdded', { createdPOsition: 'hfeuiherui' });

            const createdPOsition = await app.api
              .service('position')
              .create(insertQuery);
            socket.emit('positionAdded', { createdPOsition });
          } else {
            let position = pos.data[0];
            position.position = {
              lat: data.location.coords.latitude,
              log: data.location.coords.longitude
            };
            socket.emit('positionUpdate', {
              positionUpdated: 'gdfoghdfuiohgdfui'
            });

            const positionUpdated = await app.api
              .service('position')
              .update(position._id, position);
            console.log(positionUpdated);
            socket.emit('positionUpdate', { positionUpdated });
          }
        } catch (err) {
          console.log(err);
        }
      });
    });
  })
);

app.configure(mongoose);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);
// job queue
app.configure(jobs);
// mailer
app.configure(mailer);

app.use('/messages', messageService);
const upload = multer({
  storage,
  limits: {
    fieldSize: 1e8, // Max field value size in bytes, here it's 100MB
    fileSize: 1e8 //  The max file size in bytes, here it's 10MB
    // files: the number of files
    // READ MORE https://www.npmjs.com/package/multer#limits
  }
});

app.api.use('/uploads-file/:id', upload.single('files'), (req, res) => {
  res.send({ uploaded: true, fileName: req.file.filename });
});

app.api.use('/uploads', blobService({ Model: blobStorage }));

// Configure middleware for unknown api paths
app.use('/api/', (req, res) => {
  res.status(404).json({ error: 'Invalid path' });
});

// SPA support, return index for unknown paths
app.use('/client/', (req, res) => {
  res.sendFile(path.join(app.get('public'), 'client', 'index.html'));
});

// SPA support for admin app, return index for unknown paths
app.use('/admin/', (req, res) => {
  res.sendFile(path.join(app.get('public'), 'admin', 'index.html'));
});

app.use((req, res) => {
  res.redirect(301, '/admin/');
});

// Configure the error handler
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

module.exports = app;
