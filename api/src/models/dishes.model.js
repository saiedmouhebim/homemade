// dishes-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const modelName = 'dishes';
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    chef: { type: mongooseClient.Schema.Types.ObjectId, ref: 'user' },
    photo : String,
    name: {type: String, index: true},
    description: {type: String, index: true},
    type: {type: String, index: true},
    target: {type: String, index: true},
    ingridient: [{type: String, index: true}],
    price: {type: Number},
    quantity: {type: Number},
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);
  
};
