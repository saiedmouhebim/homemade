const { PAYMENTS } = require('../constants');
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const modelName = 'orders';
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    user: {type: String , index : true},
    checkedDate: { type: Date, default: null },
    checked: { type: Boolean, default: false },
    deliveryAdress: { type: String, index: true },
    deliveryPhone: { type: String, index: true },
    deliveryLat: { type: String, index: true },
    deliveryLog: { type: String , index: true},
    deliveryName: {type: String, index: true},
    payments: {
      type: String,
      enum: PAYMENTS.ALL
    },
//    payment: { ref: 'payment', type: Schema.Types.ObjectId },
    synchronised: { type: Boolean, default: false, index: true },
    valid: { type: Boolean, required: true },
    ref: Number,
    chef: { ref: 'users', type: Schema.Types.ObjectId },
    totalPriceInclTax: { type: Number, required: true, index: true },
    delivered: { type: Boolean, default: false },
    items: [
      {
        dish: { ref: 'dishes', type: Schema.Types.ObjectId },
        menu: { ref: 'menus', type: Schema.Types.ObjectId },
        quantity: { type: Number, required: true },
        totalPriceInclTax: { type: Number, required: true },
        color: { type: String, default: '', index: true }
      }
    ],
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);
  
};
