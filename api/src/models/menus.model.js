// menus-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const modelName = 'menus';
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema({
    chef: { type: mongooseClient.Schema.Types.ObjectId, ref: 'user' },
    dishes: [{ type: mongooseClient.Schema.Types.ObjectId, ref: 'dishes' }],
    cover: {type: String},
    name :  {type: String, index:true},
    description: {type : String, index: true},
    quantity : {type :  Number},
    price: Number,
    coockTime: Number,
    available: [{type:String}],
    day: Date
  }, {
    timestamps: true
  });

  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);
  
};
