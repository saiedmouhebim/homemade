// override all less variables of ant-design to customize the theme.
module.exports = {
  '@primary-color': 'red',
  '@btn-primary-color': 'rgba(255,255,255, 0.65)',
  '@layout-header-background': '#363636',
  '@layout-header-height': '106px',
  '@table-header-bg': 'red',
  '@table-header-sort-bg': 'red'
};
