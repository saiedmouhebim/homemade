import { combineReducers } from 'redux';

import app from './modules/app/app.ducks';
import auth, { LOG_OUT_FULFILLED } from './modules/auth/auth.ducks';
import users from './modules/users/users.ducks';
import zones from './modules/zones/zones.ducks';
import menus from './modules/Menus/menus.ducks';
import dishes from './modules/Dishes/dishes.ducks';
import orders from './modules/Adminsorders/orders.ducks';
import corders from './modules/Clientorders/orders.ducks';

const appReducer = combineReducers({
  app,
  auth,
  users,
  corders,
  menus,
  orders,
  dishes,
  zones
});

const rootReducer = (state, action) => {
  if (action.type === LOG_OUT_FULFILLED) {
    state = { app: { initialized: true } };
  }
  return appReducer(state, action);
};

export default rootReducer;
