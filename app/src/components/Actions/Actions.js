import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Row, Col, Icon, Button, Popconfirm } from 'antd';
import { CopyOutlined } from '@ant-design/icons';

const style = {
  icon: {
    fontSize: '1.2rem'
  }
};

const Actions = ({
  selectUrl,
  showResend,
  onResend,
  onDelete,
  showDelete,
  showBarcode,
  onBarcode,
  showUpdate,
  showDuplicate,
  onDuplicate
}) => (
  <Row
    className="actions"
    type="flex"
    gutter={12}
    style={{ flexWrap: 'nowrap' }}
  >
    {showBarcode ? (
      <Col>
        <Popconfirm
          placement="topRight"
          icon={
            <Icon type="warning" theme="filled" style={{ color: '#f5222d' }} />
          }
          okType="danger"
          title="Création code a barre"
          okText="Oui"
          cancelText="Non"
          onConfirm={onBarcode}
        >
          <Button type="default">
            <Icon
              type="barcode"
              style={{ fontSize: '1.2rem', color: 'grey' }}
            />
          </Button>
        </Popconfirm>
      </Col>
    ) : null}
    {showUpdate && (
      <Col>
        <Link to={selectUrl}>
          <Icon style={style.icon} type="select" />
        </Link>
      </Col>
    )}
    {showDuplicate ? (
      <Col>
        <Popconfirm
          placement="topRight"
          icon={
            <Icon type="warning" theme="filled" style={{ color: '#f5222d' }} />
          }
          okType="danger"
          title="Dupliquer"
          okText="Oui"
          cancelText="Non"
          onConfirm={onDuplicate}
        >
          <Button type="default">
            <CopyOutlined style={{ fontSize: '1.2rem', color: 'grey' }} />
          </Button>
        </Popconfirm>
      </Col>
    ) : null}

    {showResend ? (
      <Col>
        <Popconfirm
          placement="topRight"
          icon={
            <Icon type="warning" theme="filled" style={{ color: '#f5222d' }} />
          }
          okType="danger"
          title="Renvoyer"
          okText="Oui"
          cancelText="Non"
          onConfirm={onResend}
        >
          <Button type="danger">
            <Icon style={style.icon} type="mail" />
          </Button>
        </Popconfirm>
      </Col>
    ) : null}
    {showDelete ? (
      <Col>
        <Popconfirm
          placement="topRight"
          icon={
            <Icon type="warning" theme="filled" style={{ color: '#f5222d' }} />
          }
          okType="danger"
          title="Supprimer cet élément?"
          okText="Oui"
          cancelText="Non"
          onConfirm={onDelete}
        >
          <Button type="danger">
            <Icon style={style.icon} type="close-circle" />
          </Button>
        </Popconfirm>
      </Col>
    ) : null}
  </Row>
);

Actions.propTypes = {
  showDelete: PropTypes.bool,
  showUpdate: PropTypes.bool,
  selectUrl: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  onResend: PropTypes.func,
  showResend: PropTypes.func,
  onBarcode: PropTypes.func,
  showBarcode: PropTypes.func,
  showDuplicate: PropTypes.func,
  onDuplicate: PropTypes.func,
};

export default Actions;
