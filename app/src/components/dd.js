import React, { Component } from 'react';
import { Col, Row, Select, Modal } from 'antd';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import Alert from 'sweetalert2';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';

import client from '../utils/client';

const { confirm } = Modal;
export default class App extends Component {
  state = {
    exhibitors: [],
    SelectedExhibitor: null,
    visible: false,
    meetings: null,
    calendarEvents: []
  };

  componentDidMount() {
    let query = {
      query: {
        event: this.props.event._id,
        $limit: -1,
        $populate: 'exhibitor'
      }
    };
    client
      .service('meetings')
      .find(query)
      .then(data => {
        const exhibitors = this.props.target.filter(key => key.checked);
        let draggableEl = document.getElementById('external-events');
        new Draggable(draggableEl, {
          itemSelector: '.fc-event',
          eventData: function (eventEl) {
            let title = eventEl.getAttribute('title');
            let id = eventEl.getAttribute('data');
            return {
              title: title,
              id: id
            };
          }
        });
        let array = [];
        data.map(element => {
          let date = new Date(element.startDate);
          let obj = {
            id: element._id,
            title: this.getField(element.exhibitor, element.attendee),
            start: date,
            editable: this.props.attendee._id === element.attendee,
            backgroundColor:
              this.props.attendee._id === element.attendee ? 'grey' : 'red'
          };
          array.push(obj);
        });
        this.setState({
          calendarEvents: array,
          exhibitors: exhibitors,
          meetings: data
        });
      })
      .catch(err => console.log(err));
  }

  getField = (array, attendee) => {
    const returnedField = array.fields.find(key => key.key === 'company');
    return returnedField
      ? this.props.attendee._id === attendee
        ? returnedField.value
        : ''
      : '';
  };

  findField = field => {
    return this.state.exhibitors.find(key => key.id === field);
  };

  handleChange = element => {
    this.setState({ SelectedExhibitor: element });
  };

  eventClick = eventClick => {
    if (eventClick.event.backgroundColor !== 'red') {
      confirm({
        title: 'Suppression ' + eventClick.event.title,
        content: 'Etes vous certain de vouloir supprimer ce rendez-vous ?',
        onOk: () => {
          eventClick.event.remove();
          const index = this.state.exhibitors.findIndex(
            x => x.id === eventClick.event.id
          );
          let exhibitors = this.state.exhibitors;
          let id = exhibitors[index]
            ? exhibitors[index].collectionId
            : eventClick.event.id;

          client
            .service('meetings')
            .remove(id)
            .then(data => {
              if (exhibitors[index]) {
                exhibitors[index].exist = true;
                exhibitors[index].disabled = false;
                this.setState({
                  SelectedExhibitor: null,
                  exhibitors: exhibitors
                });
              } else {
                let e = exhibitors.findIndex(
                  key => key.lastName === eventClick.event.title
                );
                exhibitors[e].exist = true;
                exhibitors[e].disabled = false;
                this.setState({
                  SelectedExhibitor: null,
                  exhibitors: exhibitors
                });
              }
            })
            .catch(err => console.log(err));
        },
        onCancel() {
          console.log('Cancel');
        }
      });
    }
  };

  change = e => {
    let meeting = {
      startDate: e.date,
      event: this.props.event._id,
      exhibitor: this.state.SelectedExhibitor,
      attendee: this.props.attendee._id
    };
    let { meetings } = this.state;
    client
      .service('meetings')
      .create(meeting)
      .then(data => {
        meetings.push(data);
        const index = this.state.exhibitors.findIndex(
          x => x.id === this.state.SelectedExhibitor
        );
        let exhibitors = this.state.exhibitors;
        exhibitors[index].exist = false;
        exhibitors[index].collectionId = data._id;
        this.setState({
          SelectedExhibitor: null,
          exhibitors: exhibitors,
          meetings: meetings
        });
      })
      .catch(err => console.log(err));
  };

  changePosition = event => {
    const index = this.state.exhibitors.findIndex(x => x.id === event.event.id);
    let exhibitors = this.state.exhibitors;
    let id = exhibitors[index]
      ? exhibitors[index].collectionId
      : event.event.id;
    if (exhibitors[index]) {
      let collectionIndex = this.state.meetings.findIndex(
        key => key._id === exhibitors[index].collectionId
      );
      let meeting = this.state.meetings;
      meeting[collectionIndex].startDate = new Date(event.event.start);
      client
        .service('meetings')
        .update(meeting[collectionIndex]._id, meeting[collectionIndex])
        .then(data => {
          this.setState({ meetings: meeting });
        })
        .catch(err => console.log(err));
    } else {
      let callendarIndex = exhibitors.findIndex(
        key => key.firstName === event.event.title
      );
      let collectionIndex = this.state.meetings.findIndex(
        key => key.exhibitor._id === exhibitors[callendarIndex].id
      );
      let meeting = this.state.meetings;
      meeting[collectionIndex].startDate = new Date(event.event.start);
      client
        .service('meetings')
        .update(meeting[collectionIndex]._id, meeting[collectionIndex])
        .then(data => {
          this.setState({ meetings: meeting });
        })
        .catch(err => console.log(err));
    }
  };

  getDate = ev => {
    if (ev === 'start') {
      const date = new Date(this.props.event.startTime);
      return date;
    } else {
      const date = new Date(this.props.event.endTime);
      return date;
    }
  };

  getDay = ev => {
    if (ev === 'start') {
      let date = new Date(this.props.event.startTime);
      return date;
    } else {
      let date = new Date(this.props.event.endTime);
      return date;
    }
  };

  getFullDaterange = ev => {
    return [1, 2, 3];
  };

  render() {
    const { Option } = Select;
    return (
      <div className="animated fadeIn p-4 demo-app">
        <Row />
        <Row>
          <Col lg={3} sm={3} md={3} style={{ margin: 30 }}>
            <div
              id="external-events"
              style={{
                padding: '10px',
                width: '80%',
                margin: 10,
                height: 'auto',
                maxHeight: '-webkit-fill-available'
              }}
            >
              <p align="center">
                <strong> Exposant</strong>
              </p>
              <Select
                defaultValue="Exposant"
                style={{ width: 120 }}
                onChange={this.handleChange}
              >
                {this.state.exhibitors.map((e, i) => {
                  if (e.exist && !e.disabled) {
                    return <Option value={e.id}>{e.firstName}</Option>;
                  }
                })}
              </Select>
              {this.state.SelectedExhibitor && (
                <div
                  className="fc-event"
                  style={{ margin: 10 }}
                  title={this.findField(this.state.SelectedExhibitor).firstName}
                  data={this.state.SelectedExhibitor}
                >
                  {this.findField(this.state.SelectedExhibitor).firstName}
                </div>
              )}
            </div>
          </Col>

          <Col lg={16} sm={16} md={16}>
            <div className="demo-app-calendar" id="mycalendartest">
              <FullCalendar
                defaultView="dayGridMonth"
                header={{
                  left: 'prev,next today',
                  center: 'title',
                  right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
                }}
                rerenderDelay={10}
                editable
                eventDurationEditable={false}
                plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                ref={this.calendarComponentRef}
                weekends={this.state.calendarWeekends}
                events={this.state.calendarEvents}
                // drop={this.drop}
                eventDrop={this.changePosition}
                eventReceive={this.eventReceive}
                drop={this.change}
                eventClick={this.eventClick}
                // selectable={true}
              />
            </div>
          </Col>
        </Row>
        <Row />
      </div>
    );
  }
}
