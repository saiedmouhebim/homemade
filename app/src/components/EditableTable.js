import React from 'react';
import { connect } from 'react-redux';
import { Table, Input, Button, Popconfirm, Form, Select, Icon } from 'antd';

import { addProduct } from '../modules/Product/AddProduct/add-product.ducks';

const EditableContext = React.createContext();

const style = {
  icon: {
    fontSize: '1.2rem'
  }
};

class EditableCell extends React.Component {
  findName = product => {
    const field = product.fields.find(key => key.key === 'name');
    return field ? field.value : ' ';
  };

  getInput = (dataIndex, products) => {
    if (dataIndex === 'productName') {
      return (
        <Select onChange={this.onProductChange} style={{ width: '80%' }}>
          {this.props.products &&
            this.props.products.ids.map(id => (
              <Select.Option key={id} value={id}>
                {this.findName(this.props.products.byId[id])}
              </Select.Option>
            ))}
        </Select>
      );
    }
    return <Input />;
  };

  onProductChange = value => {
    let newProduct = {};
    this.props.products.byId[value].fields.map(field => {
      switch (field.key) {
        case 'name':
          newProduct[field.key] = field.value;
        case 'ref_ERP':
          newProduct[field.key] = field.value;
        case 'description':
          newProduct[field.key] = field.value;
      }
    });
    return newProduct;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      products,
      record,
      index,
      children,
      ...restProps
    } = this.props;

    return (
      <td {...restProps}>
        {editing &&
        (dataIndex === 'productName' || dataIndex === 'quantity') ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: true,
                  message: `Please Input ${title}!`
                }
              ],
              initialValue: record[dataIndex]
            })(this.getInput(dataIndex, products))}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.orderData,
      editingKey: '',
      key: 1,
      orderId: ''
    };
    this.columns = [
      ...this.props.orderColumns,
      {
        title: 'Opération',
        dataIndex: 'operation',
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.key)}
                    style={{ marginRight: 8 }}
                  >
                    Enreg
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Etes vous sûre"
                onConfirm={() => this.cancel(record.key)}
              >
                <a>Annuler</a>
              </Popconfirm>
            </span>
          ) : (
            <a
              disabled={editingKey !== '' || this.props.isEditable}
              onClick={() => this.edit(record.key)}
            >
              <Icon style={style.icon} type="select" />
            </a>
          );
        }
      }
    ];
  }

  isEditing = record => {
    return record.key === this.state.editingKey;
  };

  cancel = () => {
    const { data } = this.state;
    if (!data['length'].product || !data['length'].quantity) {
      data.pop();
      this.setState({ data });
    }
    this.setState({ editingKey: '' });
  };

  handleAdd = () => {
    const { count, data } = this.state;
    const { products } = this.props;
    const newData = {
      order: data[0].order,
      product: '',
      quantity: '',
      totalPriceExclTax: 0,
      totalPriceInclTax: 0,
      vatRate: 0,
      key: 'newItem',
      adding: true
    };
    this.setState({
      data: [...data, newData],
      count: count + 1,
      editingKey: 'newItem'
    });
  };

  save(form, key) {
    console.log(key);
    form.validateFields((error, row) => {
      if (error) {
        return;
      }

      // const newproductId = form.getFieldValue('productName');
      // const newProduct = this.props.products.byId[newproductId];
      // console.log(newProduct);

      const newData = [...this.state.data];

      const index = newData.findIndex(item => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row
        });

        if (newData[index].adding) {
          let newProduct = this.props.products.byId[newData[index].productName];

          newData[index].totalPriceExclTax =
            Number(newProduct.priceTTC) * Number(newData[index].quantity);
          newData[index].totalPriceInclTax =
            Number(newProduct.priceTTC) * Number(newData[index].quantity);
          newData[index].product = this.props.products.byId[newProduct._id];

          this.props.handleEdit(newData, index, item._id === row._id);
          this.setState({ data: newData, editingKey: '' });
        }

        newData[index].lastQuantity = this.state.data[index].quantity;

        newData[index].totalPriceInclTax =
          newData[index].quantity *
          newData[index].totalPriceExclTax *
          (1 + newData[index].vatRate / 100);

        this.props.handleEdit(newData, index, item._id === row._id);
        this.setState({ data: newData, editingKey: '' });
      } else {
        newData.push(row);
        this.setState({ data: newData, editingKey: '' });
      }
    });
  }

  edit(key) {
    console.log(key);
    this.setState({ editingKey: key });
  }

  render() {
    const components = {
      body: {
        cell: EditableCell
      }
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => {
          return {
            record,
            products: this.props.products,
            inputType: col.dataIndex === '_id' ? 'list' : 'text',
            dataIndex: col.dataIndex,
            title: col.title,
            editing: this.isEditing(record)
          };
        }
      };
    });
    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          dataSource={this.state.data}
          columns={columns}
          rowKey={'_id'}
          rowClassName="editable-row"
          pagination={{
            onChange: this.cancel
          }}
        />
        {!this.props.isEditable && (
          <Button
            onClick={this.handleAdd}
            type="primary"
            style={{ marginBottom: 16 }}
          >
            Ajouter article
          </Button>
        )}
      </EditableContext.Provider>
    );
  }
}

const EditableFormTable = Form.create()(EditableTable);

const mapStateToProps = state => ({
  products: state.products
});

const mapDispatchToProps = {
  addProduct
};

export default connect(mapStateToProps, mapDispatchToProps)(EditableFormTable);
