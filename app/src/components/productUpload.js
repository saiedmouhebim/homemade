import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { message, Button } from 'antd';
import { Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import Axios from 'axios';

import { addMedia } from '../modules/medias/AddMedia/add-media.ducks';
import { deleteMedia } from '../modules/medias/media/media-list.ducks';
import client from '../utils/client';
import { PDFPREVIEW } from '../const';

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

class Medias extends Component {
  state = {
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: []
  };

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf('/') + 1)
    });
  };

  handleChange = ({ fileList }) => {
    this.setState({ fileList });
  };

  handleUpload = (file, fileList) => {
    const { onChange } = this.props;

    const formData = new FormData();
    formData.append('filename', 'x');
    formData.append('files', file);
    if (this.props.type) {
      getBase64(file)
        .then(value => {
          client
            .service('uploads')
            .create({ uri: value })
            .then(data => {
              this.props
                .addMedia({ name: data.id })
                .then(media => {
                  message.success('Mèdia téléversé avec succès');
                  if (this.props.type) {
                    onChange(data.id);
                  } else {
                    onChange({ base64File: value, imageURI: data.id });
                  }
                })
                .catch(err => message.error('Erreur est survenue'));
            })
            .catch(err => message.error('Erreur est survenue'));
        })
        .catch(err => message.error('Erreur est survenue'));
    } else {
      Axios({
        method: 'post',
        url:
          process.env.REACT_APP_API_BASE_URL +
          '/uploads-file/' +
          this.props.dataId,
        data: formData,
        headers: { 'Content-Type': 'multipart/form-data' }
      })
        .then(response => {
          getBase64(file).then(value => {
            message.success('Mèdia téléversé avec succès');
            if (this.props.type) {
              onChange(response.data.fileName);
            } else {
              onChange({ base64File: value, imageURI: response.data.fileName });
            }
          });
        })
        .catch(response => {
          //handle error
          console.log(response);
        });
    }
  };

  handleRemove = () => {
    const { onChange, value } = this.props;
    if (this.props.type) {
      onChange('');
    } else {
      onChange({ base64File: '', imageURI: '' });
    }
    return true;
  };

  render() {
    const { value } = this.props;
    console.log(value);
    const imagePreview = value
      ? value.imageURI
        ? value.imageURI.length > 0
          ? `${process.env.REACT_APP_API_UPLOAD_URL +
              this.props.dataId +
              '/' +
              value.imageURI}`
          : ''
        : value.length > 0
        ? `${process.env.REACT_APP_API_UPLOAD_URL +
            this.props.dataId +
            '/' +
            value}`
        : ''
      : '';

    const uploadButton = (
      <div>
        <PlusOutlined />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    console.log(this.props);
    return (
      <div className="clearfix">
        <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          accept={this.props.type ? '.pdf' : '.png, .jpg, .jpeg'}
          disabled={this.props.disabled}
          showUploadList={false}
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          beforeUpload={this.handleUpload}
          onChange={this.handleChange}
        >
          {imagePreview ? (
            <img
              src={this.props.type ? PDFPREVIEW : imagePreview}
              alt="avatar"
              style={{ width: '100%' }}
            />
          ) : (
            uploadButton
          )}
        </Upload>
        {!this.props.disabled && imagePreview.length > 0 && (
          <Button onClick={this.handleRemove}>Effacer</Button>
        )}
      </div>
    );
  }
}

Medias.propTypes = {
  medias: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  addMedia: PropTypes.func,
  value: PropTypes.any,
  deleteMedia: PropTypes.func
};

const mapDispatchToProps = {
  addMedia,
  deleteMedia
};

export default connect(null, mapDispatchToProps)(Medias);
