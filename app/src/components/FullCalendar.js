import React, { Component } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import Alert from 'sweetalert2';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';

class ClientsCalendar extends Component {
  state = {
    visible: false,
    calendarEvents: []
  };

  render() {
    return (
      <div>
        <FullCalendar
          defaultView="dayGridMonth"
          header={{
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
          }}
          rerenderDelay={10}
          editable
          eventDurationEditable={false}
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          // ref={this.calendarComponentRef}
          // events={this.state.calendarEvents}
          // drop={this.drop}
          // eventDrop={this.changePosition}
          // eventReceive={this.eventReceive}
          // drop={this.change}
          // eventClick={this.eventClick}
          // selectable={true}
        />
      </div>
    );
  }
}

export default ClientsCalendar;
