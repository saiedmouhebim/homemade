import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Col, Drawer, Form, Modal, notification, Row } from 'antd';

class ResourceForm extends Component {
  state = { cancelVisible: false, readOnly: true, pristine: true };

  convertBack = values => values;

  setServerErrors = (values, err) => {
    // eslint-disable-next-line no-console
    console.error(err);

    notification.error({
      message: 'Erreur',
      description:
        'Une erreur est survenue, veuillez vérifier ' +
        'les valeurs entrées ou réessayer plus tard.'
    });

    // TODO: handle 400 error to display validation errors in form
  };

  componentDidMount() {
    this.setState({ readOnly: true });
  }

  handleSubmit = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (err) {
        notification.error({
          message: 'Erreur de validation',
          description:
            'Veuillez vérifier tous les champs et corriger les erreurs.'
        });
      } else {
        if (this.props.onSubmit) {
          this.props.onSubmit(this.convertBack(values));
        }
      }
    });
  };

  handleAddNew = e => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (err) {
        notification.error({
          message: 'Erreur de validation',
          description:
            'Veuillez vérifier tous les champs et corriger les erreurs.'
        });
      } else {
        if (this.props.onSubmit) {
          this.props.submitNew(this.convertBack(values));
        }
      }
    });
  };

  handleGoForward = () => {
    this.props.form.validateFields((err, values) => {
      if (err) {
        notification.error({
          message: 'Erreur de validation',
          description:
            'Veuillez vérifier tous les champs et corriger les erreurs.'
        });
      } else {
        if (this.props.onSubmit) {
          this.props.handleGoForward(this.convertBack(values));
        }
      }
    });
  };

  setReadOnly = (readOnly = true) => {
    this.setState({ readOnly });
  };

  setPristine = (pristine = true) => {
    this.setState({ pristine });
  };

  handleCancel = () => {
    if (!this.state.pristine) {
      this.setState({ cancelVisible: true });
    } else {
      this.handleCancelOk();
    }
  };

  handleCancelOk = () => {
    this.setState({ cancelVisible: false });

    if (typeof this.props.onCancel === 'function') {
      this.props.onCancel();
    }
  };

  handleUpdateFields = () => {
    this.props.UpdateFields();
  };

  handleCancelCancel = () => this.setState({ cancelVisible: false });

  renderForm(children) {
    const {
      mode,
      form,
      extraButtons,
      drawer,
      loading,
      showUpButton,
      steps,
      goBackward,
      showUpdateFields,
      addNew,
      showNextButton,
      disableEdit
    } = this.props;
    const readOnly = mode === 'edit' && this.state.readOnly;
    return (
      <React.Fragment>
        <Form
          className={readOnly ? 'form--read-only' : ''}
          layout="vertical"
          hideRequiredMark={readOnly}
          onSubmit={this.handleSubmit}
        >
          {showUpButton ? (
            <Row type="flex" justify="end" gutter={16}>
              {extraButtons && extraButtons({ form, mode, readOnly })}
              {!readOnly && (
                <Col>
                  <Button
                    type="default"
                    icon="close"
                    disabled={loading}
                    onClick={this.handleCancel}
                  >
                    Annuler
                  </Button>
                </Col>
              )}
              {!readOnly && (
                <Col>
                  <Button
                    type="primary"
                    htmlType="submit"
                    icon="check"
                    loading={loading}
                  >
                    Enregistrer
                  </Button>
                </Col>
              )}
              {!readOnly && addNew && (
                <Col>
                  <Button
                    type="primary"
                    onClick={this.handleAddNew}
                    icon="check"
                    loading={loading}
                  >
                    Enregistrer et nouveau
                  </Button>
                </Col>
              )}

              {readOnly && (
                <Col>
                  <Button
                    disabled={disableEdit}
                    type="primary"
                    icon="edit"
                    onClick={() => this.setReadOnly(false)}
                  >
                    Modifier
                  </Button>
                </Col>
              )}
            </Row>
          ) : null}
          {children({ form, mode, readOnly })}
          {!steps && (
            <Row type="flex" justify="end" gutter={16}>
              {extraButtons && extraButtons({ form, mode, readOnly })}
              {!readOnly && (
                <Col>
                  <Button
                    type="default"
                    icon="close"
                    disabled={loading}
                    onClick={this.handleCancel}
                  >
                    Annuler
                  </Button>
                </Col>
              )}
              {!readOnly && (
                <Col>
                  <Button
                    type="primary"
                    htmlType="submit"
                    icon="check"
                    loading={loading}
                  >
                    Enregistrer
                  </Button>
                </Col>
              )}
              {drawer && (
                <Col>
                  <Button
                    type="primary"
                    onClick={() => this.props.eventDrawer(true)}
                    icon="alert"
                    loading={loading}
                  >
                    Aide
                  </Button>
                </Col>
              )}
              {!readOnly && addNew && (
                <Col>
                  <Button
                    type="primary"
                    onClick={this.handleAddNew}
                    icon="check"
                    loading={loading}
                  >
                    Enregistrer et nouveau
                  </Button>
                </Col>
              )}

              {readOnly && (
                <Col>
                  <Button
                    disabled={disableEdit}
                    type="primary"
                    icon="edit"
                    onClick={() => this.setReadOnly(false)}
                  >
                    Modifier
                  </Button>
                </Col>
              )}
              {showNextButton && (
                <Col>
                  <Button
                    type="primary"
                    onClick={this.props.goToNextDocument}
                    icon="check"
                    loading={loading}
                  >
                    Document Suivant
                  </Button>
                </Col>
              )}
            </Row>
          )}
          {steps && (
            <Row type="flex" justify="end" gutter={16}>
              {extraButtons && extraButtons({ form, mode, readOnly })}
              {!readOnly && goBackward && (
                <Col>
                  <Button
                    type="default"
                    icon="left"
                    disabled={loading}
                    onClick={this.props.handleGoBackward}
                  >
                    Revenir à l'étape précédente
                  </Button>
                </Col>
              )}
              {!readOnly && (
                <Col>
                  <Button
                    type="primary"
                    onClick={this.handleGoForward}
                    //htmlType="submit"
                    icon="right"
                    loading={loading}
                  >
                    Passer à l'étape suivante
                  </Button>
                </Col>
              )}
              {readOnly && (
                <Col>
                  <Button
                    type="primary"
                    icon="edit"
                    onClick={() => this.setReadOnly(false)}
                  >
                    Modifier
                  </Button>
                </Col>
              )}
            </Row>
          )}
          {showUpdateFields && (
            <Col>
              <Button
                type="primary"
                icon="edit"
                onClick={() => this.handleUpdateFields()}
              >
                MAJ
              </Button>
            </Col>
          )}
        </Form>
        <Modal
          title="Annuler"
          okText="Oui"
          cancelText="Non"
          visible={this.state.cancelVisible}
          onOk={this.handleCancelOk}
          onCancel={this.handleCancelCancel}
        >
          <p>Toutes les modifications effectuées seront perdues.</p>
          <p>Annuler l'opération?</p>
        </Modal>
        {drawer && (
          <Drawer
            headerStyle={{ backgroundColor: '#fdc131' }}
            title={this.props.titleDrawer}
            drawerStyle={{ backgroundColor: '#f0f2f5' }}
            placement="top"
            height={'70%'}
            closable={false}
            onClose={() => this.props.eventDrawer(false)}
            visible={this.props.onDrawer}
          >
            {this.props.content}
          </Drawer>
        )}
      </React.Fragment>
    );
  }
}

ResourceForm.propTypes = {
  mode: PropTypes.string,
  content: PropTypes.string,
  titleDrawer: PropTypes.string,
  form: PropTypes.object.isRequired,
  extraButtons: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  handleGoBackward: PropTypes.func,
  handleGoForward: PropTypes.func,
  eventDrawer: PropTypes.func,
  submitNew: PropTypes.func,
  addNew: PropTypes.bool,
  onDrawer: PropTypes.bool,
  loading: PropTypes.bool,
  showUpButton: PropTypes.bool,
  drawer: PropTypes.bool,
  steps: PropTypes.any,
  goBackward: PropTypes.any,
  showUpdateFields: PropTypes.any,
  UpdateFields: PropTypes.any,
  showNextButton: PropTypes.any,
  disableEdit: PropTypes.any,
  goToNextDocument: PropTypes.any,
};

export default ResourceForm;
