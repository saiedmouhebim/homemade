import React from 'react';
import Avatar from 'react-avatar-edit';
import { Modal } from 'antd';

import { AVATAR } from '../const';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    const src = AVATAR;
    this.state = {
      preview: null,
      device: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
        ? 'android'
        : 'desktop',
      visible: false,
      src
    };

    this.onCrop = this.onCrop.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onBeforeFileLoad = this.onBeforeFileLoad.bind(this);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.value) {
      if (nextProps.value !== this.state.src) {
        this.setState({
          src: nextProps.value.length === 0 ? AVATAR : nextProps.value,
          preview: nextProps.value.length === 0 ? AVATAR : nextProps.value
        });
      }
    } else {
      this.setState({
        src: AVATAR,
        preview: AVATAR
      });
    }
  }

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
    this.props.onChange(this.state.preview);
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  onClose = () => {
    this.setState({ preview: AVATAR, src: AVATAR });
    this.props.onChange(AVATAR);
  };

  onCrop = preview => {
    this.setState({ preview });
  };

  onBeforeFileLoad = elem => {
    if (elem.target.files[0].size > 600000000) {
      alert('File is too big!');
      elem.target.value = '';
    }
  };

  render() {
    return (
      <div>
        <div className="upload-btn-wrapper">
          <button
            type={'button'}
            className="btn"
            onClick={() =>
              !this.props.disabled && this.setState({ visible: true })
            }
          >
            <img
              style={{ marginTop: 10, width: 150, height: 150 }}
              src={this.state.preview ? this.state.preview : AVATAR}
              alt="Preview"
            />
          </button>
        </div>
        {!this.props.disabled && (
          <Modal
            title="Avatar"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <div style={{ margin: '0 auto', width: 250 }}>
              <Avatar
                cropRadius={0}
                label={"Choix de l'avatar"}
                width={
                  this.state.device === 'android' ? window.innerWidth - 50 : 200
                }
                height={295}
                onCrop={this.onCrop}
                onClose={this.onClose}
                onBeforeFileLoad={this.onBeforeFileLoad}
                src={this.state.src}
              />
            </div>
          </Modal>
        )}
      </div>
    );
  }
}
