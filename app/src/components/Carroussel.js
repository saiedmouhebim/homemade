import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Upload, Modal, Spin, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

class Medias extends Component {
  state = {
    previewVisible: false,
    previewImage: '',
    previewTitle: '',
    fileList: []
  };

  componentDidMount() {
    this.getMediasData();
  }

  getMediasData = async () => {
    // this.props.fetchMedias().then(data =>{
    //   const medias = data.value;
    //   if(medias.length >0){
    //     let mediasArray = [];
    //     medias.map((element,index)=>{
    //       mediasArray.push({
    //         uid: index,
    //         name: 'image.png',
    //         status: 'done',
    //         url: `${process.env.REACT_APP_API_UPLOAD_URL + element.name}`,
    //       });
    //       console.log(mediasArray);
    //       this.setState({fileList : mediasArray});
    //     });
    //   }
    // });
    this.setState({ fileList: this.props.mediasArray });
  };

  handleCancel = () => this.setState({ previewVisible: false });

  handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf('/') + 1)
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      nextProps.mediasArray !== this.props.mediasArray ||
      nextProps.loading !== this.props.loading
    ) {
      this.setState({ fileList: nextProps.mediasArray });
    }
  }

  // handleChange = ({ fileList }) => {
  //     this.setState({ fileList });
  // };

  handleUpload = (file, fileList) => {
    getBase64(file)
      .then(value => {
        this.props.handleUpload(value, file.name);
      })
      .catch(err => message.error('Erreur est survenue'));
  };

  render() {
    const { loading } = this.props;
    const { previewVisible, previewImage, fileList, previewTitle } = this.state;

    const uploadButton = (
      <div>
        <PlusOutlined />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    if (loading) {
      return <Spin />;
    }
    return (
      <div className="clearfix">
        <Upload
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          listType="picture-card"
          fileList={fileList}
          onPreview={this.handlePreview}
          onRemove={this.props.handleRemove}
          beforeUpload={this.handleUpload}
        >
          {fileList.length >= 50 ? null : uploadButton}
        </Upload>
        <Modal
          visible={previewVisible}
          title={previewTitle}
          footer={null}
          onCancel={this.handleCancel}
        >
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}

Medias.propTypes = {
  handleUpload: PropTypes.func,
  mediasArray: PropTypes.any,
  loading: PropTypes.bool,
  handleRemove: PropTypes.func
};

export default Medias;
