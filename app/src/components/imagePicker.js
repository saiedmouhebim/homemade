import React, { Component } from 'react';
import PropTypes, { element } from 'prop-types';
import { Map } from 'immutable';

import Image from './ImageSelection';

class ImagePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      picked: this.props.value ? this.props.value : 0
    };
    this.handleImageClick = this.handleImageClick.bind(this);
    this.renderImage = this.renderImage.bind(this);
  }

  componentDidMount() {}

  handleImageClick(image) {
    const { multiple, onPick } = this.props;
    const pickedImage = multiple ? this.state.picked : Map();
    const newerPickedImage = this.has(pickedImage, image.value)
      ? this.delete(pickedImage, image.value)
      : this.set(pickedImage, image);

    this.setState({ picked: newerPickedImage });

    const pickedImageToArray = [];
    newerPickedImage.map((image, i) => pickedImageToArray.push(image));

    onPick(multiple ? pickedImageToArray : pickedImageToArray[0]);
  }

  has = (array, element) => {
    const returnedArray = array.find(key => key.value === element);
    return returnedArray ? true : false;
  };

  delete = (array, element) => {
    const index = array.findIndex(key => key.value === element);
    array.splice(index, 1);
    return array;
  };

  set = (array, obj) => {
    array.push(obj);
    return array;
  };

  renderImage(image, i) {
    return (
      <Image
        src={image.src}
        isSelected={this.has(this.state.picked, image.value)}
        onImageClick={() => this.handleImageClick(image)}
        key={i}
      />
    );
  }

  render() {
    const { images } = this.props;

    return (
      <div className="image_picker">
        {images.map(this.renderImage)}
        <div className="clear" />
      </div>
    );
  }
}

ImagePicker.propTypes = {
  images: PropTypes.array,
  multiple: PropTypes.bool,
  onPick: PropTypes.func,
  value: PropTypes.any
};

export default ImagePicker;
