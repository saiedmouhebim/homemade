import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ContentState, convertToRaw, EditorState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

export default class EditorConvertToHTML extends Component {
  constructor(props) {
    super(props);

    const html = props.value;

    this.state = {
      html,
      editorState: this.createEditorState(html)
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const html = nextProps.value;

    if (html !== this.state.html) {
      this.setState({
        html,
        editorState: this.createEditorState(html)
      });
    }
  }

  createEditorState = html => {
    const contentBlock = htmlToDraft(html ? html : '<p></p>');

    const contentState = contentBlock
      ? ContentState.createFromBlockArray(contentBlock.contentBlocks)
      : ContentState.createFromText('');

    return EditorState.createWithContent(contentState);
  };

  onEditorStateChange = editorState => {
    const html = draftToHtml(convertToRaw(editorState.getCurrentContent()));

    this.setState(
      {
        html,
        editorState
      },
      () => {
        this.props.onChange && this.props.onChange(html);
      }
    );
  };

  render() {
    const { editorState } = this.state;
    return (
      <div>
        <Editor
          localization={{
            locale: 'fr'
          }}
          mention={{
            separator: ' ',
            trigger: '{',
            suggestions: [
              {
                text: 'Nom complet',
                value: '{var:name}}',
                url: '{var:name}}'
              },
              {
                text: 'Prénom',
                value: '{var:firstName}}',
                url: '{var:firstName}}'
              },
              {
                text: 'Nom',
                value: '{var:lastName}}',
                url: '{var:lastName}}'
              },
              {
                text: 'id',
                value: '{var:id}}',
                url: '{var:id}}'
              }
            ]
          }}
          disabled={this.props.disabled}
          readOnly={this.props.disabled}
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={this.onEditorStateChange}
        >
          <textarea
            disabled
            value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
          />
        </Editor>
      </div>
    );
  }
}

EditorConvertToHTML.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
};
