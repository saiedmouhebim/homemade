import React from 'react';
import { DatePicker } from 'antd';
import PropTypes from 'prop-types';
import moment from 'moment';

class StringDateString extends React.Component {
  handleChange = (date, dateString) => {
    const { onChange } = this.props;
    onChange && onChange(dateString);
  };

  render() {
    const { value, onChange, ...props } = this.props;

    return (
      <DatePicker
        value={moment(value ? value : new Date())}
        onChange={this.handleChange}
        {...props}
      />
    );
  }
}
StringDateString.propTypes = {
  onChange: PropTypes.any,
  value: PropTypes.any
};
export default StringDateString;
