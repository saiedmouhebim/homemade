import React from 'react';
import PropTypes from 'prop-types';
import { AutoComplete } from 'antd';

import {CITIES} from '../components/cities';

class DatePick extends React.Component {
    state = {
        search: '',
        dataSource: [],
    };

    onSearch = searchText => {
        let citirs = CITIES.filter(key =>key.name && key.name.toLowerCase().indexOf(searchText) !== -1).map(elem =>elem.name);
        this.setState({
            dataSource: !searchText ? [] : citirs.length > 20 ? citirs.splice(1,20) : citirs,
        });
    };

    onSelect = (value) => {
        const { onChange } = this.props;

        onChange && onChange({value, autoComplete : true});
    }

    onChange = search => {
        this.setState({ search });
    };

    render() {
        const { dataSource, search } = this.state;

        return (
            <AutoComplete
                value={search}
                dataSource={dataSource}
                style={{ width: 200 }}
                onSelect={this.onSelect}
                onSearch={this.onSearch}
                onChange={this.onChange}
                {...this.props}
                placeholder="Tapper pour filtrer"
            />
        );
    }
}
DatePick.propTypes = {
    value: PropTypes.any,
    onChange: PropTypes.any
};
export default DatePick;
