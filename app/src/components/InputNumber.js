import React from 'react';
import { InputNumber } from 'antd';
import PropTypes from 'prop-types';

class StringInputNumber extends React.Component {
  onChange = value => {
    const { onChange } = this.props;

    onChange && onChange(value.toString());
  };

  render() {
    const { value, onChange, ...props } = this.props;

    return (
      <InputNumber
        {...props}
        onChange={this.onChange}
        value={parseInt(value ? value : 0, 10)}
        maxLength={25}
      />
    );
  }
}
StringInputNumber.propTypes = {
  onChange: PropTypes.any,
  value: PropTypes.any
};
export default StringInputNumber;
