import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getDistance } from 'geolib';
import { HeatMapOutlined } from '@ant-design/icons';
import L from 'leaflet';
import * as ELG from 'esri-leaflet-geocoder';
import { Map, TileLayer, Marker, Popup, Polygon } from 'react-leaflet';

import { fetchAllOrders } from '../modules/orders/orders.ducks';
import client from '../utils/client';

// import marker icons
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl:
    'https://unpkg.com/leaflet@1.4.0/dist/images/marker-icon-2x.png',
  iconUrl: 'https://unpkg.com/leaflet@1.4.0/dist/images/marker-icon.png',
  shadowUrl: 'https://unpkg.com/leaflet@1.4.0/dist/images/marker-shadow.png'
});

class MapComp extends Component {
  componentDidMount() {
    this.props.fetchAllOrders();
    const map = this.leafletMap.leafletElement;
    const searchControl = new ELG.Geosearch().addTo(map);
    const results = new L.LayerGroup().addTo(map);
    if (this.props.value) {
      results.addLayer(L.marker(this.props.value));
    }
    searchControl.on('results', data => {
      results.clearLayers();
      this.props.onChange && this.props.onChange(data.latlng);
      for (let i = data.results.length - 1; i >= 0; i--) {
        results.addLayer(L.marker(data.results[i].latlng));
      }
    });
  }

  render() {
    const center = [36.85724000000005, 10.189320000000066];
    const { orders, clients } = this.props;
    return (
      <Map
        style={{ height: '80vh' }}
        center={center}
        zoom="8"
        ref={m => {
          this.leafletMap = m;
        }}
      >
        <TileLayer
          attribution="&copy; <a href='https://osm.org/copyright'>STS</a> contributors"
          url={'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'}
        />

        <div className="pointer" />
        <Polygon
          positions={orders
            .filter(ord => ord.geometry)
            .map(order => order.geometry.coordinates)}
        />
        {orders.map(({ geometry, _id, client, totalPrice }) => {
          const clientGeometry = {
            latitude: client.location && client.location.coordinates[1],
            longitude: client.location && client.location.coordinates[0]
          };
          const orderGeometry = {
            latitude: geometry && geometry.coordinates[1],
            longitude: geometry && geometry.coordinates[0]
          };
          console.log(clientGeometry, orderGeometry);
          return (
            geometry && (
              <Marker position={geometry.coordinates} key={_id}>
                <Popup>
                  <h3>
                    <strong>Client :</strong>{' '}
                    {clients[client._id] &&
                      clients[client._id].fields.find(
                        field => field.key === 'name'
                      ).value}{' '}
                  </h3>
                  <span>
                    {' '}
                    <strong>Prix TTC :</strong> {Number(totalPrice).toFixed(3)}{' '}
                  </span>
                </Popup>
              </Marker>
            )
          );
        })}
      </Map>
    );
  }
}
MapComp.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.any
};

const mapStateToProps = ({ orders: { ids, byId }, clients }) => ({
  orders: ids.map(id => byId[id]),
  clients: clients.byId
});

const mapDispatchToProps = {
  fetchAllOrders
};

export default connect(mapStateToProps, mapDispatchToProps)(MapComp);
