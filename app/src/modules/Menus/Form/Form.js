import React from 'react';
import { Form, Row, Col, Input, Select  } from 'antd';
import { connect } from 'react-redux';

import {
  ResourceForm,
  ImageUpload,
  DatePick
} from '../../../components';

const DEFAULT_Zone = {
  dishes: [],
  cover: '',
  name :  '',

  description: '',
  quantity : 0,
  price: 0,
  coockTime: 15,
  day: new Date()
};

class UserForm extends ResourceForm {
  render() {
    const FormItemLayout = {
      labelCol: { span: 24 }
    };

    return this.renderForm(({ form, mode, readOnly }) => {
      const { getFieldDecorator } = form;

      return (
        <Row gutter={16}>
          <Col xs={{ span: 24 }} md={{ span: 12 }}>
            <Form.Item {...FormItemLayout} label="Cover">
              {getFieldDecorator('cover')(<ImageUpload disabled={readOnly} />)}
            </Form.Item>
          </Col>

          <Col xs={{ span: 24 }} md={{ span: 18 }}>

            <Row gutter={16}>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Name">
                  {getFieldDecorator('name', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer le nom du plat'
                      }
                    ]
                  })(<Input disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Minute de préparation">
                  {getFieldDecorator('coockTime', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer Minute de préparation'
                      }
                    ]
                  })(<Input disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Description">
                  {getFieldDecorator('description', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer la description'
                      }
                    ]
                  })(<Input.TextArea disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Price">
                  {getFieldDecorator('price')(<Input type="number" disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Quantité">
                  {getFieldDecorator('quantity')(<Input type="number" disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Date de mise en publique">
                  {getFieldDecorator('day')(<DatePick  disabled={readOnly} />)}
                </Form.Item>
              </Col>
              
              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Plats">
                  {getFieldDecorator('dishes', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer le plat'
                      }
                    ]
                  })(
                    <Select mode="multiple" disabled={readOnly}>
                      {this.props.dishes.ids.map(item => (
                        <Select.Option key={item}>
                          {this.props.dishes.byId[item].name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
      );
    });
  }
}

const WrappedForm = Form.create({
  mapPropsToFields(props) {
    const value = props.value || DEFAULT_Zone;

    return {
      dishes: Form.createFormField({ value: value.dishes }),
      cover: Form.createFormField({ value: value.cover }),
      name: Form.createFormField({ value: value.name }),
      description: Form.createFormField({ value: value.description }),
      quantity: Form.createFormField({ value: value.quantity }),
      price: Form.createFormField({ value: value.price }),
      coockTime: Form.createFormField({ value: value.coockTime }),
      day: Form.createFormField({ value: value.day })
    };
  },
  onValuesChange(props, changedValues, allValues) {
    props.onChange && props.onChange(allValues);
  }
})(UserForm);

const mapStateToProps = ({ users: { ids, byId },dishes }) => ({
  users: ids.map(id => byId[id]),
  dishes
});

const withStore = connect(mapStateToProps);

export default withStore(WrappedForm);
