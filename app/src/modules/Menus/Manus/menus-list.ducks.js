import { combineReducers } from 'redux';

import client from '../../../utils/client';
import { getSkipFromLimitAndPage } from '../../../utils/helpers';

export const REQUEST_MENUS_PENDING = 'REQUEST_MENUS_PENDING';
export const REQUEST_MENUS_REJECTED = 'REQUEST_MENUS_REJECTED';
export const REQUEST_MENUS_FULFILLED = 'REQUEST_MENUS_FULFILLED';

export const REMOVE_MENU_PENDING = 'REMOVE_MENU_PENDING';
export const REMOVE_MENU_REJECTED = 'REMOVE_MENU_REJECTED';
export const REMOVE_MENU_FULFILLED = 'REMOVE_MENU_FULFILLED';

// Reducers

const loading = (state = true, { type }) => {
  switch (type) {
    case REQUEST_MENUS_PENDING:
    case REMOVE_MENU_PENDING:
      return true;
    case REQUEST_MENUS_REJECTED:
    case REQUEST_MENUS_FULFILLED:
    case REMOVE_MENU_REJECTED:
    case REMOVE_MENU_FULFILLED:
      return false;
    default:
      return state;
  }
};

const ids = (state = [], { type, payload }) => {
  switch (type) {
    case REQUEST_MENUS_FULFILLED:
      return payload.data.map(user => user._id);
    case REMOVE_MENU_FULFILLED:
      return state.filter(id => {
        return id !== payload._id;
      });
    default:
      return state;
  }
};

const total = (state = 0, { type, payload }) => {
  switch (type) {
    case REQUEST_MENUS_FULFILLED:
      return payload.total;
    default:
      return state;
  }
};

const page = (state = 1, { type, payload }) => {
  switch (type) {
    case REQUEST_MENUS_FULFILLED:
      return payload.skip / payload.limit + 1;
    default:
      return state;
  }
};

export default combineReducers({
  loading,
  ids,
  page,
  total
});

// Action Creators

export const fetchmenus = ({
  page = 1,
  limit = 10,
  sort: $sort = { createdAt: -1 },
  filter = {}
}) => {

  const query = {
    query: {
      $limit: limit,
      $skip: getSkipFromLimitAndPage(limit, page),
      $sort,
      ...filter
    }
  };

  return {
    type: 'REQUEST_MENUS',
    payload: client.service('menus').find(query)
  };
};

export const deletemenu = id => {
  return {
    type: 'REMOVE_MENU',
    payload: client.service('menus').remove(id)
  };
};
