import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Avatar, Row, Col, Button } from 'antd';

import { convertTableMeta } from '../../../utils/helpers';
import { Actions, DataTable } from '../../../components';

import { fetchmenus, deletemenu } from './menus-list.ducks';

class Dishes extends Component {
  componentDidMount() {
    this.getMenusData({ page: 1 });
  }

  getMenusData = ({ page, limit = 10, filter, sort }) => {
    filter = { ...filter,
      chef: this.props.userId};
    this.props.fetchmenus({ page, limit, filter, sort });
  };

  onTableChange = (pagination, filters, sorter) => {
    filters = { ...filters,
      chef: this.props.userId};
    this.getMenusData(
      convertTableMeta(pagination, filters, sorter, {
        name: 'fullName',
        type: 'array'
      })
    );
  };

  render() {
    const { loading, page, total, menus } = this.props;

    const columns = [
      {
        title: 'Photo',
        key: 'photo',
        render: (text, record) => <Avatar src={record.cover} icon="user" />
      },
      {
        title: 'name',
        key: 'name',
        render: (text, record) => <p>{record.name}</p>
      },
      {
        title: 'Prix',
        key: 'Prix',
        render: (text, record) => <p>{record.price}</p>
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <Actions
            showUpdate
            showDelete
            selectUrl={`/menu/${record._id}`}
            onDelete={() => {
              this.props.deletemenu(record._id);
            }}
          />
        )
      }
    ];

    return (
      <div>
        <Row>
          <Col>
            <h1>Gestion des Menu</h1>
            <DataTable
              loading={loading}
              dataSource={menus}
              columns={columns}
              rowKey="_id"
              pagination={{
                current: page,
                pageSize: 10,
                total
              }}
              onChange={this.onTableChange}
            />
          </Col>
        </Row>
        <Row style={{ marginTop: '1rem', marginBottom: '1rem' }}>
          <Col style={{ textAlign: 'right' }}>
            <Link to="/menu/new">
              <Button type="primary" icon="plus-square-o">
                Ajout d'un menu
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
    );
  }
}

Dishes.propTypes = {
  deletemenu: PropTypes.func,
  fetchmenus: PropTypes.func.isRequired,
  userId: PropTypes.any,
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  menus: PropTypes.arrayOf(PropTypes.object).isRequired
};

const mapStateToProps = ({
  menus: {
    byId,
    list: { ids, loading, total, page }
  },
  auth,
}) => ({
  auth,
  menus: ids.map(id => byId[id]),
  page,
  loading,
  total,
  userType: auth.user.type,
  userId: auth.user._id,
});

const mapDispatchToProps = { fetchmenus, deletemenu };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(Dishes);
