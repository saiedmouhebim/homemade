import { combineReducers } from 'redux';

import client from '../../../utils/client';

export const REQUEST_MENU_PENDING = 'REQUEST_MENUS_PENDING';
export const REQUEST_MENU_REJECTED = 'REQUEST_MENUS_REJECTED';
export const REQUEST_MENU_FULFILLED = 'REQUEST_MENUS_FULFILLED';

export const UPDATE_MENU_PENDING = 'UPDATE_MENU_PENDING';
export const UPDATE_MENU_REJECTED = 'UPDATE_MENU_REJECTED';
export const UPDATE_MENU_FULFILLED = 'UPDATE_MENU_FULFILLED';

// Reducers

const loading = (state = false, { type }) => {
  switch (type) {
    case REQUEST_MENU_PENDING:
    case UPDATE_MENU_PENDING:
      return true;
    case REQUEST_MENU_REJECTED:
    case UPDATE_MENU_REJECTED:
    case REQUEST_MENU_FULFILLED:
    case UPDATE_MENU_FULFILLED:
      return false;
    default:
      return state;
  }
};

export default combineReducers({ loading });

// Action Creators

export const fetchMenu = id => {
  return (dispatch, getState) => {
    const user = getState().menus.byId[id];

    return dispatch({
      type: 'REQUEST_MENU',
      payload: user ? Promise.resolve(user) : client.service('menus').get(id)
    });
  };
};

export const updateMenu = (id, data) => {
  return {
    type: 'UPDATE_MENU',
    payload: client.service('menus').update(id, data)
  };
};
