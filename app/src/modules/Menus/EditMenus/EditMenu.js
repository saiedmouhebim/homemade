import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Spin, message } from 'antd';

import Form from '../Form';

import { fetchMenu, updateMenu } from './edit-Menu.ducks';

class EditDish extends Component {
  state = { id: null, menu: null };

  componentDidMount = () => {
    const {
      match: {
        params: { id }
      }
    } = this.props;

    this.props.fetchMenu(id).then(({ value: menu }) => {
      this.form.setPristine();
      this.setState({ id, menu });
    });
  };

  handleChange = values => {
    this.setState({ menu: values });
  };

  handleSubmit = values => {
    let { id, menu } = this.state;
    menu.chef = this.props.userId;
    this.props
      .updateMenu(id, menu)
      .then(() => {
        message.success('Menu modifié avec succès');
        this.form.setReadOnly();
        this.form.setPristine();
        this.setState({ menu: Object.assign({}, menu) });
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleCancel = () => {
    const { id } = this.state;
    const {
      menus: { byId }
    } = this.props;

    this.form.setReadOnly();
    this.form.setPristine();

    this.setState({ menu: byId[id] });
  };

  render() {
    const { menu } = this.state;
    const { loading } = this.props;

    return (
      <div>
        <h1>Détails/Modification d'un menu</h1>
        <Spin spinning={ loading}>
          <Form
            wrappedComponentRef={form => (this.form = form)}
            mode="edit"
            value={menu}
            onChange={this.handleChange}
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            loading={loading}
          />
        </Spin>
      </div>
    );
  }
}

EditDish.propTypes = {
  menus: PropTypes.object,
  loading: PropTypes.bool,
  match: PropTypes.object.isRequired,
  userId: PropTypes.any,
  fetchMenu: PropTypes.func,
  updateMenu: PropTypes.func
};

const mapStateToProps = ({ menus ,auth}) => ({
  menus,
  loading: menus.edit.loading,
  userId: auth.user._id,

});

const mapDispatchToProps = { fetchMenu, updateMenu };

const withStore = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withStore(EditDish);
