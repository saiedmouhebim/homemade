import { combineReducers } from 'redux';

import client from '../../utils/client';

import menusList, { REQUEST_MENUS_FULFILLED } from './Manus/menus-list.ducks';
import addMenu, { ADD_MENU_FULFILLED } from './AddMenu/add-Menu.ducks';
import editMenu, {
  REQUEST_MENU_FULFILLED,
  UPDATE_MENU_FULFILLED
} from './EditMenus/edit-Menu.ducks';

export const REQUEST_ALL_MENUS_PENDING = 'REQUEST_ALL_MENUS_PENDING';
export const REQUEST_ALL_MENUS_REJECTED = 'REQUEST_ALL_MENUS_REJECTED';
export const REQUEST_ALL_MENUS_FULFILLED = 'REQUEST_ALL_MENUS_FULFILLED';
// Reducers

const ids = (state = [], { type, payload }) => {
  switch (type) {
    case REQUEST_MENUS_FULFILLED:
      return [...new Set(state.concat(payload.data.map(user => user._id)))];
    case REQUEST_ALL_MENUS_FULFILLED:
      return [...new Set(state.concat(payload.map(user => user._id)))];
    case ADD_MENU_FULFILLED:
    case REQUEST_MENU_FULFILLED:
    case UPDATE_MENU_FULFILLED:
      return [payload._id, ...state];
    default:
      return state;
  }
};

const byId = (state = {}, { type, payload }) => {
  switch (type) {
    case REQUEST_MENUS_FULFILLED:
      return payload.data.reduce(
        (res, user) => Object.assign({}, { [user._id]: user }, res),
        state
      );
    case REQUEST_ALL_MENUS_FULFILLED:
      return payload.reduce(
        (res, users) => Object.assign({}, { [users._id]: users }, res),
        state
      );
    case ADD_MENU_FULFILLED:
    case REQUEST_MENU_FULFILLED:
    case UPDATE_MENU_FULFILLED:
      return Object.assign({}, state, { [payload._id]: payload });
    default:
      return state;
  }
};

export default combineReducers({
  ids,
  byId,
  list: menusList,
  add: addMenu,
  edit: editMenu
});

export const fetchAllMenus= () => {
  const query = {
    query: { $limit: -1 }
  };

  return {
    type: 'REQUEST_ALL_MENUS',
    payload: client.service('menus').find(query)
  };
};
