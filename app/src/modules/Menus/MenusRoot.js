import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import {fetchAllDishes} from '../Dishes/dishes.ducks';

import AddMenu from './AddMenu';
import Manus from './Manus';
import EditMenus from './EditMenus';

class ZonesRoot extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    fetchAllDishes: PropTypes.func,
  };

  componentDidMount() {
    this.props.fetchAllDishes();
  }

  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Route exact path={`${match.url}/`} component={Manus} />
        <Route exact path={`${match.url}/new`} component={AddMenu} />
        <Route exact path={`${match.url}/:id`} component={EditMenus} />
      </Switch>
    );
  }
}

const mapDispatchToProps = {
  fetchAllDishes
};

const withStore = connect(null, mapDispatchToProps);

export default withStore(ZonesRoot);
