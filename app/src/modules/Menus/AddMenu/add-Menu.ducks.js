import { combineReducers } from 'redux';

import client from '../../../utils/client';

export const ADD_MENU_PENDING = 'ADD_MENU_PENDING';
export const ADD_MENU_REJECTED = 'ADD_MENU_REJECTED';
export const ADD_MENU_FULFILLED = 'ADD_MENU_FULFILLED';

// Reducers

const loading = (state = false, { type }) => {
  switch (type) {
    case ADD_MENU_PENDING:
      return true;
    case ADD_MENU_REJECTED:
    case ADD_MENU_FULFILLED:
      return false;
    default:
      return state;
  }
};

export default combineReducers({ loading });

// Action Creators

export const addMenu = data => {
  return {
    type: 'ADD_MENU',
    payload: client.service('menus').create(data)
  };
};
