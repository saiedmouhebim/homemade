import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { message } from 'antd';

import Form from '../Form';

import { addMenu } from './add-Menu.ducks';

class AddMenu extends Component {
  state = {
    menu: null
  };

  handleChange = values => {
    
    this.setState({ menu: values });
  };

  handleSubmit = values => {
    let {menu} = this.state;
    menu.chef = this.props.userId;
    this.props
      .addMenu(menu)
      .then(({ value }) => {
        message.success('Menu créé avec succès');
        this.props.history.push(`/menu/${value._id}`);
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleSubmitNew = values => {
    let {menu} = this.state;
    menu.chef = this.props.userId;
    this.props
      .addMenu(menu)
      .then(({ value }) => {
        message.success('Menu créé avec succès');
        this.setState({ menu: null });
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleCancel = () => {
    this.props.history.push('/menu');
  };

  render() {
    const { loading } = this.props;

    return (
      <div>
        <h1>Création d'un menu</h1>
        <Form
          wrappedComponentRef={form => (this.form = form)}
          value={this.state.menu}
          onChange={this.handleChange}
          onSubmit={this.handleSubmit}
          onCancel={this.handleCancel}
          submitNew={this.handleSubmitNew}
          addNew
          loading={loading}
        />
      </div>
    );
  }
}

AddMenu.propTypes = {
  history: PropTypes.object,
  addMenu: PropTypes.func,
  loading: PropTypes.bool,
  userId: PropTypes.any
};

const mapStateToProps = ({
  menus: {
    add: { loading }
  },
  auth
}) => ({
  loading,
  userId: auth.user._id,
});

const mapDispatchToProps = { addMenu };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(AddMenu);
