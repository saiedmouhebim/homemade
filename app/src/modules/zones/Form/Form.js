import React from 'react';
import { Form, Row, Col, Input } from 'antd';
import { connect } from 'react-redux';

import {
  ResourceForm,
  Autocomplete
} from '../../../components';

const DEFAULT_Zone = {
  adress1: '',
  city: '',
  state: '',
  lat: 36,
  log: 10,
};

class UserForm extends ResourceForm {
  render() {
    const FormItemLayout = {
      labelCol: { span: 24 }
    };

    return this.renderForm(({ form, mode, readOnly }) => {
      const { getFieldDecorator } = form;

      return (
        <Row gutter={16}>
          <Col xs={{ span: 24 }} md={{ span: 12 }}>
          Sélection automatique
            <Form.Item {...FormItemLayout} label="adress">
              {getFieldDecorator('adress1', {
                rules: [
                  {
                    required: true,
                    message: 'Veuillez entrer l adresse'
                  }
                ]
              })(<Autocomplete disabled={readOnly}/>)}
            </Form.Item>
          </Col>

          <Col xs={{ span: 24 }} md={{ span: 18 }}>
          Selection Manuelle

            <Row gutter={16}>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="City">
                  {getFieldDecorator('city', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer le city'
                      }
                    ]
                  })(<Input disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="State">
                  {getFieldDecorator('state', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer le state'
                      }
                    ]
                  })(<Input disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="latitude">
                  {getFieldDecorator('lat')(<Input type="number" disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="logitude">
                  {getFieldDecorator('log')(<Input type="number" disabled={readOnly} />)}
                </Form.Item>
              </Col>

            </Row>
          </Col>
        </Row>
      );
    });
  }
}

const WrappedForm = Form.create({
  mapPropsToFields(props) {
    const value = props.value || DEFAULT_Zone;

    return {
      adress1: Form.createFormField({ value: value.adress1 }),
      state: Form.createFormField({ value: value.state }),
      city: Form.createFormField({ value: value.city }),
      lat: Form.createFormField({ value: value.lat }),
      log: Form.createFormField({ value: value.log }),

    };
  },
  onValuesChange(props, changedValues, allValues) {
    props.onChange && props.onChange(allValues);
  }
})(UserForm);

const mapStateToProps = ({ users: { ids, byId } }) => ({
  users: ids.map(id => byId[id])
});

const withStore = connect(mapStateToProps);

export default withStore(WrappedForm);
