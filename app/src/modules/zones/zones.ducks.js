import { combineReducers } from 'redux';

import client from '../../utils/client';

import zonesList, { REQUEST_ZONES_FULFILLED } from './Zones/zones-list.ducks';
import addZone, { ADD_ZONE_FULFILLED } from './AddZone/add-zone.ducks';
import editZone, {
  REQUEST_ZONE_FULFILLED,
  UPDATE_ZONE_FULFILLED
} from './EditZone/edit-zone.ducks';

export const REQUEST_ALL_ZONES_PENDING = 'REQUEST_ALL_ZONES_PENDING';
export const REQUEST_ALL_ZONES_REJECTED = 'REQUEST_ALL_ZONES_REJECTED';
export const REQUEST_ALL_ZONES_FULFILLED = 'REQUEST_ALL_ZONES_FULFILLED';
// Reducers

const ids = (state = [], { type, payload }) => {
  switch (type) {
    case REQUEST_ZONES_FULFILLED:
      return [...new Set(state.concat(payload.data.map(user => user._id)))];
    case REQUEST_ALL_ZONES_FULFILLED:
      return [...new Set(state.concat(payload.map(user => user._id)))];
    case ADD_ZONE_FULFILLED:
    case REQUEST_ZONE_FULFILLED:
    case UPDATE_ZONE_FULFILLED:
      return [payload._id, ...state];
    default:
      return state;
  }
};

const byId = (state = {}, { type, payload }) => {
  switch (type) {
    case REQUEST_ZONES_FULFILLED:
      return payload.data.reduce(
        (res, user) => Object.assign({}, { [user._id]: user }, res),
        state
      );
    case REQUEST_ALL_ZONES_FULFILLED:
      return payload.reduce(
        (res, users) => Object.assign({}, { [users._id]: users }, res),
        state
      );
    case ADD_ZONE_FULFILLED:
    case REQUEST_ZONE_FULFILLED:
    case UPDATE_ZONE_FULFILLED:
      return Object.assign({}, state, { [payload._id]: payload });
    default:
      return state;
  }
};

export default combineReducers({
  ids,
  byId,
  list: zonesList,
  add: addZone,
  edit: editZone
});

export const fetchAllzones = () => {
  const query = {
    query: { $limit: -1 }
  };

  return {
    type: 'REQUEST_ALL_ZONES',
    payload: client.service('zones').find(query)
  };
};
