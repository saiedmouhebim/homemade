import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Spin, message } from 'antd';

import Form from '../Form';
import { CITIES } from '../../../components/cities';

import { fetchZone, updateZone } from './edit-zone.ducks';

class EditZone extends Component {
  state = { id: null, zone: null };

  componentDidMount = () => {
    const {
      match: {
        params: { id }
      }
    } = this.props;

    this.props.fetchZone(id).then(({ value: zone }) => {
      this.form.setPristine();
      this.setState({ id, zone });
    });
  };

  handleChange = values => {
    let autocomplete = false;
    if(values.adress1.autoComplete) {
      values.adress1 = values.adress1.value;
      autocomplete = true;
    }
    this.form.setPristine(false);
    if(autocomplete) {
      const item = CITIES.find(key =>key.name.toLowerCase() === values.adress1.toLowerCase());
      values.city = item.name;
      values.state = item.state;
      values.lat = parseFloat(item.coords.lat);
      values.log = parseFloat(item.coords.lon);
    }
    this.setState({ zone: values });
  };

  handleSubmit = values => {
    const { id, zone } = this.state;

    this.props
      .updateZone(id, zone)
      .then(() => {
        message.success('Zone modifié avec succès');
        this.form.setReadOnly();
        this.form.setPristine();
        this.setState({ zone: Object.assign({}, zone) });
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleCancel = () => {
    const { id } = this.state;
    const {
      zones: { byId }
    } = this.props;

    this.form.setReadOnly();
    this.form.setPristine();

    this.setState({ zone: byId[id] });
  };

  render() {
    const { zone } = this.state;
    const { loading } = this.props;

    return (
      <div>
        <h1>Détails/Modification d'une zone</h1>
        <Spin spinning={ loading}>
          <Form
            wrappedComponentRef={form => (this.form = form)}
            mode="edit"
            value={zone}
            onChange={this.handleChange}
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            loading={loading}
          />
        </Spin>
      </div>
    );
  }
}

EditZone.propTypes = {
  zones: PropTypes.object,
  loading: PropTypes.bool,
  match: PropTypes.object.isRequired,
  fetchZone: PropTypes.func,
  updateZone: PropTypes.func
};

const mapStateToProps = ({ zones }) => ({
  zones,
  loading: zones.edit.loading
});

const mapDispatchToProps = { fetchZone, updateZone };

const withStore = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withStore(EditZone);
