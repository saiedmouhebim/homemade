import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {  Row, Col, Button } from 'antd';

import { convertTableMeta } from '../../../utils/helpers';
import { Actions, DataTable } from '../../../components';

import { fetchZones, deleteZone } from './zones-list.ducks';

class Users extends Component {
  componentDidMount() {
    this.getZonesData({ page: 1 });
  }

  getZonesData = ({ page, limit = 10, filter, sort }) => {
    this.props.fetchZones({ page, limit, filter, sort });
  };

  onTableChange = (pagination, filters, sorter) => {
    this.getZonesData(
      convertTableMeta(pagination, filters, sorter, {
        name: 'fullName',
        type: 'array'
      })
    );
  };

  render() {
    const { loading, page, total, zones } = this.props;

    const columns = [
      {
        title: 'State',
        key: 'adresse',
        render: (text, record) => <div  >{record.state}</div>
      },
      {
        title: 'City',
        key: 'adresse',
        render: (text, record) => <div  >{record.city}</div>
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <Actions
            showUpdate
            showDelete
            selectUrl={`/zones/${record._id}`}
            onDelete={() => {
              this.props.deleteZone(record._id);
            }}
          />
        )
      }
    ];

    return (
      <div>
        <Row>
          <Col>
            <h1>Gestion des Zones</h1>
            <DataTable
              loading={loading}
              dataSource={zones}
              columns={columns}
              rowKey="_id"
              pagination={{
                current: page,
                pageSize: 10,
                total
              }}
              onChange={this.onTableChange}
            />
          </Col>
        </Row>
        <Row style={{ marginTop: '1rem', marginBottom: '1rem' }}>
          <Col style={{ textAlign: 'right' }}>
            <Link to="/zones/new">
              <Button type="primary" icon="plus-square-o">
                Ajout d'une zone
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
    );
  }
}

Users.propTypes = {
  deleteZone: PropTypes.func,
  fetchZones: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  zones: PropTypes.arrayOf(PropTypes.object).isRequired
};

const mapStateToProps = ({
  zones: {
    byId,
    list: { ids, loading, total, page }
  },
  auth,
}) => ({
  auth,
  zones: ids.map(id => byId[id]),
  page,
  loading,
  total,
  userType: auth.user.type,
  userId: auth.user._id,
});

const mapDispatchToProps = { fetchZones, deleteZone };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(Users);
