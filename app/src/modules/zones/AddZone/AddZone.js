import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { message } from 'antd';

import Form from '../Form';
import { CITIES } from '../../../components/cities';

import { addZone } from './add-zone.ducks';

class AddZone extends Component {
  state = {
    zone: null
  };

  handleChange = values => {
    let autocomplete = false;
    if(values.adress1.autoComplete) {
      values.adress1 = values.adress1.value;
      autocomplete = true;
    }
    this.form.setPristine(false);
    if(autocomplete) {
      const item = CITIES.find(key =>key.name.toLowerCase() === values.adress1.toLowerCase());
      values.city = item.name;
      values.state = item.state;
      values.lat = parseFloat(item.coords.lat);
      values.log = parseFloat(item.coords.lon);
    }
    this.setState({ zone: values });
  };

  handleSubmit = values => {
    this.props
      .addZone(values)
      .then(({ value }) => {
        message.success('Zone créé avec succès');
        this.props.history.push(`/zones/${value._id}`);
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleSubmitNew = values => {
    this.props
      .addZone(this.state.zone)
      .then(({ value }) => {
        message.success('Zone créé avec succès');
        this.setState({ zone: null });
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleCancel = () => {
    this.props.history.push('/zones');
  };

  render() {
    const { loading } = this.props;

    return (
      <div>
        <h1>Création d'une zone</h1>
        <Form
          wrappedComponentRef={form => (this.form = form)}
          value={this.state.zone}
          onChange={this.handleChange}
          onSubmit={this.handleSubmit}
          onCancel={this.handleCancel}
          submitNew={this.handleSubmitNew}
          addNew
          loading={loading}
        />
      </div>
    );
  }
}

AddZone.propTypes = {
  history: PropTypes.object,
  addZone: PropTypes.func,
  loading: PropTypes.bool
};

const mapStateToProps = ({
  zones: {
    add: { loading }
  }
}) => ({
  loading
});

const mapDispatchToProps = { addZone };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(AddZone);
