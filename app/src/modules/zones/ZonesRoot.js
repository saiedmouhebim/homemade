import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import Zones from './Zones';
import AddZone from './AddZone';
import EditZone from './EditZone';

class ZonesRoot extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
  };

  componentDidMount() {
  }

  render() {
    const { match } = this.props;

    return (
      <Switch>
        <Route exact path={`${match.url}/`} component={Zones} />
        <Route exact path={`${match.url}/new`} component={AddZone} />
        <Route exact path={`${match.url}/:id`} component={EditZone} />
      </Switch>
    );
  }
}

const mapDispatchToProps = {
};

const withStore = connect(null, mapDispatchToProps);

export default withStore(ZonesRoot);
