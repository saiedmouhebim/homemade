import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout, BackTop } from 'antd';
import { Route, Switch, Redirect } from 'react-router-dom';

import { logout } from '../auth/auth.ducks';
import Users from '../users';
import Zones from '../zones';
import Dishes from '../Dishes';
import Menus from '../Menus';
import AdminsOrders from '../Adminsorders';
import ClientOrders from '../Clientorders';

import Breadcrumbs from './Breadcrumbs';
import SideMenu from './SideMenu';

const styles = {
  content: {
    background: '#fff',
    padding: '1.5rem',
    margin: 0
  },
  layout: { padding: '0 1.5rem 1.5rem' }
};

class Dashboard extends Component {
  static propTypes = {
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired
    }).isRequired,
  };

  render() {
    const { location } = this.props;

    //if (getDefaultPage(user))
    return (
      <Layout>
        <BackTop />
        <SideMenu path={location.pathname} />
        <Layout style={styles.layout}>
          <Breadcrumbs path={location.pathname} />
          <Layout.Content style={styles.content}>
            <Switch>
              {/* {hasAccess(user, 'users') && (
                <Route path="/users" component={Users} />
              )} */}
              <Route path="/users" component={Users} />
              <Route path="/dish" component={Dishes} />
              <Route path="/menu" component={Menus} />
              <Route path="/Aorders" component={AdminsOrders} />
              <Route path="/zones" component={Zones} />
              <Route path="/Corders" component={ClientOrders} />
            
              
                <Redirect to={'/users'} />
            </Switch>
          </Layout.Content>
        </Layout>
      </Layout>
    );
    // else {
    //   this.props.logout();
    //   return null;
    // }
  }
}

const mapStateToProps = ({ auth }) => ({
  user: auth.user
});

const mapDispatchToProps = { logout };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(Dashboard);
