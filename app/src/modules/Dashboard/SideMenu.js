import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout, Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';

import { PAGES_BY_KEY } from '../../const';

const menuItems = [
 
  PAGES_BY_KEY['users'],
  PAGES_BY_KEY['zones'],
  PAGES_BY_KEY['dish'],
  PAGES_BY_KEY['menu'],
  PAGES_BY_KEY['aorders'],
  PAGES_BY_KEY['corders'],
  
];

function SideMenu({ path, user }) {
  const selectedKey = '/' + path.split('/')[1];

  return (
    <Layout.Sider breakpoint="lg" collapsedWidth="0">
      <Menu
        mode="inline"
        defaultSelectedKeys={[selectedKey]}
        selectedKeys={[selectedKey]}
        defaultOpenKeys={['monitoring', 'tariffs']}
        style={{ height: '100%', borderRight: 0 }}
      >
        {renderMenuItems(menuItems, user)}
      </Menu>
    </Layout.Sider>
  );
}

SideMenu.propTypes = {
  path: PropTypes.string.isRequired,
  user: PropTypes.object
};

function showMenuItem(item, user) {
  
  return item.target.includes(user.type);
  
}

function renderMenuItems(menuItems, user) {
  return menuItems
    .filter(item => showMenuItem(item, user))
    .map((item, index) => {
      if (item.subMenus) {
        return (
          <Menu.SubMenu
            key={item.key}
            title={
              <span>
                <Icon type={item.icon} />
                {item.label}
              </span>
            }
          >
            {renderMenuItems(item.subMenus, user)}
          </Menu.SubMenu>
        );
      } else {
        return (
          <Menu.Item key={item.to}>
            <Link to={item.to} style={{ display: 'flex' }}>
              <Icon type={item.icon} /> {item.label}
            </Link>
          </Menu.Item>
        );
      }
    });
}

const mapStateToProps = ({ auth }) => ({
  user: auth.user
});

const withStore = connect(mapStateToProps);

export default withStore(SideMenu);
