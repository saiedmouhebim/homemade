import React from 'react';
import PropTypes from 'prop-types';
import { Breadcrumb } from 'antd';
import { NavLink } from 'react-router-dom';

const pathsMap = {
  users: 'Utilisateurs',
  zones: 'Zone',
  dish: 'Plat',
  Aorders: 'Commande',
  Corders: 'Commande',
  menu: 'menu',
};

export default function Breadcrumbs({ path }) {
  const paths = path.split('/');

  paths.shift();

  return (
    <Breadcrumb style={{ margin: '1rem 0' }}>
      {paths.map((item, index, array) => (
        <Breadcrumb.Item key={item}>
          <NavLink to={`/${array.slice(0, index + 1).join('/')}`}>
            {pathsMap[item] || (index === paths.length - 1 && 'Détails')}
          </NavLink>
        </Breadcrumb.Item>
      ))}
    </Breadcrumb>
  );
}

Breadcrumbs.propTypes = {
  path: PropTypes.string.isRequired
};
