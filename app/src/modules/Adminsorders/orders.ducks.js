import { combineReducers } from 'redux';

import ordersList, { REQUEST_ORDERS_FULFILLED } from './orders/orders-list.ducks';
// Reducers

const ids = (state = [], { type, payload }) => {
  switch (type) {
    case REQUEST_ORDERS_FULFILLED:
      return [...new Set(state.concat(payload.data.map(user => user._id)))];
    default:
      return state;
  }
};

const byId = (state = {}, { type, payload }) => {
  switch (type) {
    case REQUEST_ORDERS_FULFILLED:
      return payload.data.reduce(
        (res, user) => Object.assign({}, { [user._id]: user }, res),
        state
      );
    default:
      return state;
  }
};

export default combineReducers({
  ids,
  byId,
  list: ordersList,
});
