import { combineReducers } from 'redux';

import client from '../../../utils/client';
import { getSkipFromLimitAndPage } from '../../../utils/helpers';

export const REQUEST_ORDERS_PENDING = 'REQUEST_ORDERS_PENDING';
export const REQUEST_ORDERS_REJECTED = 'REQUEST_ORDERS_REJECTED';
export const REQUEST_ORDERS_FULFILLED = 'REQUEST_ORDERS_FULFILLED';

// Reducers

const loading = (state = true, { type }) => {
  switch (type) {
    case REQUEST_ORDERS_PENDING:
      return true;
    case REQUEST_ORDERS_REJECTED:
    case REQUEST_ORDERS_FULFILLED:
      return false;
    default:
      return state;
  }
};

const ids = (state = [], { type, payload }) => {
  switch (type) {
    case REQUEST_ORDERS_FULFILLED:
      return payload.data.map(user => user._id);
    default:
      return state;
  }
};

const total = (state = 0, { type, payload }) => {
  switch (type) {
    case REQUEST_ORDERS_FULFILLED:
      return payload.total;
    default:
      return state;
  }
};

const page = (state = 1, { type, payload }) => {
  switch (type) {
    case REQUEST_ORDERS_FULFILLED:
      return payload.skip / payload.limit + 1;
    default:
      return state;
  }
};

export default combineReducers({
  loading,
  ids,
  page,
  total
});

// Action Creators

export const fetchOrders = ({
  page = 1,
  limit = 10,
  sort: $sort = { createdAt: -1 },
  filter = {}
}) => {

  const query = {
    query: {
      $limit: limit,
      $skip: getSkipFromLimitAndPage(limit, page),
      $sort,
      ...filter
    }
  };

  return {
    type: 'REQUEST_ORDERS',
    payload: client.service('orders').find(query)
  };
};
