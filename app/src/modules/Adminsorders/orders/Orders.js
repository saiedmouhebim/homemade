import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {  Row, Col } from 'antd';

import { convertTableMeta } from '../../../utils/helpers';
import {  DataTable } from '../../../components';

import { fetchOrders } from './orders-list.ducks';

class Users extends Component {
  componentDidMount() {
    this.getOrdersData({ page: 1 });
  }

  getOrdersData = ({ page, limit = 10, filter, sort }) => {
    this.props.fetchOrders({ page, limit, filter, sort });
  };

  onTableChange = (pagination, filters, sorter) => {
    this.getOrdersData(
      convertTableMeta(pagination, filters, sorter, {
        name: 'fullName',
        type: 'array'
      })
    );
  };

  render() {
    const { loading, page, total, orders } = this.props;

    const columns = [
      {
        title: 'Ref',
        key: 'ref',
        render: (text, record) => <div  >{record.ref}</div>
      },
      {
        title: 'Prix TTC',
        key: 'totalPriceInclTax',
        render: (text, record) => <div  >{record.totalPriceInclTax} EUR</div>
      },
      {
        title: 'Validé ?',
        key: 'valid',
        render: (text, record) => <div  >{record.valid ? 'Validée' : 'Non validée'}</div>
      },
      {
        title: 'Payéé ?',
        key: 'checked',
        render: (text, record) => <div  >{record.checked ? 'Payée' : 'Non payée'}</div>
      },
    ];

    return (
      <div>
        <Row>
          <Col>
            <h1>Gestion des Commandes</h1>
            <DataTable
              loading={loading}
              dataSource={orders}
              columns={columns}
              rowKey="_id"
              pagination={{
                current: page,
                pageSize: 10,
                total
              }}
              onChange={this.onTableChange}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

Users.propTypes = {
  fetchOrders: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  orders: PropTypes.arrayOf(PropTypes.object).isRequired
};

const mapStateToProps = ({
  orders: {
    byId,
    list: { ids, loading, total, page }
  },
  auth,
}) => ({
  auth,
  orders: ids.map(id => byId[id]),
  page,
  loading,
  total,
  userType: auth.user.type,
  userId: auth.user._id,
});

const mapDispatchToProps = { fetchOrders };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(Users);
