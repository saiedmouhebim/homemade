import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import orders from './orders';

class ZonesRoot extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
  };

  componentDidMount() {
  }

  render() {
    const { match } = this.props;

    return (
      <Switch>
        <Route exact path={`${match.url}/`} component={orders} />
      </Switch>
    );
  }
}

const mapDispatchToProps = {
};

const withStore = connect(null, mapDispatchToProps);

export default withStore(ZonesRoot);
