import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Layout,
  Avatar,
  Menu,
  Icon,
  Dropdown
} from 'antd';
import {  AntDesignOutlined } from '@ant-design/icons';

import logo from '../../images/homemade.png';
import { logout } from '../auth/auth.ducks';

class Header extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    user: PropTypes.object,
    location: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired
  };

  handleMenuClick = ({ key }) => {
    if (key === 'logout') {
      this.props.logout();
    }
  };

  render() {
    const {
      isAuthenticated,
      user,
      location: { pathname },
    } = this.props;
    const loginPage = pathname === '/login';

    const style = {
      header: {
        color: '#fff',
        boxSizing: 'content-box'
      }
    };

    if (loginPage) {
      style.header.height = 200;
    } else {
      style.header.borderBottom = '8px solid orange';
    }

    const menu = (
      <Menu>
        <Menu onClick={this.handleMenuClick}>
          <Menu.Item key="logout">
            <Icon type="logout" /> Déconnexion
          </Menu.Item>
        </Menu>
      </Menu>
    );

    return (
      <Layout.Header style={style.header}>
        <Row
          type="flex"
          justify="space-between"
          align={loginPage ? 'top' : 'middle'}
          gutter={8}
          style={{ flexWrap: 'nowrap', height: '100%' }}
        >
          <Col style={{ flex: '1 0 auto' }}>
            <Row type="flex" gutter={16} align="middle">
              {!loginPage && (
                <Col>
                  <Avatar
                    shape="square"
                    size="xlarge"
                    icon={<AntDesignOutlined />}
                    src={logo}
                  />
                </Col>
              )}
              <Col
                className="header-title"
                style={{ lineHeight: 1, marginTop: loginPage ? '1rem' : 0 }}
              >
                <div
                  style={{ fontSize: '32px', fontWeight: 200, marginBottom: 8 , color : 'grey'}}
                >
                  HOMEMADE
                </div>
                <div
                  style={{
                    fontSize: '24px',
                    color: 'red',
                    fontStyle: 'italic',
                    fontWeight: 200
                  }}
                >
                  Back office 
                </div>
              </Col>
            </Row>
          </Col>
          {isAuthenticated && (
            <React.Fragment>
              <Col
                style={{ textAlign: 'right', lineHeight: 1.2, marginRight: 15 }}
              >
                <div style={{ fontWeight: 'bold' }}>
                  {user.firstName} {user.lastName}
                </div>
                <div style={{ fontSize: '.8rem', color: '#f5222d' }}>
                  {user.type}
                </div>
              </Col>
              <Col>
                <Dropdown overlay={menu}>
                  <Avatar
                    className="user-photo"
                    style={{ cursor: 'pointer' }}
                    size="large"
                    src={user.photo}
                    icon="user"
                  />
                </Dropdown>
              </Col>
            </React.Fragment>
          )}
        </Row>
      </Layout.Header>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  isAuthenticated: !!auth.user,
  user: auth.user,
});

const mapDispatchToProps = { logout };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(Header);
