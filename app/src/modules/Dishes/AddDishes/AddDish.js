import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { message } from 'antd';

import Form from '../Form';

import { addDish } from './add-dish.ducks';

class AddDish extends Component {
  state = {
    dish: null
  };

  handleChange = values => {
    
    this.setState({ dish: values });
  };

  handleSubmit = values => {
    let {dish} = this.state;
    dish.chef = this.props.userId;
    this.props
      .addDish(dish)
      .then(({ value }) => {
        message.success('Plat créé avec succès');
        this.props.history.push(`/dish/${value._id}`);
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleSubmitNew = values => {
    let {dish} = this.state;
    dish.chef = this.props.userId;
    this.props
      .addDish(dish)
      .then(({ value }) => {
        message.success('Plat créé avec succès');
        this.setState({ dish: null });
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleCancel = () => {
    this.props.history.push('/dish');
  };

  render() {
    const { loading } = this.props;

    return (
      <div>
        <h1>Création d'une plat</h1>
        <Form
          wrappedComponentRef={form => (this.form = form)}
          value={this.state.dish}
          onChange={this.handleChange}
          onSubmit={this.handleSubmit}
          onCancel={this.handleCancel}
          submitNew={this.handleSubmitNew}
          addNew
          loading={loading}
        />
      </div>
    );
  }
}

AddDish.propTypes = {
  history: PropTypes.object,
  addDish: PropTypes.func,
  loading: PropTypes.bool,
  userId: PropTypes.any
};

const mapStateToProps = ({
  dishes: {
    add: { loading }
  },
  auth
}) => ({
  loading,
  userId: auth.user._id,
});

const mapDispatchToProps = { addDish };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(AddDish);
