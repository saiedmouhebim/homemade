import { combineReducers } from 'redux';

import client from '../../../utils/client';

export const ADD_DISH_PENDING = 'ADD_DISH_PENDING';
export const ADD_DISH_REJECTED = 'ADD_DISH_REJECTED';
export const ADD_DISH_FULFILLED = 'ADD_DISH_FULFILLED';

// Reducers

const loading = (state = false, { type }) => {
  switch (type) {
    case ADD_DISH_PENDING:
      return true;
    case ADD_DISH_REJECTED:
    case ADD_DISH_FULFILLED:
      return false;
    default:
      return state;
  }
};

export default combineReducers({ loading });

// Action Creators

export const addDish = data => {
  return {
    type: 'ADD_DISH',
    payload: client.service('dishes').create(data)
  };
};
