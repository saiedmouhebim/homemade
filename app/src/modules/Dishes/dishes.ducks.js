import { combineReducers } from 'redux';

import client from '../../utils/client';

import dishesList, { REQUEST_DISHS_FULFILLED } from './Dishes/dishes-list.ducks';
import addDish, { ADD_DISH_FULFILLED } from './AddDishes/add-dish.ducks';
import editDish, {
  REQUEST_DISH_FULFILLED,
  UPDATE_DISH_FULFILLED
} from './EditDishes/edit-dishe.ducks';

export const REQUEST_ALL_DISHES_PENDING = 'REQUEST_ALL_DISHES_PENDING';
export const REQUEST_ALL_DISHES_REJECTED = 'REQUEST_ALL_DISHES_REJECTED';
export const REQUEST_ALL_DISHES_FULFILLED = 'REQUEST_ALL_DISHES_FULFILLED';
// Reducers

const ids = (state = [], { type, payload }) => {
  switch (type) {
    case REQUEST_DISHS_FULFILLED:
      return [...new Set(state.concat(payload.data.map(user => user._id)))];
    case REQUEST_ALL_DISHES_FULFILLED:
      return [...new Set(state.concat(payload.map(user => user._id)))];
    case ADD_DISH_FULFILLED:
    case REQUEST_DISH_FULFILLED:
    case UPDATE_DISH_FULFILLED:
      return [payload._id, ...state];
    default:
      return state;
  }
};

const byId = (state = {}, { type, payload }) => {
  switch (type) {
    case REQUEST_DISHS_FULFILLED:
      return payload.data.reduce(
        (res, user) => Object.assign({}, { [user._id]: user }, res),
        state
      );
    case REQUEST_ALL_DISHES_FULFILLED:
      return payload.reduce(
        (res, users) => Object.assign({}, { [users._id]: users }, res),
        state
      );
    case ADD_DISH_FULFILLED:
    case REQUEST_DISH_FULFILLED:
    case UPDATE_DISH_FULFILLED:
      return Object.assign({}, state, { [payload._id]: payload });
    default:
      return state;
  }
};

export default combineReducers({
  ids,
  byId,
  list: dishesList,
  add: addDish,
  edit: editDish
});

export const fetchAllDishes= () => {
  const query = {
    query: { $limit: -1 }
  };

  return {
    type: 'REQUEST_ALL_DISHES',
    payload: client.service('dishes').find(query)
  };
};
