import React from 'react';
import { Form, Row, Col, Input, Select } from 'antd';
import { connect } from 'react-redux';

import { DISHESTYPE, DISHESINGRIDIENT, DISHESTARGET } from '../../../const';
import {
  ResourceForm,
  SearchableSelect,
  ImageUpload
} from '../../../components';

const DEFAULT_Zone = {
  photo: '',
  name: '',
  description: '',
  type: '',
  target: [],
  ingridient: '',
  price: 0,
  quantity: 0,
};

class UserForm extends ResourceForm {
  render() {
    const FormItemLayout = {
      labelCol: { span: 24 }
    };

    return this.renderForm(({ form, mode, readOnly }) => {
      const { getFieldDecorator } = form;

      return (
        <Row gutter={16}>
          <Col xs={{ span: 24 }} md={{ span: 12 }}>
            <Form.Item {...FormItemLayout} label="Photo de l'utilisateur">
              {getFieldDecorator('photo')(<ImageUpload disabled={readOnly} />)}
            </Form.Item>
          </Col>

          <Col xs={{ span: 24 }} md={{ span: 18 }}>

            <Row gutter={16}>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Name">
                  {getFieldDecorator('name', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer le nom du plat'
                      }
                    ]
                  })(<Input disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Description">
                  {getFieldDecorator('description', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer la description'
                      }
                    ]
                  })(<Input.TextArea disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Price">
                  {getFieldDecorator('price')(<Input type="number" disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Quantité">
                  {getFieldDecorator('quantity')(<Input type="number" disabled={readOnly} />)}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Type">
                  {getFieldDecorator('type', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer la civilité'
                      }
                    ]
                  })(
                    <SearchableSelect disabled={readOnly}>
                      {DISHESTYPE.map(item => (
                        <Select.Option key={item.key}>
                          {item.label}
                        </Select.Option>
                      ))}
                    </SearchableSelect>
                  )}
                </Form.Item>
              </Col>

              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Ingridient">
                  {getFieldDecorator('ingridient', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer la civilité'
                      }
                    ]
                  })(
                    <Select
                      mode="tags"
                      disabled={readOnly}>
                      {DISHESINGRIDIENT.map(item => (
                        <Select.Option key={item.key}>
                          {item.label}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col xs={{ span: 24 }} md={{ span: 12 }}>
                <Form.Item {...FormItemLayout} label="Personne Concerné">
                  {getFieldDecorator('target', {
                    rules: [
                      {
                        required: true,
                        message: 'Veuillez entrer la Personne Concerné'
                      }
                    ]
                  })(
                    <SearchableSelect disabled={readOnly}>
                      {DISHESTARGET.map(item => (
                        <Select.Option key={item.key}>
                          {item.label}
                        </Select.Option>
                      ))}
                    </SearchableSelect>
                  )}
                </Form.Item>
              </Col>

            </Row>
          </Col>
        </Row>
      );
    });
  }
}

const WrappedForm = Form.create({
  mapPropsToFields(props) {
    const value = props.value || DEFAULT_Zone;

    return {
      photo: Form.createFormField({ value: value.photo }),
      name: Form.createFormField({ value: value.name }),
      description: Form.createFormField({ value: value.description }),
      type: Form.createFormField({ value: value.type }),
      target: Form.createFormField({ value: value.target }),
      price: Form.createFormField({ value: value.price }),
      quantity: Form.createFormField({ value: value.quantity }),
      ingridient: Form.createFormField({ value: value.ingridient })
    };
  },
  onValuesChange(props, changedValues, allValues) {
    props.onChange && props.onChange(allValues);
  }
})(UserForm);

const mapStateToProps = ({ users: { ids, byId } }) => ({
  users: ids.map(id => byId[id])
});

const withStore = connect(mapStateToProps);

export default withStore(WrappedForm);
