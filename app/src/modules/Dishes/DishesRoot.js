import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import AddDishes from './AddDishes';
import Dishes from './Dishes';
import EditDishes from './EditDishes';

class ZonesRoot extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
  };

  componentDidMount() {
  }

  render() {
    const { match } = this.props;

    return (
      <Switch>
        <Route exact path={`${match.url}/`} component={Dishes} />
        <Route exact path={`${match.url}/new`} component={AddDishes} />
        <Route exact path={`${match.url}/:id`} component={EditDishes} />
      </Switch>
    );
  }
}

const mapDispatchToProps = {
};

const withStore = connect(null, mapDispatchToProps);

export default withStore(ZonesRoot);
