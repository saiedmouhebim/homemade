import { combineReducers } from 'redux';

import client from '../../../utils/client';
import { getSkipFromLimitAndPage } from '../../../utils/helpers';

export const REQUEST_DISHS_PENDING = 'REQUEST_DISHS_PENDING';
export const REQUEST_DISHS_REJECTED = 'REQUEST_DISHS_REJECTED';
export const REQUEST_DISHS_FULFILLED = 'REQUEST_DISHS_FULFILLED';

export const REMOVE_DISH_PENDING = 'REMOVE_DISH_PENDING';
export const REMOVE_DISH_REJECTED = 'REMOVE_DISH_REJECTED';
export const REMOVE_DISH_FULFILLED = 'REMOVE_DISH_FULFILLED';

// Reducers

const loading = (state = true, { type }) => {
  switch (type) {
    case REQUEST_DISHS_PENDING:
    case REMOVE_DISH_PENDING:
      return true;
    case REQUEST_DISHS_REJECTED:
    case REQUEST_DISHS_FULFILLED:
    case REMOVE_DISH_REJECTED:
    case REMOVE_DISH_FULFILLED:
      return false;
    default:
      return state;
  }
};

const ids = (state = [], { type, payload }) => {
  switch (type) {
    case REQUEST_DISHS_FULFILLED:
      return payload.data.map(user => user._id);
    case REMOVE_DISH_FULFILLED:
      return state.filter(id => {
        return id !== payload._id;
      });
    default:
      return state;
  }
};

const total = (state = 0, { type, payload }) => {
  switch (type) {
    case REQUEST_DISHS_FULFILLED:
      return payload.total;
    default:
      return state;
  }
};

const page = (state = 1, { type, payload }) => {
  switch (type) {
    case REQUEST_DISHS_FULFILLED:
      return payload.skip / payload.limit + 1;
    default:
      return state;
  }
};

export default combineReducers({
  loading,
  ids,
  page,
  total
});

// Action Creators

export const fetchDishes = ({
  page = 1,
  limit = 10,
  sort: $sort = { createdAt: -1 },
  filter = {}
}) => {

  const query = {
    query: {
      $limit: limit,
      $skip: getSkipFromLimitAndPage(limit, page),
      $sort,
      ...filter
    }
  };

  return {
    type: 'REQUEST_DISHS',
    payload: client.service('dishes').find(query)
  };
};

export const deleteDish = id => {
  return {
    type: 'REMOVE_DISH',
    payload: client.service('dishes').remove(id)
  };
};
