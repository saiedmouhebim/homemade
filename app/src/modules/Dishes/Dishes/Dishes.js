import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Avatar, Row, Col, Button } from 'antd';

import { convertTableMeta } from '../../../utils/helpers';
import { Actions, DataTable } from '../../../components';

import { fetchDishes, deleteDish } from './dishes-list.ducks';

class Dishes extends Component {
  componentDidMount() {
    this.getDishesData({ page: 1 });
  }

  getDishesData = ({ page, limit = 10, filter, sort }) => {
    filter = { ...filter,
      chef: this.props.userId};
    this.props.fetchDishes({ page, limit, filter, sort });
  };

  onTableChange = (pagination, filters, sorter) => {
    filters = { ...filters,
      chef: this.props.userId};
    this.getDishesData(
      convertTableMeta(pagination, filters, sorter, {
        name: 'fullName',
        type: 'array'
      })
    );
  };

  render() {
    const { loading, page, total, dishes } = this.props;

    const columns = [
      {
        title: 'Photo',
        key: 'photo',
        render: (text, record) => <Avatar src={record.photo} icon="user" />
      },
      {
        title: 'name',
        key: 'name',
        render: (text, record) => <p>{record.name}</p>
      },
      {
        title: 'price',
        key: 'price',
        render: (text, record) => <p>{record.price}</p>
      },
      {
        title: 'type',
        key: 'type',
        render: (text, record) => <p>{record.type}</p>
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <Actions
            showUpdate
            showDelete
            selectUrl={`/dish/${record._id}`}
            onDelete={() => {
              this.props.deleteDish(record._id);
            }}
          />
        )
      }
    ];

    return (
      <div>
        <Row>
          <Col>
            <h1>Gestion des Plats</h1>
            <DataTable
              loading={loading}
              dataSource={dishes}
              columns={columns}
              rowKey="_id"
              pagination={{
                current: page,
                pageSize: 10,
                total
              }}
              onChange={this.onTableChange}
            />
          </Col>
        </Row>
        <Row style={{ marginTop: '1rem', marginBottom: '1rem' }}>
          <Col style={{ textAlign: 'right' }}>
            <Link to="/dish/new">
              <Button type="primary" icon="plus-square-o">
                Ajout d'une plat
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
    );
  }
}

Dishes.propTypes = {
  deleteDish: PropTypes.func,
  fetchDishes: PropTypes.func.isRequired,
  userId: PropTypes.any,
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  dishes: PropTypes.arrayOf(PropTypes.object).isRequired
};

const mapStateToProps = ({
  dishes: {
    byId,
    list: { ids, loading, total, page }
  },
  auth,
}) => ({
  auth,
  dishes: ids.map(id => byId[id]),
  page,
  loading,
  total,
  userType: auth.user.type,
  userId: auth.user._id,
});

const mapDispatchToProps = { fetchDishes, deleteDish };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(Dishes);
