import { combineReducers } from 'redux';

import client from '../../../utils/client';

export const REQUEST_DISH_PENDING = 'REQUEST_DISHS_PENDING';
export const REQUEST_DISH_REJECTED = 'REQUEST_DISHS_REJECTED';
export const REQUEST_DISH_FULFILLED = 'REQUEST_DISHS_FULFILLED';

export const UPDATE_DISH_PENDING = 'UPDATE_DISH_PENDING';
export const UPDATE_DISH_REJECTED = 'UPDATE_DISH_REJECTED';
export const UPDATE_DISH_FULFILLED = 'UPDATE_DISH_FULFILLED';

// Reducers

const loading = (state = false, { type }) => {
  switch (type) {
    case REQUEST_DISH_PENDING:
    case UPDATE_DISH_PENDING:
      return true;
    case REQUEST_DISH_REJECTED:
    case UPDATE_DISH_REJECTED:
    case REQUEST_DISH_FULFILLED:
    case UPDATE_DISH_FULFILLED:
      return false;
    default:
      return state;
  }
};

export default combineReducers({ loading });

// Action Creators

export const fetchDish = id => {
  return (dispatch, getState) => {
    const user = getState().dishes.byId[id];

    return dispatch({
      type: 'REQUEST_DISH',
      payload: user ? Promise.resolve(user) : client.service('dishes').get(id)
    });
  };
};

export const updateDish = (id, data) => {
  return {
    type: 'UPDATE_DISH',
    payload: client.service('dishes').update(id, data)
  };
};
