import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Spin, message } from 'antd';

import Form from '../Form';

import { fetchDish, updateDish } from './edit-dishe.ducks';

class EditDish extends Component {
  state = { id: null, dish: null };

  componentDidMount = () => {
    const {
      match: {
        params: { id }
      }
    } = this.props;

    this.props.fetchDish(id).then(({ value: dish }) => {
      this.form.setPristine();
      this.setState({ id, dish });
    });
  };

  handleChange = values => {
    this.setState({ dish: values });
  };

  handleSubmit = values => {
    let { id, dish } = this.state;
    dish.chef = this.props.userId;
    this.props
      .updateDish(id, dish)
      .then(() => {
        message.success('Plat modifié avec succès');
        this.form.setReadOnly();
        this.form.setPristine();
        this.setState({ dish: Object.assign({}, dish) });
      })
      .catch(err => {
        this.form.setServerErrors(values, err);
      });
  };

  handleCancel = () => {
    const { id } = this.state;
    const {
      dishes: { byId }
    } = this.props;

    this.form.setReadOnly();
    this.form.setPristine();

    this.setState({ dish: byId[id] });
  };

  render() {
    const { dish } = this.state;
    const { loading } = this.props;

    return (
      <div>
        <h1>Détails/Modification d'une plat</h1>
        <Spin spinning={ loading}>
          <Form
            wrappedComponentRef={form => (this.form = form)}
            mode="edit"
            value={dish}
            onChange={this.handleChange}
            onSubmit={this.handleSubmit}
            onCancel={this.handleCancel}
            loading={loading}
          />
        </Spin>
      </div>
    );
  }
}

EditDish.propTypes = {
  dishes: PropTypes.object,
  loading: PropTypes.bool,
  match: PropTypes.object.isRequired,
  userId: PropTypes.any,
  fetchDish: PropTypes.func,
  updateDish: PropTypes.func
};

const mapStateToProps = ({ dishes ,auth}) => ({
  dishes,
  loading: dishes.edit.loading,
  userId: auth.user._id,

});

const mapDispatchToProps = { fetchDish, updateDish };

const withStore = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withStore(EditDish);
