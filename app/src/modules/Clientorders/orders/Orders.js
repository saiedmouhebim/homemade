import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col, Table } from 'antd';

import { convertTableMeta } from '../../../utils/helpers';
import { DataTable } from '../../../components';
import client from '../../../utils/client';

import { fetchOrders } from './orders-list.ducks';

class Users extends Component {

  state = {
    orders: [],
  };

  componentDidMount() {
    this.getOrdersData({ page: 1 });
  }

  getOrdersData = ({ page, limit = 10, filter, sort }) => {
    filter = {
      ...filter,
      chef: this.props.userId
    };
    this.props.fetchOrders({ page, limit, filter, sort }).then(({value: {data : orders}}) => {
      this.setState({orders});
    })  ;
  };

  onTableChange = (pagination, filters, sorter) => {
    this.getOrdersData(
      convertTableMeta(pagination, filters, sorter, {
        name: 'fullName',
        type: 'array'
      })
    );
  };

  updateItem= async (record) => {
    let {orders} = this.state;
    const index = orders.findIndex(key =>key._id === record._id);
    orders[index].valid = true;
    client.service('orders').update(orders[index]._id, orders[index]).then(()=>{
      this.setState({orders});
    });
  }

  render() {
    const { loading, page, total } = this.props;
    const {orders} = this.state;
    const expandedRowRender = (column) => {
      const columns = [
        {
          title: 'Plat',
          key: 'ref',
          render: (text, record) => <div  >{record.dish.name}</div>
        },
        {
          title: 'Prix',
          key: 'price',
          render: (text, record) => <div  >{record.dish.price}</div>
        },
        {
          title: 'Quantity',
          key: 'quantity',
          render: (text, record) => <div  >{record.quantity}</div>
        },
        {
          title: 'priceTTC',
          key: 'PriceTTc',
          render: (text, record) => <div  >{record.totalPriceInclTax}</div>
        },
      ];

      return <Table rowKey={'_id'} columns={columns} dataSource={column} pagination={false} />;
    };

    const columns = [
      {
        title: 'Ref',
        key: 'ref',
        render: (text, record) => <div>{record.ref}</div>
      },
      {
        title: 'Prix TTC',
        key: 'totalPriceInclTax',
        render: (text, record) => <div  >{record.totalPriceInclTax} EUR</div>
      },
      {
        title: 'Validé ?',
        key: 'valid',
        render: (text, record) => <div style={{color : record.valid ? 'green': 'red'}} >{record.valid ? 'Validée' : 'Non validée'}</div>
      },
      {
        title: 'Payéé ?',
        key: 'checked',
        render: (text, record) => <div  style={{color : record.checked ? 'green': 'red'}}>{record.checked ? 'Payée' : 'Non payée'}</div>
      },
      { title: 'Action', key: 'operation', render: (text, record) => <a onClick={()=>this.updateItem(record)}>{!record.valid ? 'Valider' : ''}</a> },
    ];

    return (
      <div>
        <Row>
          <Col>
            <h1>Gestion des Commandes</h1>
            <DataTable
              loading={loading}
              dataSource={orders}
              columns={columns}
              rowKey="_id"
              expandedRowRender={record => expandedRowRender(record.items)}
              pagination={{
                current: page,
                pageSize: 10,
                total
              }}
              onChange={this.onTableChange}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

Users.propTypes = {
  fetchOrders: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  userId: PropTypes.string.isRequired,
  total: PropTypes.number.isRequired,
};

const mapStateToProps = ({
  corders: {
    byId,
    list: { ids, loading, total, page }
  },
  auth,
}) => ({
  auth,
  corders: ids.map(id => byId[id]),
  page,
  loading,
  total,
  userType: auth.user.type,
  userId: auth.user._id,
});

const mapDispatchToProps = { fetchOrders };

const withStore = connect(mapStateToProps, mapDispatchToProps);

export default withStore(Users);
