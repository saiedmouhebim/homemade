/**
 * Creates a hash indexed by key from an array of objects.
 *
 * @param {Array} options array of objects
 *
 * @returns {Object} hash of objects indexed by keys
 */
function getByKey(options) {
  return options.reduce((acc, options) => {
    acc[options.key] = options;
    return acc;
  }, {});
}

export const NOTIFICATION_TARGET = [
  {
    key: 'all',
    label: 'Clients & Utilisateur & Fournisseurs & Groupe clients'
  },
  { key: 'clients', label: 'Clients' },
  { key: 'users', label: 'Utilisateur' },
  { key: 'suppliers', label: 'Fournisseurs' },
  { key: 'clientGroupe', label: 'Groupe clients' }
];

export const NOTIFICATION_CASES = [
  {
    key: 'all',
    label: 'Aucune condition',
    target: ['all', 'clients', 'users', 'suppliers', 'clientGroupe']
  },
  {
    key: 'filter',
    label: 'Filtrage',
    target: ['clients', 'users', 'suppliers', 'clientGroupe']
  }
];

export const GENDERS = [
  { key: 'm', label: 'Monsieur' },
  { key: 'f', label: 'Madame' }
];

export const DISHESTYPE = [
  { key: 'Entree', label: 'Entré' },
  { key: 'Desert', label: 'Dessert' },
  { key: 'principal', label: 'principal' }
];
export const DISHESTARGET = [
  { key: 'None', label: 'Rien' },
  { key: 'Pescetarian', label: 'Pescétarien' },
  { key: 'Vegetarian', label: 'Végétarienne' },
  { key: 'Vegan', label: 'Végétalienne' },
  { key: 'Keto', label: 'Keto' }
];

export const DISHESINGRIDIENT = [
  { key: 'tree nuts', label: 'noix' },
  { key: 'shellfish', label: 'fruits de mer' },
  { key: 'soy', label: 'soja' },
  { key: 'Egg', label: 'Oeuf' },
  { key: 'Gelatin', label: 'Gélatine' },
  { key: 'Dairy', label: 'Laitier' },
  { key: 'Penauts', label: 'Cacahuètes' },
  { key: 'fish', label: 'poisson' },
  { key: 'wheat', label: 'du blé' },
  { key: 'corn', label: 'maïs' },
  { key: 'seeds', label: 'graines' },
  { key: 'gluten', label: 'gluten' },
];

export const GENDERS_BY_KEY = getByKey(GENDERS);

export const USER_TYPES = [
  { key: 'admin', label: 'Administrateur' },
  { key: 'cfo', label: 'CHEF CUISINNER' },
  { key: 'Livreur', label: 'LIVREUR' },
  { key: 'Client', label: 'Client' },
];

export const DOCUMENT_TYPES = [
  { key: 0, label: 'Devis' },
  { key: 1, label: 'Bon de commande' },
  { key: 2, label: 'Préparation de livraison' },
  { key: 3, label: 'Bon de livraison' },
  { key: 4, label: 'Bon de retour' },
  { key: 5, label: 'Bon d’avoir' },
  { key: 6, label: 'Facture' },
  { key: 7, label: 'Facture comptabilisée' }
];

export const USER_TYPES_BY_KEY = getByKey(USER_TYPES);

export const TICKET_TYPES = [
  { key: 'ticket', label: 'Ticket' },
  { key: 'card', label: 'Carte à unités' },
  { key: 'flat', label: 'Forfait' }
];

export const TICKET_TYPES_BY_KEY = getByKey(TICKET_TYPES);

export const SUBSCRIPTION_TYPES = [
  { key: 'fixedDate', label: 'à date fixe' },
  { key: 'regular', label: 'regulier' }
];

export const SUBSCRIPTION_TYPES_REGULAR = [
  { key: 'month', label: 'Mensuel' },
  { key: 'quarter', label: 'Trimestriel' },
  { key: 'semester', label: 'Semestriel' },
  { key: 'year', label: 'Annuel' }
];

export const SUBSCRIPTION_TYPES_BY_KEY = getByKey(SUBSCRIPTION_TYPES);

export const PERSON_STATUS = [
  { key: 'single', label: 'Célibataire' },
  { key: 'married', label: 'Marié' }
];

export const PERSON_STATUS_BY_KEY = getByKey(PERSON_STATUS);

export const ID_TYPES = [
  { key: 'id', label: 'C.I.N.' },
  { key: 'studentCard', label: "Carte d'étudiant" },
  { key: 'passport', label: 'Passeport' },
  { key: 'residencePermit', label: 'Carte de séjour' }
];

export const ID_TYPES_BY_KEY = getByKey(ID_TYPES);

export const TREE_PAGES = [
  {
    title: 'Paramétrage',
    key: 'parametre',
    children: [
      {
        title: 'Formulaire',
        key: 'formulaire',
        children: [
          { title: 'Lecture', key: 'ReadForm' },
          { title: 'Suppression', key: 'deleteForm' },
          { title: 'Création/Modification', key: 'create-updateForm' }
        ]
      },
      {
        title: 'Rôles & privilèges',
        key: 'roles',
        children: [
          { title: 'Lecture', key: 'ReadRoles' },
          { title: 'Suppression', key: 'deleteRoles' },
          { title: 'Création/Modification', key: 'create-updateRoles' }
        ]
      },
      {
        title: 'Réglages mobilité',
        key: 'mobilité',
        children: [
          { title: 'Lecture', key: 'ReadMobilité' },
          { title: 'Suppression', key: 'deleteMobilité' },
          { title: 'Création/Modification', key: 'create-updateMobilité' }
        ]
      },
      {
        title: 'Zonage',
        key: 'zones',
        children: [
          { title: 'Lecture', key: 'ReadZones' },
          { title: 'Suppression', key: 'deleteZones' },
          { title: 'Création/Modification', key: 'create-updateZones' }
        ]
      },
      {
        title: 'TVA',
        key: 'vat',
        children: [
          { title: 'Lecture', key: 'ReadVat' },
          { title: 'Suppression', key: 'deleteVat' },
          {
            title: 'Création/Modification',
            key: 'create-updateVat'
          }
        ]
      },
      {
        title: 'medias',
        key: 'media',
        children: [
          { title: 'Lecture', key: 'ReadMedia' },
          { title: 'Suppression', key: 'deleteMedia' },
          {
            title: 'Création/Modification',
            key: 'create-updateMedia'
          }
        ]
      },
      {
        title: 'Modes de livraison',
        key: 'deliveries',
        children: [
          { title: 'Lecture', key: 'ReadDeliveries' },
          { title: 'Suppression', key: 'deleteDeliveries' },
          {
            title: 'Création/Modification',
            key: 'create-updateDeliveries'
          }
        ]
      },
      {
        title: 'Droits de consommation',
        key: 'consumptions',
        children: [
          { title: 'Lecture', key: 'ReadConsumptions' },
          { title: 'Suppression', key: 'deleteConsumptions' },
          {
            title: 'Création/Modification',
            key: 'create-updateConsumptions'
          }
        ]
      },
      {
        title: 'Types réclamations',
        key: 'claimTypes',
        children: [
          { title: 'Lecture', key: 'ReadClaimTypes' },
          { title: 'Suppression', key: 'deleteClaimTypes' },
          {
            title: 'Création/Modification',
            key: 'create-updateClaimTypes'
          }
        ]
      },
      {
        title: 'Modes de règlement',
        key: 'payments',
        children: [
          { title: 'Lecture', key: 'ReadPayments' },
          { title: 'Suppression', key: 'deletePayments' },
          {
            title: 'Création/Modification',
            key: 'create-updatePayments'
          }
        ]
      },
      {
        title: 'Synchronisation',
        key: 'synchronisation',
        children: [
          { title: 'Lecture', key: 'ReadSynchronisation' },
          { title: 'Suppression', key: 'deleteSynchronisation' },
          {
            title: 'Création/Modification',
            key: 'create-updateSynchronisation'
          }
        ]
      },
      {
        title: 'Unités',
        key: 'units',
        children: [
          { title: 'Lecture', key: 'ReadUnits' },
          { title: 'Suppression', key: 'deleteUnits' },
          {
            title: 'Création/Modification',
            key: 'create-updateUnits'
          }
        ]
      }
    ]
  },
  {
    title: 'Utilisateur',
    key: 'users',
    children: [
      { title: 'Lecture', key: 'ReadU' },
      { title: 'Suppression', key: 'deleteU' },
      { title: 'Création/Modification', key: 'create-updateU' }
    ]
  },

  {
    title: 'Catalogue',
    key: 'catalogue',
    children: [
      {
        title: 'Base articles',
        key: 'products',
        children: [
          { title: 'Lecture', key: 'ReadProducts' },
          { title: 'Suppression', key: 'deleteProducts' },
          { title: 'Création/Modification', key: 'create-updateProducts' }
        ]
      },
      {
        title: 'Familles',
        key: 'family',
        children: [
          { title: 'Lecture', key: 'Readfamily' },
          { title: 'Suppression', key: 'deletefamily' },
          { title: 'Création/Modification', key: 'create-updatefamily' }
        ]
      },
      {
        title: 'Couleurs',
        key: 'colors',
        children: [
          { title: 'Lecture', key: 'ReadColor' },
          { title: 'Suppression', key: 'deleteColor' },
          { title: 'Création/Modification', key: 'create-updateColor' }
        ]
      },
      {
        title: 'Marques',
        key: 'brands',
        children: [
          { title: 'Lecture', key: 'ReadBrand' },
          { title: 'Suppression', key: 'deleteBrand' },
          { title: 'Création/Modification', key: 'create-updateBrand' }
        ]
      },
      {
        title: 'Catégories',
        key: 'categories',
        children: [
          { title: 'Lecture', key: 'ReadCategories' },
          { title: 'Suppression', key: 'deleteCategories' },
          { title: 'Création/Modification', key: 'create-updateCategories' }
        ]
      },
      {
        title: 'Médias',
        key: 'medias',
        children: [
          { title: 'Lecture', key: 'ReadMedias' },
          { title: 'Suppression', key: 'deleteMedias' },
          { title: 'Création/Modification', key: 'create-updateMedias' }
        ]
      }
    ]
  },

  {
    title: 'Clients',
    key: 'allClients',
    children: [
      {
        title: 'Base groupes clients',
        key: 'clientsGroupe',
        children: [
          { title: 'Lecture', key: 'ReadClientsGroupe' },
          { title: 'Suppression', key: 'deleteClientsGroupe' },
          { title: 'Création/Modification', key: 'create-updateClientsGroupe' }
        ]
      },
      {
        title: 'Base clients',
        key: 'clients',
        children: [
          { title: 'Lecture', key: 'ReadClients' },
          { title: 'Suppression', key: 'deleteClients' },
          { title: 'Création/Modification', key: 'create-updateClients' }
        ]
      },
      {
        title: 'Types de clients',
        key: 'types',
        children: [
          { title: 'Lecture', key: 'ReadType' },
          { title: 'Suppression', key: 'deleteType' },
          {
            title: 'Création/Modification',
            key: 'create-updateType'
          }
        ]
      },
      {
        title: 'Réclamations',
        key: 'claims',
        children: [
          { title: 'Lecture', key: 'ReadClaims' },
          { title: 'Suppression', key: 'deleteClaims' },
          {
            title: 'Création/Modification',
            key: 'create-updateClaims'
          }
        ]
      }
    ]
  },

  {
    title: 'Fournisseurs',
    key: 'fournisseurs',
    children: [
      { title: 'Lecture', key: 'ReadFournisseurs' },
      { title: 'Suppression', key: 'deleteFournisseurs' },
      {
        title: 'Création/Modification',
        key: 'create-updateFournisseurs'
      }
    ]
  },

  {
    title: 'Stockage et logistique',
    key: 'storage',
    children: [
      {
        title: 'Magasins',
        key: 'warehouses',
        children: [
          { title: 'Lecture', key: 'ReadWarehouses' },
          { title: 'Suppression', key: 'deleteWarehouses' },
          {
            title: 'Création/Modification',
            key: 'create-updateWarehouses'
          }
        ]
      },
      {
        title: 'Inventaires',
        key: 'inventories',
        children: [
          { title: 'Lecture', key: 'ReadInventories' },
          { title: 'Suppression', key: 'deleteInventories' },
          {
            title: 'Création/Modification',
            key: 'create-updateInventories'
          }
        ]
      },
      {
        title: 'Mouvements de stocks',
        key: 'stocks',
        children: [
          { title: 'Lecture', key: 'ReadStocks' },
          { title: 'Suppression', key: 'deleteStocks' },
          {
            title: 'Création/Modification',
            key: 'create-updateStocks'
          }
        ]
      },
      {
        title: 'Commandes à préparer',
        key: 'validOrders',
        children: [
          { title: 'Lecture', key: 'ReadValidOrders' },
          { title: 'Suppression', key: 'deleteValidOrders' },
          {
            title: 'Création/Modification',
            key: 'create-updateValidOrders'
          }
        ]
      }
    ]
  },

  {
    title: 'Ventes',
    key: 'orders',
    children: [
      {
        title: 'Commandes',
        key: 'ordersList',
        children: [
          { title: 'Lecture', key: 'ReadOrders' },
          { title: 'Suppression', key: 'deleteOrders' },
          {
            title: 'Création/Modification',
            key: 'create-updateOrders'
          }
        ]
      },
      {
        title: 'Nouvelle Commande',
        key: 'newOrder',
        children: [
          { title: 'Lecture', key: 'ReadNewOrders' },
          { title: 'Suppression', key: 'deleteNewOrders' },
          {
            title: 'Création/Modification',
            key: 'create-updateNewOrders'
          }
        ]
      },
      {
        title: 'Documents SAGE',
        key: 'bills',
        children: [
          { title: 'Lecture', key: 'ReadBills' },
          { title: 'Suppression', key: 'deleteBills' },
          {
            title: 'Création/Modification',
            key: 'create-updateBills'
          }
        ]
      },
      {
        title: 'Factures impayées',
        key: 'sageOrders',
        children: [
          { title: 'Lecture', key: 'ReadSageOrders' },
          { title: 'Suppression', key: 'deleteSageOrders' },
          {
            title: 'Création/Modification',
            key: 'create-updateSageOrders'
          }
        ]
      },
      {
        title: 'Livraisons',
        key: 'deliveryForms',
        children: [
          { title: 'Lecture', key: 'ReadDeliveryForms' },
          { title: 'Suppression', key: 'deleteDeliveryForms' },
          {
            title: 'Création/Modification',
            key: 'create-updateDeliveryForms'
          }
        ]
      }
    ]
  },

  {
    title: 'Force de vente',
    key: 'sailes',
    children: [
      {
        title: 'Objectifs de vente',
        key: 'sailesObjective',
        children: [
          { title: 'Lecture', key: 'ReadSailesObjective' },
          { title: 'Suppression', key: 'deleteSailesObjective' },
          {
            title: 'Création/Modification',
            key: 'create-updateSailesObjective'
          }
        ]
      },
      {
        title: 'Campagnes de promotion',
        key: 'promotions',
        children: [
          { title: 'Lecture', key: 'ReadPromotions' },
          { title: 'Suppression', key: 'deletePromotions' },
          { title: 'Création/Modification', key: 'create-updatePromotions' }
        ]
      },
      {
        title: 'Campagnes de fidelisation',
        key: 'fidelity',
        children: [
          { title: 'Lecture', key: 'ReadFidelity' },
          { title: 'Suppression', key: 'deleteFidelity' },
          { title: 'Création/Modification', key: 'create-updateFidelity' }
        ]
      },
      {
        title: 'Plans de tournées',
        key: 'tour',
        children: [
          { title: 'Lecture', key: 'ReadTour' },
          { title: 'Suppression', key: 'deleteTour' },
          { title: 'Création/Modification', key: 'create-updateTour' }
        ]
      },
      {
        title: 'Véhicules',
        key: 'trucks',
        children: [
          { title: 'Lecture', key: 'ReadTrucks' },
          { title: 'Suppression', key: 'deleteTrucks' },
          {
            title: 'Création/Modification',
            key: 'create-updateTrucks'
          }
        ]
      }
    ]
  },

  {
    title: 'Merchandizing',
    key: 'merchandizing',
    children: [
      {
        title: 'Survey',
        key: 'survey',
        children: [
          { title: 'Lecture', key: 'ReadSurvey' },
          { title: 'Suppression', key: 'deleteSurvey' },
          {
            title: 'Création/Modification',
            key: 'create-updateSurvey'
          }
        ]
      }
    ]
  },

  {
    title: 'KPIs',
    key: 'kpis',
    children: [
      {
        title: 'Ventes Globales',
        key: 'globalSailes',
        children: [{ title: 'Lecture', key: 'ReadGlobalSailes' }]
      },
      {
        title: 'Agrégats',
        key: 'aggregate',
        children: [{ title: 'Lecture', key: 'ReadAggregate' }]
      },
      {
        title: 'Fidelisation',
        key: 'fidelityStats',
        children: [{ title: 'Lecture', key: 'ReadFidelityStats' }]
      }
    ]
  },

  {
    title: 'Digital marketing',
    key: 'marketing',
    children: [
      {
        title: 'Campagnes',
        key: 'notifications',
        children: [
          { title: 'Lecture', key: 'ReadNotifications' },
          { title: 'Suppression', key: 'deleteNotifications' },
          { title: 'Création/Modification', key: 'create-updateNotifications' }
        ]
      },
      {
        title: 'Time line',
        key: 'timeLine',
        children: [{ title: 'Lecture', key: 'ReadTimeLine' }]
      }
    ]
  },
  {
    title: 'Actualités',
    key: 'news',
    children: [
      { title: 'Lecture', key: 'ReadNews' },
      { title: 'Suppression', key: 'deleteNews' },
      {
        title: 'Création/Modification',
        key: 'create-updateNews'
      }
    ]
  }
];

export const MOBILE_MENU = [
  { key: 'tours', label: 'Plans de tournées' },
  { key: 'orders', label: 'Commandes' },
  { key: 'deliveries', label: 'Livraisons' },
  { key: 'payments', label: 'Règlements' },
  { key: 'clients', label: 'Clients' },
  { key: 'products', label: 'Catalogue' },
  { key: 'claims', label: 'Réclamations' },
  { key: 'visits', label: 'Rapports de visite' }
];

export const PERIOD = [
  { key: 'daily', label: 'Hebdomadaire' },
  { key: 'monthly', label: 'Mensuel ' },
  { key: 'trimestre', label: 'Trimestrie' }
];

export const ACCESS_PAGE = [
  { key: 'fields', label: 'Formulaires' },
  { key: 'orders', label: 'Commandes ' },
  { key: 'newOrder', label: 'Nouvelle Commande' },
  { key: 'products', label: 'Base articles' },
  { key: 'inventories', label: 'Inventaires' },
  { key: 'aggregates', label: 'Agrégats' }
];

export const STOCK_INFORMATION = [
  { key: 'threeColors', label: 'Affichage signalétique à couleurs' },
  { key: 'real', label: 'Affichage stock réel' },
  { key: 'twoColors', label: 'Les deux en même temps' }
];

export const PAGES = [
  {
    key: 'users',
    to: '/users',
    icon: 'user',
    target: ['admin', 'cfo'],
    label: 'Utilisateurs'
  },
  {
    key: 'zones',
    to: '/zones',
    icon: 'map',
    target: ['admin'],
    label: 'Zones'
  },
  {
    key: 'dish',
    to: '/dish',
    icon: 'map',
    target: ['cfo'],
    label: 'Plat'
  },
  {
    key: 'menu',
    to: '/menu',
    icon: 'map',
    target: ['cfo'],
    label: 'Menu'
  },
  {
    key: 'aorders',
    to: '/Aorders',
    icon: 'shop',
    target: ['admin'],
    label: 'Commande'
  },
  {
    key: 'corders',
    to: '/Corders',
    icon: 'shop',
    target: ['cfo'],
    label: 'Commande'
  },
];

export const PAGES_BY_KEY = getByKey(PAGES);

export const DEFAULT_PAGE = {
  centralCashier: PAGES_BY_KEY['users'],
  financeManager: PAGES_BY_KEY['users'],
  siteManager: PAGES_BY_KEY['users'],
  admin: PAGES_BY_KEY['users']
};

export const state = [
  { id: '1', code: '1', nom: 'Adrar' },
  { id: '2', code: '2', nom: 'Chlef' },
  { id: '3', code: '3', nom: 'Laghouat' },
  { id: '4', code: '4', nom: 'Oum El Bouaghi' },
  { id: '5', code: '5', nom: 'Batna' },
  { id: '6', code: '6', nom: 'B\u00e9ja\u00efa' },
  { id: '7', code: '7', nom: 'Biskra' },
  { id: '8', code: '8', nom: 'B\u00e9char' },
  { id: '9', code: '9', nom: 'Blida' },
  { id: '10', code: '10', nom: 'Bouira' },
  { id: '11', code: '11', nom: 'Tamanrasset' },
  { id: '12', code: '12', nom: 'T\u00e9bessa' },
  { id: '13', code: '13', nom: 'Tlemcen' },
  { id: '14', code: '14', nom: 'Tiaret' },
  {
    id: '15',
    code: '15',
    nom: 'Tizi Ouzou'
  },
  { id: '16', code: '16', nom: 'Alger' },
  { id: '17', code: '17', nom: 'Djelfa' },
  {
    id: '18',
    code: '18',
    nom: 'Jijel'
  },
  { id: '19', code: '19', nom: 'S\u00e9tif' },
  { id: '20', code: '20', nom: 'Sa\u00efda' },
  {
    id: '21',
    code: '21',
    nom: 'Skikda'
  },
  { id: '22', code: '22', nom: 'Sidi Bel Abb\u00e8s' },
  {
    id: '23',
    code: '23',
    nom: 'Annaba'
  },
  { id: '24', code: '24', nom: 'Guelma' },
  { id: '25', code: '25', nom: 'Constantine' },
  {
    id: '26',
    code: '26',
    nom: 'M\u00e9d\u00e9a'
  },
  { id: '27', code: '27', nom: 'Mostaganem' },
  {
    id: '29',
    code: '29',
    nom: 'Mascara'
  },
  { id: '30', code: '30', nom: 'Ouargla' },
  { id: '31', code: '31', nom: 'Oran' },
  {
    id: '32',
    code: '32',
    nom: 'El Bayadh'
  },
  { id: '33', code: '33', nom: 'Illizi' },
  {
    id: '34',
    code: '34',
    nom: 'Bordj Bou Arreridj'
  },
  { id: '35', code: '35', nom: 'Boumerd\u00e8s' },
  {
    id: '36',
    code: '36',
    nom: 'El Tarf'
  },
  { id: '37', code: '37', nom: 'Tindouf' },
  { id: '38', code: '38', nom: 'Tissemsilt' },
  {
    id: '39',
    code: '39',
    nom: 'El Oued'
  },
  { id: '40', code: '40', nom: 'Khenchela' },
  { id: '41', code: '41', nom: 'Souk Ahras' },
  {
    id: '42',
    code: '42',
    nom: 'Tipaza'
  },
  { id: '43', code: '43', nom: 'Mila' },
  { id: '44', code: '44', nom: 'A\u00efn Defla' },
  {
    id: '45',
    code: '45',
    nom: 'Na\u00e2ma'
  },
  { id: '46', code: '46', nom: 'A\u00efn T\u00e9mouchent' },
  {
    id: '47',
    code: '47',
    nom: 'Gharda\u00efa'
  },
  { id: '48', code: '48', nom: 'Relizane' }
];

export const CLIENTGROUPEFIELDS = [
  {
    key: 'logo',
    label: 'Logo',
    showInNotifications: false,
    required: true,
    showInList: true,
    type: 'file',
    listValues: [],
    possibleValue: ['file'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'ERP',
    label: 'Référence',
    required: true,
    showInList: false,
    showInNotifications: false,
    type: 'string',
    possibleValue: ['string'],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'name',
    label: 'Nom',
    required: true,
    showInList: true,
    type: 'string',
    showInNotifications: true,
    possibleValue: ['string'],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'adress',
    label: 'Adresse',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'phone',
    label: 'Téléphone',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'email',
    label: 'Email',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'email',
    listValues: [],
    possibleValue: ['email'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'geospaciallocation',
    label: 'Localisation',
    required: false,
    showInList: false,
    type: 'map',
    listValues: [],
    possibleValue: ['map'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'webSite',
    label: 'Site web',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    showInNotifications: true,
    possibleValue: ['url'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'state',
    label: 'Gouvernorat/Ville',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'cascader',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres1',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres2',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres3',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres4',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'montant',
      'inputNumber',
      'checkBox'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres5',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  }
];

export const CLIENTGROUPEFIELDS_BY_KEY = getByKey(CLIENTGROUPEFIELDS);

export const CLIENTFIELDS = [
  {
    key: 'photo',
    label: 'Photo',
    showInNotifications: false,
    required: true,
    showInList: true,
    type: 'file',
    listValues: [],
    possibleValue: ['file'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'ERP',
    label: 'Référence',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'name',
    label: 'Nom',
    required: true,
    showInList: true,
    type: 'string',
    showInNotifications: true,
    possibleValue: ['string'],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'adress',
    label: 'Adresse',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    showInNotifications: true,
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'postalCode',
    label: 'Code Postal',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    showInNotifications: true,
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'village',
    label: 'Ville',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    showInNotifications: true,
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'phone',
    label: 'Téléphone',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'string',
    possibleValue: ['string'],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'zone',
    label: 'Zone',
    required: true,
    showInNotifications: false,
    showInList: true,
    type: 'list',
    possibleValue: ['list'],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'email',
    label: 'Email',
    required: false,
    showInNotifications: true,
    showInList: false,
    type: 'email',
    listValues: [],
    possibleValue: ['email'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'lng',
    label: 'Longitude',
    required: false,
    showInList: false,
    type: 'geometry',
    listValues: [],
    defaultValue: '',
    possibleValue: ['geometry'],
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'lt',
    label: 'Latitude',
    required: false,
    showInList: false,
    type: 'geometry',
    listValues: [],
    defaultValue: '',
    possibleValue: ['geometry'],
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'webSite',
    label: 'Site web',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['url'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'responsible',
    label: 'Premier responsable',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'encours',
    label: 'Encours autorisé',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string', 'montant'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'clientType',
    label: 'Type client',
    required: false,
    showInList: false,
    type: 'list',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['list'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'visitDay',
    label: 'Jour de visite',
    required: false,
    showInNotifications: true,
    showInList: false,
    type: 'list',
    listValues: [
      'Pas de préférence',
      'Lundi',
      'Mardi',
      'Mercredi',
      'Jeudi',
      'Vendredi',
      'Samedi',
      'Dimanche'
    ],
    possibleValue: ['list'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'VisitsPriority',
    label: 'Priorité de visite',
    required: false,
    showInNotifications: true,
    showInList: false,
    type: 'list',
    listValues: [],
    possibleValue: ['list'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'contactPerson',
    label: 'Interlocuteur',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },

  {
    key: 'newsletter',
    label: 'Newsletter',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['toggle'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },

  {
    key: 'description',
    label: 'Description',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'vatCode',
    label: 'Code TVA',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'clientGroupe',
    label: 'Groupe client',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['clientGroupe'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },

  {
    key: 'qres1',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres2',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'montant',
      'inputNumber',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres3',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres4',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres5',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  }
];

export const PRODUCT = [
  {
    key: 'photo',
    label: 'Photo',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['file'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'ref_ERP',
    label: 'Référence ERP',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'internalRef',
    label: 'Référence interne',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'name',
    label: 'Désignation',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'price',
    label: 'Prix',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['montant'],
    defaultValue: false,
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'barcode',
    label: 'Code à barres',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'description',
    label: 'Description',
    required: true,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['Texte riche'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'instock',
    label: 'Qtée en stock',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['inputNumber', 'string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'packing',
    label: 'Unité logistique',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['list', 'string', 'radio'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'maxQuantity',
    label: 'Qtée max',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['inputNumber'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'buyingPrice',
    label: 'Prix achat',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['montant'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'color',
    label: 'Couleurs',
    required: true,
    showInList: true,
    type: 'color',
    listValues: [],
    possibleValue: ['color'],
    defaultValue: false,
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'netWeight',
    label: 'Poids net',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'grossWeight',
    label: 'Poids brut',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'volume',
    label: 'Volume',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'unity ',
    label: 'Unité',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'list',
      'tags',
      'file',
      'radio',
      'color',
      'inputNumber',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: false,
    disabled: false,
    typeDisabled: false
  },

  {
    key: 'perishable',
    label: 'Périssable',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['toggle'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'phygital',
    label: 'Code phygital pour table tactile',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'brochure',
    label: 'Brochure',
    required: false,
    showInList: false,
    type: 'url',
    listValues: [],
    possibleValue: ['url'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'disponobilty',
    label: 'Disponibilité à la vente',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['toggle'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'redLevel',
    label: 'Stock niveau rouge',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['inputNumber'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  // {
  //   key: 'orangeLevel',
  //   label: 'Stock niveau orange',
  //   required: true,
  //   showInList: true,
  //   type: 'string',
  //   listValues: [],
  //   possibleValue: ['inputNumber'],
  //   defaultValue: '',
  //   disabled: false,
  //   typeDisabled: false
  // },
  {
    key: 'greenLevel',
    label: 'Stock niveau vert',
    required: true,
    showInList: true,
    type: 'string',
    listValues: [],
    possibleValue: ['inputNumber'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres1',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche',
      'medias'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres2',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche',
      'medias'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres3',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche',
      'medias'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres4',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche',
      'medias'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres5',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche',
      'medias'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  }
];

export const SUPPLIERSFIELDS = [
  {
    key: 'logo',
    label: 'Logo',
    required: false,
    showInList: false,
    showInNotifications: false,
    type: 'file',
    listValues: [],
    possibleValue: ['file'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'name',
    label: 'Nom',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'string',
    possibleValue: ['string'],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'adress',
    label: 'Adresse',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'ref_ERP',
    label: 'Référence',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'compAd',
    label: 'Adresse complémentaire',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'description',
    label: 'Description',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'phonefix',
    label: 'Tél fixe',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'string',
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'phoneport',
    label: 'Tél portable',
    required: false,
    showInList: false,
    type: 'string',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['string'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'email',
    label: 'Email',
    required: false,
    showInList: false,
    type: 'email',
    showInNotifications: true,
    listValues: [],
    possibleValue: ['email'],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'products',
    label: 'Produits',
    required: false,
    showInList: false,
    type: 'map',
    listValues: [],
    possibleValue: ['tags'],
    defaultValue: '',
    disabled: false,
    typeDisabled: true
  },
  {
    key: 'active',
    label: 'Activation',
    required: false,
    showInList: false,
    type: 'cascader',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'supplierERP',
    label: 'Code fournisseur ERP',
    required: false,
    showInList: false,
    showInNotifications: true,
    type: 'string',
    ERPField: true,
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres1',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres2',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres3',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres4',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'montant',
      'inputNumber',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres5',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant',
      'checkBox',
      'Texte riche'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  }
];

export const SURVEYFIELDS = [
  {
    key: 'q1',
    label: `Produits présents sur l'étalage`,
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'q2',
    label: 'Produits concurrents',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'q3',
    label: 'Evaluation des produits par le client',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'q4',
    label: 'Critiques relatives aux produits',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'q5',
    label: 'Notoriété',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'q6',
    label: "Points d'amélioration souhaités",
    required: false,
    showInList: false,
    type: 'string',
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'q7',
    label: 'Réclamation',
    required: false,
    showInList: false,
    type: 'string',
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    listValues: [],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres1',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres2',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres3',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres4',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  },
  {
    key: 'qres5',
    label: '',
    required: false,
    showInList: false,
    type: 'string',
    listValues: [],
    possibleValue: [
      'string',
      'url',
      'list',
      'email',
      'tags',
      'toggle',
      'file',
      'evaluation',
      'textarea',
      'radio',
      'color',
      'date',
      'slider',
      'inputNumber',
      'montant'
    ],
    defaultValue: '',
    disabled: false,
    typeDisabled: false
  }
];

export const CLIENTFIELDS_BY_KEY = getByKey(CLIENTFIELDS);

export const PRODUCT_BY_KEY = getByKey(PRODUCT);

export const SURVEYFIELDS_BY_KEY = getByKey(SURVEYFIELDS);

export const SUPPLIERSFIELDS_BY_KEY = getByKey(SUPPLIERSFIELDS);

export const ALL_FIELDS = [
  ...CLIENTGROUPEFIELDS,
  ...CLIENTFIELDS,
  ...PRODUCT,
  ...SURVEYFIELDS,
  ...SUPPLIERSFIELDS
];

export const FIELDS_CONTAINERS_BY_KEY = {
  CLIENTGROUPEFIELDS: 'clientGroupe',
  CLIENTFIELDS: 'client',
  PRODUCT: 'product',
  SUPPLIERSFIELDS: 'suppliers'
};

export const SURVEY_FIELDS_CONTAINERS_BY_KEY = {
  SURVEYFIELDS: 'evaluationFields'
};

export const FIELDS_CONTAINERS = Object.keys(FIELDS_CONTAINERS_BY_KEY).map(
  key => FIELDS_CONTAINERS_BY_KEY[key]
);

